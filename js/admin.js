/*var siteurl = 'http://app.annectos.net:8081/temporder/';
var rooturl = 'http://app.annectos.net/ecomm.bulls.api';

var rootProdurl = 'http://puma.annectos.net/index.html#/main/prod/';
var rootCaturl = 'http://puma.annectos.net/index.html#/main/cat/';

var RootStart = "http://";
var RootEnd = ".annectos.net";

var GlobalOrderId = 0;
var GlobalUserId = 0;

var PopClose = 0;*/


var rooturl = 'http://localhost:8185';
var siteurl = 'http://52.5.222.127/ecomm.api.prod/ordercsv/';

var rootProdurl = 'http://54.251.138.26/index.html#/prod/';
var rootCaturl = 'http://54.251.138.26/index.html#/cat/';

var RootStart = "http://";
var RootEnd = ".annectos.net";

var GlobalOrderId = 0;
var GlobalUserId = 0;

var PopClose = 0;


var PaymentType = "";
var PumaPayStatus = 0;

var PartialOrderProducts = [];
var Partial_Product_Ids = [];

var open_ = window.open;

window.open = function (url, name, opts) {
    if (name === '_self') { name = '_blank'; }
    open_(url, '_blank', opts);
};

$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};

var loadingadd = function () {
    $('body').append('<div class="loading"><img src="images/loader2.gif" /></div>');
}
var loadingcut = function () {
    $('.loading').remove();
}

$.fn.nl2br = function () {
    return this.each(function () {
        $(this).val().replace("|", "\n");
    });
};


$(document).on('click', '#tblSrchProdGrid tbody tr', function () {

    var thisid = $(this).attr('id');
    $('#' + thisid + ' input[type=radio]').prop('checked', true);

    if ($('#' + thisid + ' input[type=radio]').is(':checked')) {
        var grid = jQuery('#tblSrchProdGrid');
        grid.jqGrid('setSelection', thisid);
    }
    var grid = jQuery('#tblSrchProdGrid');
    var selr = jQuery('#tblSrchProdGrid').jqGrid('getGridParam', 'selrow');
    var GridData = grid.getRowData(selr);

    /*Start********************Added Code For Search Product In Differnt Page***********************/

    if (ProdSrchFrom == 1) {
        $('#ddlProduct').val(GridData.id);
        getProductDetails(1);
    }
    else if (ProdSrchFrom == 2) {
        $('#ddlParentProduct').val(GridData.id);
        getChildProduct();
    }
    else if (ProdSrchFrom == 3) {
        $('#PCAProdList').val(GridData.id);
        getProductDetails(2);
    }
    else if (ProdSrchFrom == 4) {
        $('#PFMProductList').val(GridData.id);
        GetProdFeatures();
    }

    /*End********************Added Code For Search Product In Differnt Page***********************/
	
    $('#ProdSearchDiv, .alert-overlay2').addClass('hide');

});


$(document).ready(function () {



    //document screen size
    var windowWidth = $(document).width();
    var windowHeight = $(document).height();

    //top bar height
    var topBarHeight = $('.top-bar').height();

    //sub nav height
    var subNavHeight = $('.sub-nav').height();

    //footer height
    var footer = $('.footer').height();

    // pop up heading height
    var popheadingHeight = $('.popup-heading').height();

    // pop up footer height
    var popupfooterHeight = $('.popup-main-container .buttn-set').height();




    //flexible main div
    $('.flexible-main-div .inner').height(windowHeight - (topBarHeight + subNavHeight + footer + 25));
    $('.flexible-main-div .inner2').height(windowHeight - (topBarHeight + subNavHeight + footer + 75));

    //flexible pop up main div
    $('.flexiable-popup-div .inner').height(windowHeight - (popheadingHeight + popupfooterHeight + 155));

    //flexible pop up main div2
    $('.flexiable-popup-div .inner2').height(windowHeight - (popheadingHeight + popupfooterHeight + 190));
    $('.flexiable-popup-div .inner3').height(windowHeight - (popheadingHeight + popupfooterHeight + 190));
    $('.flexiable-popup-div2 .inner4, .add-location-table').height(windowHeight - (popheadingHeight + popupfooterHeight + 147));

    $('.tree-overflow').height($('.flexiable-popup-div2 .inner4').height() - 70);
    $('.tree-overflow2').height($('.flexiable-popup-div2 .inner4').height() - 90);
    $(window).resize(function () {
        var windowHeight = $(document).height();
        $('.flexible-main-div .inner').height(windowHeight - (topBarHeight + subNavHeight + footer + 25));
        $('.flexiable-popup-div .inner').height(windowHeight - (popheadingHeight + popupfooterHeight + 155));
        $('.flexiable-popup-div .inner2').height(windowHeight - (popheadingHeight + popupfooterHeight + 190));
        $('.flexiable-popup-div .inner3').height(windowHeight - (popheadingHeight + popupfooterHeight + 190));
        $('.flexible-main-div .inner2').height(windowHeight - (topBarHeight + subNavHeight + footer + 75));
        $('.flexiable-popup-div2 .inner4, .add-location-table').height(windowHeight - (popheadingHeight + popupfooterHeight + 147));
        $('.tree-overflow').height($('.flexiable-popup-div2 .inner4').height() - 70);
        $('.tree-overflow2').height($('.flexiable-popup-div2 .inner4').height() - 90);

        var nfpainner2 = $('#assign-nfpa-pop .inner2').height();
        if (nfpainner2 > 266) {
            $('.nfpa-box').css('margin-top', nfpainner2 / 2 - 130);
        }


    })

    //catalog sub menu
    $('.main-nav li a').click(function () {

        if ($(this).parent('li').hasClass('main-nav-li')) {
            $('.main-nav li.main-nav-li').removeClass('active');
            $(this).parent('li').addClass('active');
        }
        if ($(this).parent('li').parent('ul').parent('li').hasClass('catalog-sub-menu')) {
            $('.main-nav li.main-nav-li').removeClass('active');
            $(this).parent('li').parent('ul').parent('li').addClass('active');

            $('.popup-main-container').addClass('hide');
            $('#catalog-search').removeClass('hide');
        }
        var thisval = $(this).attr('href');
        return false;
    })



    //close popup window
    $('.popup-main-container .close-btn').click(function () {
        $(this).parent().addClass('hide');
    })


    //msds link search section
    var innerOne = "<input id=\"SearchText\" name=\"SearchText\" type=\"text\" style=\"width: 71%;\"/>";

    var innerFive = "<input id=\"SearchPNText\" name=\"SearchPNText\" onfocus=\"if (this.value === this.getAttribute('placeholder') ) {this.value = '';}\"  onblur=\"if (this.value === '') { this.value = this.getAttribute('placeholder');}\" placeholder =\"Product\" value =\"Product\" type=\"text\" value =\"\" style=\"width: 25%;\"/><input id=\"SearchMNText\" name=\"SearchMNText\" value =\"Manufacturer\" onfocus=\"if (this.value === this.getAttribute('placeholder') ) {this.value = '';}\"  onblur=\"if (this.value === '') { this.value = this.getAttribute('placeholder');}\"  placeholder='Manufacturer'  type=\"text\" style=\"width: 25%;\"/>" +
                            "<input id=\"SearchCNText\" name=\"SearchCNText\"  onfocus=\"if (this.value === this.getAttribute('placeholder') ) {this.value = '';}\"  onblur=\"if (this.value === '') { this.value = this.getAttribute('placeholder');}\" placeholder =\"CAS\" type=\"text\" value =\"CAS\" style=\"width: 25%;\"/><input id=\"SearchPTNText\" name=\"SearchPTNText\"  onfocus=\"if (this.value === this.getAttribute('placeholder') ) {this.value = '';}\"  onblur=\"if (this.value === '') { this.value = this.getAttribute('placeholder');}\" value =\"Product Code\" placeholder='Product Code' type=\"text\" style=\"width: 25%;\"/>";


    $('#searchOptions').change(function () {
        var thisval = $(this).val();
        if (thisval == '5') {
            $('#Searchtd').html(innerFive);
            $('#SearchPNText').focus();
        } else {
            $('#Searchtd').html(innerOne);
            $('#SearchText').focus();
        }

    })

    $('#ddlUploadType').change(function () {
        var thisval = $(this).val();
        alert(thisval);
        UploadDataType.value = thisval;
        alert(UploadDataType.value);
    })

    $('#ddlOrdStatus').change(function () {
        var thisval = $(this).val();
        //alert(thisval);
        if (thisval == 2 || thisval == 7) {
            $('#tblWarehouse').removeClass('hide');
        }
        else {

            $('#tblWarehouse').addClass('hide');
        }

    })


    $('#PageNo').change(function () {
        var thisval = $(this).val();
        getUserIntimateDtls(thisval);

    })

    //$("#txtDisputeNotesAct").blur(function () {

    //    PrevNote =  $.trim($("#txtDisputeNotesInAct").val());
    //    CurrNote =  $.trim($(this).val());
    //    DisputeNote = PrevNote + " ." + CurrNote;     
    //    $("#txtDisputeNotesInAct").val(DisputeNote);

    //});



    $(".StoreDate").datepicker({
        dateFormat: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        changeDay: true
    });

 

    $('.datepicker').datepicker({ dateFormat: 'mm/dd/yy', minDate: (0), maxDate: (2) });


    $(".onlyNumeric").keydown(function (event) {
        // Allow only backspace and delete
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    });


    //nfpa box flexible margin
    var nfpainner2 = $('#assign-nfpa-pop .inner2').height();
    if (nfpainner2 > 266) {
        $('.nfpa-box').css('margin-top', nfpainner2 / 2 - 130);
    }

    // setup link
    $('.set-up a').click(function () {
        var thisval = $(this).attr('href');
        $('.popup-main-container').addClass('hide');

        if (thisval == "Customer-Maintenance") {

            loadingadd();

            $("#ddlTrackCycle").val('MONT');
            $("#ddlTrackCycle").prop('disabled', true);

            var ddlCompMode = $("#CompanyEntryType");
            PopulateCompany(ddlCompMode);
            vm_CompanyViewModel = new CompanyViewModel();
            ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));

        }
        else if (thisval == "Category-Filter-Setup") {

            loadingadd();

            var CFCategoryList = $("#CFCategoryList");
            PopCatFilterList(CFCategoryList);
            $('#CFCategoryName').val('');
            $('#CFCatDisplayName').val('');
            $("#FilterList").GridUnload();
        }

        else if (thisval == "Product-Catalog-Maintenance") {


            var ddlBrnad = $("#ddlBrnad");
            PopulateBrandList(ddlBrnad, "L");

            loadingadd();
            var ddlProduct = $("#ddlProduct");
            PopulateProductList(ddlProduct);

            vm_ProductViewModel = new ProductViewModel();
            ko.applyBindings(vm_ProductViewModel, document.getElementById("Product-Catalog-Maintenance"));

            $("#btnUpldPrdImg").prop("disabled", true);
            $("#btnBulkProdPrice").prop("disabled", true);
            $("#btnBulkProdStock").prop("disabled", true);

            var ProductLink = $("#ProductLink");
            ProductLink.html("");

        }


        else if (thisval == "Product-Category-Association") {

            loadingadd();

            var PCAProdList = $("#PCAProdList");
            PopulateProductList(PCAProdList);

            //var PCACategoryList = $("#PCACategoryList");
            //setTimeout(function () { PopAllCategoryTree(PCACategoryList); }, 1000);
            //PopAllCategoryList(PCACategoryList);


            var DynamicChkBox = $("#DynamicChkBox");
            setTimeout(function () { PopAllCategoryTree(DynamicChkBox); }, 1000);



            var PCAParentCategory = $("#PCAParentCategory");
            setTimeout(function () { PopAllCategoryList(PCAParentCategory); }, 1000);
            //PopAllCategoryList(PCAParentCategory);


            $("#PCAProdName").val('');
            $("#PCAProdDesc").val('');

        }

        else if (thisval == "Store-Category-Maintenance") {


            $('#btnAddAnotherCat').prop('disabled', true);

            $('input[name=chkSplStore]').prop('checked', false).triggerHandler('click');


            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', true);


            jQuery("#tdLevel input:radio").removeAttr('disabled');
            $('#ddlParentCat').attr('disabled', false);
            $("#btnUpldImgVid").prop("disabled", true);
            $('[name=level]').prop('checked', false);
            $("#ddlParentCat").find('option').remove().end();

            loadingadd();
            var ddlCategory = $("#ddlCategory");
            PopulateCategoryList(ddlCategory);

            //vm_CategoryViewModel = new CategoryViewModel();
            //ko.applyBindings(vm_CategoryViewModel, document.getElementById("Store-Category-Maintenance"));
            ClearCateFields();

        }
        else if (thisval == "Child-Product-Maintenance") {

            loadingadd();
            var ddlParentProduct = $("#ddlParentProduct");
            //PopulateProductList(ddlParentProduct);
            PopParentProd(ddlParentProduct);
        }

        else if (thisval == "Product-Features-Maintenance") {

            loadingadd();

            var PFMProductList = $("#PFMProductList");
            PopParentProductList(PFMProductList);

            ClearProdFeature();

        }
        else if (thisval == "Brand-Setup") {

            //loadingadd();

            var ddlBrand = $("#ddlBrand");
            PopulateBrandList(ddlBrand, "N");
            $("#txtBrandName").val('');

        }
        else if (thisval == "Order-Processing") {


            var ddlOrderStore = $("#ddlOrderStore");
            PopCompanyUI(ddlOrderStore);

            var ddlwarehouse = $("#ddlwarehouse");
            popwarehouse(ddlwarehouse);


            //PaymentType = "Offline";
            //PopPayStatus(PaymentType);

        }
        else if (thisval == "Order-Tracking") {




            var ddlStoreForTrack = $("#ddlStoreForTrack");
            PopCompanyUI(ddlStoreForTrack);

        }

        else if (thisval == "Deal-Setup") {

            var ddlDealBrand = $("#ddlDealBrand");
            PopulateBrandList(ddlDealBrand, "L");
            PopDealHistory();

        }

        else if (thisval == "Product-Approval") {

            $("#ProductGrid").GridUnload();
            var ddlSchBrand = $("#ddlSchBrand");
            PopulateBrandList(ddlSchBrand, "L");
        }
            //else if (thisval == "Product-Enquiry") {
            //    var company = $("#ddlcompany");
            //   Populate_Company(company);
            // }

        else if (thisval == "Store-Config") {

            loadingadd();
            var ddlStoreComp = $("#ddlStoreComp");
            PopStoreCompany(ddlStoreComp);
        }

        else if (thisval == "E-gift-voucher") {

            loadingadd();
            var ddlVouComp = $("#ddlVouComp");
            PopStoreCompany(ddlVouComp);
        }

        else if (thisval == "Manage-E-Gift-Voucher") {

            loadingadd();
            var ddlVouSchComp = $("#ddlVouSchComp");
            PopStoreCompany(ddlVouSchComp);
            //getAllVoucherCode();
        }

        else if (thisval == "Order-View-Comments-History-subnav") {

            alert('Order - View - Comments - History - subnav');
        }
        else if (thisval == "OrderView") {

            alert('OrderView');
        }
        else if (thisval == "Assign-User-Rights") {


            getUserData();
        }

        else if (thisval == "Intimate-Users") {

            var ddlUIComp = $("#ddlUIComp");
            PopCompanyUI(ddlUIComp);

            $('#txtSearchEmail').val('');
            $("#tblUserIntimate").GridUnload();
            $("#gvFooter1").hide();


        }
        else if (thisval == "E-gift-voucher-report") {

            var ddlEGiftCompany = $("#ddlEGiftCompany");
            PopCompanyUI(ddlEGiftCompany);
        }

        else if (thisval == "Promo-Management") {

            $('#btnTransferPromo').attr('disabled', 'disabled');

            var ddlPromotion = $("#ddlPromotion");
            getPromoDetails(ddlPromotion);
            var ddlCustomerStore = $("#ddlCustomerStore");
            PopCompanyUI(ddlCustomerStore);



        }




        $('#' + thisval).removeClass('hide');
        return false;
    })
})

$(document).ready(function () {

    $('.main-app h2').addClass('headcolor');



    $('.tree_conatiner .close_btn').click(function () {
        $(this).parent().addClass('hide');
        $('.alert-overlay2').addClass('hide');
    })

    $('.tree_conatiner .close_btn2').click(function () {
        $(this).parent().addClass('hide');
        $('.alert-overlay3').addClass('hide');
    })

    $('.main-nav li a').click(function () {

        var thisval = $(this).attr('href');

        $('.popup-main-container').addClass('hide');

        if (thisval == "Warehouse-details") {

            // alert("Hi");
            //loadingadd();
            $("#txtware_name").prop('disabled', false);
            var ddlID = $("#warehouse_list");
            PopulateWarehouseList(ddlID);

        }

        else if (thisval == "Customer-Maintenance") {

            loadingadd();
            $("#ddlTrackCycle").val('MONT');
            $("#ddlTrackCycle").prop('disabled', true);
            var ddlCompMode = $("#CompanyEntryType");
            PopulateCompany(ddlCompMode);
            vm_CompanyViewModel = new CompanyViewModel();
            ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));

        }
        else if (thisval == "Category-Filter-Setup") {

            loadingadd();

            var CFCategoryList = $("#CFCategoryList");
            PopCatFilterList(CFCategoryList);
            $('#CFCategoryName').val('');
            $('#CFCatDisplayName').val('');
            $("#FilterList").GridUnload();
        }

        else if (thisval == "Product-Catalog-Maintenance") {


            var ddlBrnad = $("#ddlBrnad");
            PopulateBrandList(ddlBrnad, "L");

            loadingadd();

            var ddlProduct = $("#ddlProduct");
            PopulateProductList(ddlProduct);


            vm_ProductViewModel = new ProductViewModel();
            ko.applyBindings(vm_ProductViewModel, document.getElementById("Product-Catalog-Maintenance"));

            $("#btnUpldPrdImg").prop("disabled", true);
            $("#btnBulkProdPrice").prop("disabled", true);
            $("#btnBulkProdStock").prop("disabled", true);

            var ProductLink = $("#ProductLink");
            ProductLink.html("");

            //$("#imageList").GridUnload();

            //$("#imageList").jqGrid({
            //    datatype: "local", height: 150, colNames: ['Image Sl #', 'Public Link', 'Action'], colModel: [{ name: 'Filter_Name', index: 'Filter_Name', width: 200 },
            //      { name: 'Public_Link', index: 'Public_Link', width: 200 }, { name: 'action', index: 'action', width: 200 }],
            //    multiselect: true, caption: "<div class='search_result_heading green'><i class='icons'></i>List of Added Filters</div>"
            //});
            //var mydata = [{ Filter_Name: "", Public_Link: "", action: "Delete" },
            //              { Filter_Name: "", Public_Link: "", action: "Delete" },
            //              { Filter_Name: "", Public_Link: "", action: "Delete" },
            //              { Filter_Name: "", Public_Link: "", action: "Delete" }

            //];
            //for (var i = 0; i <= mydata.length; i++)
            //    $("#imageList").jqGrid('addRowData', i + 1, mydata[i]);
        }


        else if (thisval == "Product-Category-Association") {

            loadingadd();

            var PCAProdList = $("#PCAProdList");
            PopulateProductList(PCAProdList);

            //var PCACategoryList = $("#PCACategoryList");
            //setTimeout(function () { PopAllCategoryTree(PCACategoryList); }, 1000);
            //PopAllCategoryList(PCACategoryList);

            var DynamicChkBox = $("#DynamicChkBox");
            setTimeout(function () { PopAllCategoryTree(DynamicChkBox); }, 1000);



            var PCAParentCategory = $("#PCAParentCategory");
            setTimeout(function () { PopAllCategoryList(PCAParentCategory); }, 1000);
            //PopAllCategoryList(PCAParentCategory);


            $("#PCAProdName").val('');
            $("#PCAProdDesc").val('');

        }

        else if (thisval == "Store-Category-Maintenance") {


            $('#btnAddAnotherCat').prop('disabled', true);

            $('input[name=chkSplStore]').prop('checked', false).triggerHandler('click');


            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', true);

            jQuery("#tdLevel input:radio").removeAttr('disabled');
            $('#ddlParentCat').attr('disabled', false);
            $("#btnUpldImgVid").prop("disabled", true);
            $('[name=level]').prop('checked', false);
            $("#ddlParentCat").find('option').remove().end();

            loadingadd();
            var ddlCategory = $("#ddlCategory");
            PopulateCategoryList(ddlCategory);

            //vm_CategoryViewModel = new CategoryViewModel();
            //ko.applyBindings(vm_CategoryViewModel, document.getElementById("Store-Category-Maintenance"));
            ClearCateFields();

        }
        else if (thisval == "Child-Product-Maintenance") {

            loadingadd();
            var ddlParentProduct = $("#ddlParentProduct");
            // PopulateProductList(ddlParentProduct);
            PopParentProd(ddlParentProduct);
        }

        else if (thisval == "Product-Features-Maintenance") {

            loadingadd();
            var PFMProductList = $("#PFMProductList");
            PopParentProductList(PFMProductList);
            ClearProdFeature();
        }
        else if (thisval == "Brand-Setup") {

            //loadingadd();

            var ddlBrand = $("#ddlBrand");
            PopulateBrandList(ddlBrand, "N");
            $("#txtBrandName").val('');

        }
        else if (thisval == "Order-Processing") {


            var ddlOrderStore = $("#ddlOrderStore");
            PopCompanyUI(ddlOrderStore);


            var ddlwarehouse = $("#ddlwarehouse");
            popwarehouse(ddlwarehouse);


            //PaymentType = "Offline";
            //PopPayStatus(PaymentType);

        }
        else if (thisval == "Order-Tracking") {


            var ddlStoreForTrack = $("#ddlStoreForTrack");
            PopCompanyUI(ddlStoreForTrack);

        }
        else if (thisval == "Deal-Setup") {

            var ddlDealBrand = $("#ddlDealBrand");
            PopulateBrandList(ddlDealBrand, "L");
            PopDealHistory();


        }
        else if (thisval == "Product-Approval") {

            $("#ProductGrid").GridUnload();
            var ddlSchBrand = $("#ddlSchBrand");
            PopulateBrandList(ddlSchBrand, "L");
        }
		
            //else if (thisval == "Product-Enquiry") 
            //{
            //    var company = $("#ddlcompany");
            //    Populate_Company(company);
            // }
	   
        else if (thisval == "Store-Config") {

            loadingadd();
            var ddlStoreComp = $("#ddlStoreComp");
            PopStoreCompany(ddlStoreComp);

        }
        else if (thisval == "E-gift-voucher") {

            loadingadd();
            var ddlVouComp = $("#ddlVouComp");
            PopStoreCompany(ddlVouComp);
        }
        else if (thisval == "Manage-E-Gift-Voucher") {
            //getAllVoucherCode();
            loadingadd();
            var ddlVouSchComp = $("#ddlVouSchComp");
            PopStoreCompany(ddlVouSchComp);
        }
        else if (thisval == "Order-View-Comments-History-subnav") {

            alert('Order - View - Comments - History - subnav');
        }
        else if (thisval == "OrderView") {

            alert('OrderView');
        }
        else if (thisval == "Assign-User-Rights") {
            getUserData();
        }
        else if (thisval == "Intimate-Users") {

            alert('Users');
        }
        else if (thisval == "Promo-Management") {

            $('#btnTransferPromo').attr('disabled', 'disabled');
            //$('#btnTransferPromo').prop('disabled', true);

            var ddlPromotion = $("#ddlPromotion");
            getPromoDetails(ddlPromotion);
            var ddlCustomerStore = $("#ddlCustomerStore");
            PopCompanyUI(ddlCustomerStore);



        }

        else if (thisval == "Daily-Sales-Wise-Report") {

            var ddlReportStore = $("#ddlReportStore");
            PopCompanyUI(ddlReportStore);
        }

        else if (thisval == "Customer-Points-Balance-Report") {

            var ddlCustPointStoreReport = $("#ddlCustPointStoreReport");
            PopCompanyUI(ddlCustPointStoreReport);
            $('#txtEmailCustPointReport').val('');
        }



        $('#' + thisval).removeClass('hide');
        return false;

    })



    $('#btnAddProducts').click(function () {

        $("#ProductList").GridUnload();

        $("#ProductList").jqGrid({
            datatype: "local", height: 200, colNames: ['Product Name'], colModel: [{ name: 'Product_Name', index: 'Product_Name', width: 200 }],
            multiselect: true, caption: "<div class='search_result_heading green'><i class='icons'></i>List of Products</div>"
        });
        var mydata = [{ Product_Name: "ABC", action: "Delete" },
                      { Product_Name: "EFG", action: "Delete" },
                      { Product_Name: "HIJ", action: "Delete" },
                      { Product_Name: "KLM", action: "Delete" },
                      { Product_Name: "NOP", action: "Delete" },
                      { Product_Name: "QRS", action: "Delete" }
        ];
        for (var i = 0; i <= mydata.length; i++)
            $("#ProductList").jqGrid('addRowData', i + 1, mydata[i]);

        $('#AddProduct').removeClass('hide');

    });

    $("#txtProdDesc").keyup(function () {
        var i = $("#txtProdDesc").val().length;
        $("#CountCharacter").text('Characters: ' + i);
    });


    $('#btnUpldFile').click(function () {



        $('#CustPointUpld').submit();

    });


    $('#btnChkOrderStatus').click(function () {

        var OrdType = $("#ddlOrderTrackStatus").val();

        if (OrdType == "1") {
            getOrderTrackRecord();
        }
        else {

            getWillShipTrackRecord();
        }

    });






    $('#btnCanEGiftVouSrch').click(function () {

        $("#ddlEGiftCompany").val(0);
        $("#tblVouDtls").GridUnload();
        $(".StoreDate").val('');

    });

    $('#ddlPromotion').change(function () {






        var Promotion_ID = $(this).val();

        if (Promotion_ID != "0") {

            loadingadd();

            $.support.cors = true;
            $.ajax({
                url: rooturl + '/admin/promo_list/',
                type: 'GET',
                data: { promo_id: Promotion_ID },
                dataType: 'json',
                cache: false,
                success: function (result) {
                    loadingcut();

                    $('#txtPromoName').val(result[0].promo_name);
                    var CompVal = $("#ddlCustomerStore option:contains(" + result[0].company + ")").val();
                    $('#ddlCustomerStore').val(CompVal);
                    $('#txtMinPurchase').val(result[0].min_purchase);
                    $('#ddlDiscType').val(result[0].disc_type);
                    $('#txtMaxDiscount').val(result[0].max_discount);
                    $('#txtPromoStartDate').val(result[0].start_date);
                    $('#txtPromoEndDate').val(result[0].end_date);



                    var Curren_Date = new Date();
                    var End_Date = $.trim($("#txtPromoEndDate").val());
                    End_Date = new Date(End_Date);
                    if (Curren_Date > End_Date) {
                        $('#btnTransferPromo').attr('disabled', 'disabled');
                    }
                    else {
                        $('#btnTransferPromo').removeAttr('disabled');
                    }

                    loadingcut();

                },
                error: function (req, status, errorObj) {
                    alert('Error');
                    loadingcut();
                }
            });
        }
        else {
            $(".Promo").val('');

            $("#ddlCustomerStore").val(0)
            $("#ddlDiscType").val(0)

        }


    });




    $('#btnAddUser').click(function () {

        //var ddlUserCompany = $("#ddlUserCompany");
        //PopulateCompany(ddlUserCompany);

        vm_UserViewModel = new UserViewModel();
        ko.applyBindings(vm_UserViewModel, document.getElementById("adduserdiv"));

        $('#adduserdiv, .alert-overlay2').removeClass('hide');

    });

    $('#btnCancelUser').click(function () {

        vm_UserViewModel = new UserViewModel();
        ko.applyBindings(vm_UserViewModel, document.getElementById("adduserdiv"));

        $('#adduserdiv, .alert-overlay2').addClass('hide');

    });


    $('#btnChangePinCancel').click(function () {

        $('#PinDiv').removeClass('PinFieldRed');
        $('#txtChngPinUserID').val('');
        $("#lblPin").text('');


    });

    $(".NumericField").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything

        var input = $(this).val();
        var lines = input.split('.');

        if (lines.length > 2) {
            alertBox('More Than One Decimal Point Is Not Allowed');
            return false;
        }

        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
            //display error message
            //            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });


    $('#btnCancelChildProd').click(function () {

        $("#txtEditChildProdStock").val('');
        $("#chkEditProdExpress").prop('checked', false);
        $('#EditChildProd, .alert-overlay2').addClass('hide');

    });


    $('#btnAddAnotherCat').click(function () {

        $('#ddlCategory').val(0);
        getCategoryDetails(1);
        $("#btnCatSave").prop('disabled', false);
        $(this).prop("disabled", true);

    });



    $('#btnBack').click(function () {

        if (UploadFor.value == "1") {
            $('#Store-Category-Maintenance').removeClass('hide');
        }
        else if (UploadFor.value == "2") {
            $('#Product-Catalog-Maintenance').removeClass('hide');
        }

        $('#UploadImageVideoUrl').addClass('hide');
    });

    $('#btnBackCat').click(function () {
        $('#Category-Filter-Setup').addClass('hide');
        $('#Store-Category-Maintenance').removeClass('hide');
    });

    $('#btnBackCatMain').click(function () {
        $('#Product-Category-Association').addClass('hide');
        $('#Store-Category-Maintenance').removeClass('hide');
    });

    $('#btnBackOrdView').click(function () {

        $('#Order-Processing').removeClass('hide');
        $('#OrderView').addClass('hide');
        $('.OrderView').val('');
        $("#ItemsOrdered").GridUnload();
    });

    $('#btnAddProdOthCat').click(function () {

        $('#ProdFromOthCat').removeClass('hide');


    });
    $('#btnRemoveProdByCat').click(function () {

        $('#RemoveProduct').removeClass('hide');

    });

    $('#btnCancelGift').click(function () {
        $('.clsegift').val('');
        $('#ddlVouComp').val(0);
    });


    $('#btnUpdPayStatus').click(function () {
        alert('In Progress');
    });


    $('#btnCancelComp').click(function () {
        vm_CompanyViewModel = new CompanyViewModel();
        ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));


    });


    /////*********************User Grid***********************/
    //$(document).on('click', '#UserGrid tbody tr', function () {            
    //    var thisid = $(this).attr('id');     
    //    $('#' + thisid + ' input[type=radio]').attr('checked', 'checked');
    //   // $('#' + thisid + ' input[type=radio]').hide();
    //})



    $('#btnCancelBrand').click(function () {
        var ddlBrand = $("#ddlBrand");
        PopulateBrandList(ddlBrand, "N");
        $("#txtBrandName").val('');
    });


    $('#btnCancelCatSave').click(function () {
        //vm_CategoryViewModel = new CategoryViewModel();
        //ko.applyBindings(vm_CategoryViewModel, document.getElementById("Store-Category-Maintenance"));
        //$("#ddlCategory").val(0);
        //jQuery("#tdLevel input:radio").removeAttr('disabled');
        //$('#ddlParentCat').attr('disabled', false);
        //$('[name=level]').prop('checked', false);
        //$("#ddlParentCat").find('option').remove().end();
        //$("#btnUpldImgVid").prop("disabled", true);
        ClearCateFields();
    });

    $('#btnCopyFilter').click(function () {
        var CFCategoryList = $("#CFCategoryList").val();
        if (CFCategoryList == "0" || CFCategoryList == null) {
            alert('Please Select Category');
            return false;
        }
        var CFCatList = $("#CFCatList");
        PopCatListToCopy(CFCatList);
        $("#ddlCatFilters").find('option').remove().end();
        $('#CopyCategory').removeClass('hide');

    });
    $('#btnCancelValue').click(function () {
        $("#FeatureValues").find('option').remove().end();
    });
    $('#btnRemoveFilter').click(function () {

        var RemoveItem = $('#FilterValues option:selected').val();
        if (typeof RemoveItem === "undefined") {
            alert('Please Choose Option To Remove');
            return false;
        }
        else {
            $('#FilterValues option:selected').remove();
        }

    });


    $('#btnCancelPCA').click(function () {
        $("#PCAProdList").val(0);
        $("#PCAProdName").val('');
        $("#PCAProdDesc").val('');
        $(".ClsSelect").val(0);
    });




    $('#btnSavewarehouseInfo').click(function () {


        var ddlwarehouse = $("#ddlwarehouse").val();
        if (ddlwarehouse == "0" || ddlwarehouse == null) {
            alert('Please Select Warehouse');
            return false;
        }


        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, status: 10, warehouse: ddlwarehouse },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Email Send To The Warehouse Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
                loadingcut();
            }
        });


    });


    $('#btnAddFeature').click(function () {

        var Prod = $('#PFMProductList').val();
        if (Prod == "0") {
            alert('Please Select Product First');
            return true;
        }

        $('#AdditionalFeatureDiv, .alert-overlay2').removeClass('hide');
        $('.addfeature').val('');
    });


    $('#btnCanSrchIntimateUser').click(function () {


        $('#ddlUIComp').val("0");
        $('#txtSearchEmail').val('');
        $("#tblUserIntimate").GridUnload();
        $("#gvFooter1").hide();

    });


    $('#btnOrdStatUpd').click(function () {

        var UpdOrdStatus = $('#ddlOrdStatus').val();
     
        $('#txtDaysDelayed').val('');
        $('#txtOrdUpdInfo').val('');

        if (UpdOrdStatus == -1) {
            alert('Please Select An Order Status.');
            return false;
        }
        if (UpdOrdStatus == 5) {
            //$('#OrdUpdStaHeading').text('Shipping Information');
            //$('#lblOrdUpdCap').text('Enter Shipping Information');
            $('#OrderShipInfo, .alert-overlay2').removeClass('hide');
        }
        else if (UpdOrdStatus == 6) {

            $('#trNoOfDaysDelay').removeClass('hide');
            $('#OrdUpdStaHeading').text('Delayed Information');
            $('#lblOrdUpdCap').text('Enter Delayed Note');
            $('#OrderStatusUpdInfo, .alert-overlay2').removeClass('hide');
        }
        else if (UpdOrdStatus == 7) {
            $('#trNoOfDaysDelay').addClass('hide');
            $('#OrdUpdStaHeading').text('Cancellation Information');
            $('#lblOrdUpdCap').text('Enter Cancellation Note');
            $('#OrderStatusUpdInfo, .alert-overlay2').removeClass('hide');
        }
        else if (UpdOrdStatus == 13) {
            //$('#OrdUpdStaHeading').text('Shipping Information');
            //$('#lblOrdUpdCap').text('Enter Shipping Information');
           
            $('#DisputeInfo, .alert-overlay2').removeClass('hide');
        }

        else if (UpdOrdStatus == 14) {
            
            $('#txtExtendedOrderNo').val(GlobalOrderId);
            $('#ExtendedInfo, .alert-overlay2').removeClass('hide');
            // $('#OrderShipInfo, .alert-overlay2').removeClass('hide');
        }


        else if (UpdOrdStatus == 102) {

            $('#PaymentInfoDiv, .alert-overlay2').removeClass('hide');
        }
        else if (UpdOrdStatus == 17) {
            ViewPartialProd(PartialOrderProducts);
            $('#PartialOrder, .alert-overlay2').removeClass('hide');
        }
        else {

            var ddlOrdStatus = $("#ddlOrdStatus").val()
            
            if (ddlOrdStatus == "2") {
                ddlwarehouse = $('#ddlwarehouse').val();
                if (ddlwarehouse == "" || ddlwarehouse == null || ddlwarehouse == "0") {
                    alert('Please Select Warehouse.');
                    return false;
                }
            }
            else {
                ddlwarehouse = "";
            }

            $.support.cors = true;
            $.ajax({
                url: rooturl + '/category/update_order/',
                type: 'POST',
                data: { order_id: GlobalOrderId, status: ddlOrdStatus, warehouse: ddlwarehouse },
                dataType: 'json',
                success: function (result) {
                    alert('Record Updated Successfully.');
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }
    });
    $('#btnPartiallyShipped').click(function () {


        Partial_Product_Ids = get_Partial_Prod_ID();

        if (Partial_Product_Ids != false) {

            $('#PartialOrder, .alert-overlay2').addClass('hide');
            $('#OrderShipInfo, .alert-overlay2').removeClass('hide');
        }

    });


    $('#btnAddPartialShipQty').click(function () {

        var ProdID = $.trim($("#txtShipPartialProdID").val());
        var ProdSKU = $.trim($("#txtShipPartialProdSKU").val());
        
        var ProdOrdQty = $.trim($("#txtShipPartialProdOrdQty").val()) * 1;
        var ProdDelQty = $.trim($("#txtShipPartialProdDelQty").val()) * 1;
        var ProdShipQty = $.trim($("#txtShipPartialProdShipQty").val()) * 1;
        var BalQty=ProdOrdQty-ProdDelQty;

        if (ProdShipQty <= 0) {
            alert('Shipping Quantity Should Not Be Less Than Or Equal To 0');
            return false;
        }else if (ProdShipQty > ProdOrdQty) {
            alert('Shipping Quantity Should Be Less Than Ordered Quantity');
            return false;
        }
        else if (ProdShipQty > BalQty) {
            alert('Shipping Quantity Should Be Equal To Balance Quantity ');
            return false;
        }


        for (var i = 0; i < PartialOrderProducts.length; i++) {

            if (PartialOrderProducts[i].id == ProdID && PartialOrderProducts[i].sku==ProdSKU)
            {
                PartialOrderProducts[i].shipping_qty = ProdShipQty;
                break;
            }

        }

        ViewPartialProd(PartialOrderProducts);


        $('#AddShippingQty, .alert-overlay3').addClass('hide');
           
    });

    $('#btnCancelPartialShipQty').click(function () {

        $('#AddShippingQty, .alert-overlay3').addClass('hide'); 

    });

    $('#btnPartiallyCancel').click(function () {


        // Partial_Product_Ids = [];
        $('#PartialOrder, .alert-overlay2').addClass('hide');



    });


    $('#btnAddCourierInfo').click(function () {


        var ddlOrdStatus = $.trim($("#ddlOrdStatus").val());

        var txtShippingDate = $.trim($("#txtShippingDate").val());
        if (txtShippingDate == null || txtShippingDate == "") {
            alert('Please Choose Shipping Date');
            return false;
        }


        var txtCourServProvider = $.trim($("#txtCourServProvider").val());
        if (txtCourServProvider == null || txtCourServProvider == "") {
            alert('Please Enter Courier Service Provider');
            return false;
        }

        var txtCourierTrackNo = $.trim($("#txtCourierTrackNo").val());
        if (txtCourierTrackNo == null || txtCourierTrackNo == "") {
            alert('Please Enter Courier Track No');
            return false;
        }

        var txtCourierTrackLink = $.trim($("#txtCourierTrackLink").val());
        if (txtCourierTrackLink == null || txtCourierTrackLink == "") {
            alert('Please Enter Courier Track Link');
            return false;
        }

        var txtCourierCost = $.trim($("#txtCourierCost").val());
        if (txtCourierCost == null || txtCourierCost == "") {
            alert('Please Enter Courier Cost');
            return false;
        }

        var Send_Email_Criteria = 0;

        if ($("#chkSendEmail").is(':checked')) {
            var Send_Email_Criteria = 1;
        }

        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, status: ddlOrdStatus, shipping_info: txtCourServProvider, courier_track_no: txtCourierTrackNo, courier_track_link: txtCourierTrackLink, courier_cost: txtCourierCost, shipping_date: txtShippingDate, send_email: Send_Email_Criteria, partial_order_products: Partial_Product_Ids },
            dataType: 'json',
            success: function (result) {
                Partial_Product_Ids = [];
                ViewOrder(GlobalOrderId);
                $('#OrderShipInfo, .alert-overlay2').addClass('hide');
                alert('Record Updated Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    });


    $('#btnDisputeInfo').click(function () {


        var ddlOrdStatus = $.trim($("#ddlOrdStatus").val());

        var txtDisputeNotesAct = $.trim($("#txtDisputeNotesAct").val());
        if (txtDisputeNotesAct == null || txtDisputeNotesAct == "") {
            alert('Please Enter Dispute Notes');
            return false;
        }


        var PrevNote = $.trim($("#txtDisputeNotesInAct").val());
        var CurrNote = $.trim($("#txtDisputeNotesAct").val());
        var DisputeNote = "";
        if (PrevNote == null || PrevNote == "") {
            DisputeNote = CurrNote;
        }
        else {
            DisputeNote = PrevNote + " ." + CurrNote;
        }
        $("#txtDisputeNotesInAct").val(DisputeNote);


        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, status: ddlOrdStatus, dispute_notes: DisputeNote },
            dataType: 'json',
            success: function (result) {
                $('#DisputeInfo, .alert-overlay2').addClass('hide');
                alert('Record Updated Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    });



    $('#btnExtendedInfo').click(function () {


        var ddlOrdStatus = $.trim($("#ddlOrdStatus").val());
        // $("#txtExtendedOrderNo").val(GlobalOrderId);
        //var txtDisputeNotesAct = $.trim($("#txtDisputeNotesAct").val());
        //if (txtDisputeNotesAct == null || txtDisputeNotesAct == "") {
        //    alert('Please Enter Dispute Notes');
        //    return false;
        //}



        var Extended_time = $.trim($("#txtExtendedDate").val());
        var Extended_reason = $.trim($("#txtExtendedReason").val());
        //var CurrNote = $.trim($("#txtDisputeNotesAct").val());
        //var DisputeNote = "";
        //if (PrevNote == null || PrevNote == "") {
        //    DisputeNote = CurrNote;
        //}
        //else {
        //    DisputeNote = PrevNote + " ." + CurrNote;
        //}
        //$("#txtDisputeNotesInAct").val(DisputeNote);


        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, status: ddlOrdStatus, expiry_date: Extended_time, extended_reason: Extended_reason },
            dataType: 'json',
            success: function (result) {
                $('#DisputeInfo, .alert-overlay2').addClass('hide');
                alert('Record Updated Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    });



    $('#btnPayInfo').click(function () {


        var ddlOrdStatus = $.trim($("#ddlOrdStatus").val());

        var txtPayNotes = $.trim($("#txtPayNotes").val());
        if (txtPayNotes == null || txtPayNotes == "") {
            alert('Please Enter Payment Notes');
            return false;
        }

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, status: ddlOrdStatus, payment_notes: txtPayNotes },
            dataType: 'json',
            success: function (result) {
                $('#PaymentInfoDiv, .alert-overlay2').addClass('hide');
                alert('Record Updated Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    });


    $('#btnCancelDisputeInfo').click(function () {

        $("#txtDisputeNotesAct").val('');

    });

    $('#btnCancelExtendedInfo').click(function () {

        $("#txtExtendedDate").val('');
        $("#txtExtendedReason").val('');

    });





    $('#btnCancelCourierInfo').click(function () {

        $("#txtCourServProvider").val('');
        $("#txtShippingDate").val('');
        $("#txtCourierCost").val('');
        $("#txtCourierTrackNo").val('');
        $("#txtCourierTrackLink").val('');
        $("#chkSendEmail").prop('checked', false);
        $('#OrderShipInfo, .alert-overlay2').addClass('hide');

    });


    //  $('#btnSearchProd').click(function () {
    //     var ddlSrchBrand = $("#ddlSrchBrand");
    //     PopulateBrandList(ddlSrchBrand, "L");
    //     $('.ProdSrchFld').val('');
    //     $("#tblSrchProdGrid").GridUnload();
    //     $('#ProdSearchDiv h2').addClass('headcolor');
    //     $('#ProdSearchDiv, .alert-overlay2').removeClass('hide');
    //  });

    $('#btnCancelProdSrch').click(function () {

        $('.ProdSrchFld').val('');
        $("#tblSrchProdGrid").GridUnload();
    });

    $('#btnCancelPromo').click(function () {

        $(".Promo").val('');
        $("#ddlPromotion").val(0);
        $("#ddlCustomerStore").val(0)
        $("#ddlDiscType").val(0)
    });


    $('#btnAddOrdStatNote').click(function () {

        var OrdStatusNote = $.trim($("#txtOrdUpdInfo").val());
        var ddlOrdStatus = $("#ddlOrdStatus").val();

        var txtDaysDelayed = $.trim($("#txtDaysDelayed").val());

        var ddlwarehouse = $("#ddlwarehouse").val();

        if (OrdStatusNote == '' || OrdStatusNote == null) {

            if (ddlOrdStatus == 5) {
                alert('Please Enter Shipping Information');
                return false;
            }
            else if (ddlOrdStatus == 6) {
                alert('Please Enter Delayed Note');
                return false;
            }
            else if (ddlOrdStatus == 7) {
                alert('Please Enter Cancellation Note');
                return false;
            }

        }

        if (ddlOrdStatus == 6) {
            if (txtDaysDelayed == '' || txtDaysDelayed == null) {
                alert('Please Enter No. Of Days Delayed.');
                return false;
            }
        }


        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/update_order/',
            type: 'POST',
            data: { order_id: GlobalOrderId, Delay_Days: txtDaysDelayed, status: ddlOrdStatus, shipping_info: OrdStatusNote, delayed_note: OrdStatusNote, cancellation_note: OrdStatusNote, warehouse: ddlwarehouse },
            dataType: 'json',
            success: function (result) {
                $('#OrderStatusUpdInfo, .alert-overlay2').addClass('hide');
                alert('Record Updated Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    });

    $('#btnCancelOrdStatNote').click(function () {
        $('#txtOrdUpdInfo').val('');
        $('#OrderStatusUpdInfo, .alert-overlay2').addClass('hide');
    });


    $('#btnCancelFeatures').click(function () {
        $("#FilterValues").find('option').remove().end();
        $('#SelFeaturesValue').addClass('hide');
    });
    $('#btnCopyFilterCancel').click(function () {

        $("#FilterValues").find('option').remove().end();
        $('#CopyCategory').addClass('hide');
    });

    $('#btnProdCancel').click(function () {

        $("#txtSchProdName").val('');
        $("#ddlSchBrand").val('');
        $("#ProductGrid").GridUnload();

    });

    $('#btnExportToExcel').click(function (e) {

        var OrdStore = $("#ddlOrderStore").val();
        if (OrdStore == 0) {
            OrdStore = null;
        }
        else {

            var OrdStore = $.trim($("#ddlOrderStore option:selected").text());
        }



        var OrdTyp = $("#ddlOrderType").val();
        var txtOrdFrom = $.trim($("#txtOrdFrom").val());
        if (txtOrdFrom == '' || txtOrdFrom == null) {
            txtOrdFrom = null;
        }
        var txtOrdTo = $.trim($("#txtOrdTo").val());
        if (txtOrdTo == '' || txtOrdTo == null) {
            txtOrdTo = null;
        }

        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/getOrder_Export_To_Excel?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&OrderFrmDt=' + txtOrdFrom + '&OrderToDt=' + txtOrdTo + '&json=true',
            type: "GET",
            dataType: "json",
            cache: false,
            success: function (result) {
                loadingcut();
                window.open(siteurl + result + ".csv", "_blank");
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    });


    $('#btnLinkCFS').click(function () {

        var Category_Type = $("#ddlCategory").val();

        if (Category_Type == 0) {
            alert('Please Select A Category.');
            return false;
        }
        else {
            var arr = Category_Type.split('.');
            if (arr.length == 2) {
                alert("You Can't Select A Level 1 Category ");
                return false;
            }
        }

        var CFCategoryList = $("#CFCategoryList");
        PopCatFilterList(CFCategoryList);
        setTimeout(function () { $("#CFCategoryList").val(Category_Type); }, 1000);
        setTimeout(function () { getCategoryDetails(2); }, 1500);

        $('#CFCategoryName').val('');
        $('#CFCatDisplayName').val('');
        $("#FilterList").GridUnload();
        $('.popup-main-container').addClass('hide');
        $('#Category-Filter-Setup').removeClass('hide');
    });
    $('#btnLinkPCA').click(function () {
        var PCAProdList = $("#PCAProdList");
        PopulateProductList(PCAProdList);
        //var PCACategoryList = $("#PCACategoryList");
        //PopAllCategoryTree(PCACategoryList);

        var DynamicChkBox = $("#DynamicChkBox");
        setTimeout(function () { PopAllCategoryTree(DynamicChkBox); }, 1000);

        var PCAParentCategory = $("#PCAParentCategory");
        PopAllCategoryList(PCAParentCategory);
        $("#PCAProdName").val('');
        $("#PCAProdDesc").val('');
        $('.popup-main-container').addClass('hide');
        $('#Product-Category-Association').removeClass('hide');
    });
    $('#btnLinkBSC').click(function () {

        $('.popup-main-container').addClass('hide');
        $('#Build-Store-Categlog').removeClass('hide');
    });

    $('#btnCancelCopyFilter').click(function () {

        $('#AddFilterValue').addClass('hide');
    });


    $('#btnCancelFeature').click(function () {

        ClearProdFeature();
        $("#PFMProductList").val(0);

    });
    $('#btnCancelAddFeature').click(function () {

        $("#txtAddFeature").val('');
        $('#txtAddFeatureValue').val('');
        $("#AdditionalFeature").GridUnload();

    });


    $('#btnCancelAddFeatures').click(function () {

        $('.addfeature').val('');
    });

    /******************Copy Category Filters**************/

    $('#btnCopyCatFilter').click(function () {

        var Master_Cat_ID = $("#CFCategoryList").val();
        var Cat_ID = $("#CFCatList").val();

        var ddlCatFilters = $("#ddlCatFilters").val();
        var i = 0;
        $('#ddlCatFilters').find('option').each(function () {
            i++;
        });

        if (i == 0) {
            alert('There Are No Filter To Copy.Select Another One!');
            return false;
        }

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/insert_filter/',
            type: 'POST',
            data: { copyto_catid: Master_Cat_ID, copyfrom_catid: Cat_ID },
            dataType: 'json',
            success: function (result) {
                alert('Copied Successfully.');
                $('#CopyCategory').addClass('hide');
                getCategoryDetails(2);
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    });

    $('input:radio[name=level]').click(function () {

        var val = $('input:radio[name=level]:checked').val();
        if (val == "3") {
            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', false);
            $("#txtShipping").val('3-5 Days');
            $("#txtMinShipDays , #txtMaxShipDays").val('');
        }
        else {
            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', true);
            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").val('');
        }

        PopParentCatList();
    });

    $('input:radio[name=SalesTracking]').click(function () {

        var val = $('input:radio[name=SalesTracking]:checked').val();
        $("#ddlTrackCycle").val('MONT');

        if (val == "1") {
            $("#ddlTrackCycle").prop('disabled', false);
        }
        else {
            $("#ddlTrackCycle").prop('disabled', true);
        }


    });



    $('#chkHaveChild').click(function () {

        if ($("#chkHaveChild").is(':checked')) {

            var ddlProduct = $("#ddlProduct").val();

            if (ddlProduct == "0") {

                //$("#txtStock").val('');
            }
            //$("#txtStock").prop('disabled', true);
            $("#btnBulkProdStock").prop("disabled", true);
            $("#chkExpress").prop('checked', true);
            $("#chkExpress").prop('disabled', true);

        }
        else {
            //$("#txtStock").prop('disabled', false);
            $("#btnBulkProdStock").prop("disabled", false);
            $("#chkExpress").prop('disabled', false);
            $("#chkExpress").prop('checked', false);
        }

    });

    $('#chkSplStore').click(function () {

        if ($("#chkSplStore").is(':checked')) {

            $('.clsspecial').prop('disabled', false);
        }
        else {

            $('.clsspecial').prop('disabled', true);
        }

    });


    /****************Multiple Item Edit********************/

    //$('#ddlOrder').change(function () {
    //    var ddlOrder = $("#ddlOrder").val();
    //    PopOrderDetails(ddlOrder);
    //});




})


/******************************Company ViewModel*******************************/
var vm_UserViewModel;

function UserViewModel() {

    //Make the self as this reference

    var self = this;

    //Declare observable which will be bind with UI

    self.Id = ko.observable("");
    self.user_name = ko.observable("");
    self.company_id = ko.observable(82);
    self.password = ko.observable("");
    self.user_type = ko.observable("");

    //The Object which stored data entered in the observables

    var UserData = {

        Id: self.Id,
        user_name: self.user_name,
        company_id: self.company_id,
        password: self.password,
        user_type: self.user_type
    };
}





/******************************Company ViewModel*******************************/
var vm_CompanyViewModel;

function CompanyViewModel() {

    //Make the self as this reference

    var self = this;

    //Declare observable which will be bind with UI

    self.id = ko.observable(0);
    self.name = ko.observable("");
    self.displayname = ko.observable("");
    self.contactname = ko.observable("");
    self.phonenumber = ko.observable("");
    self.address = ko.observable("");
    self.emailid = ko.observable("");
    self.logo = ko.observable("");
    self.points_ratio = ko.observable("");
    self.point_range_max = ko.observable("");
    self.point_range_min = ko.observable("");
    self.root_url_1 = ko.observable("");
    self.root_url_2 = ko.observable("");
    self.shipping_charge = ko.observable("");
    self.min_order_for_free_shipping = ko.observable("");

    self.sales_tracking = ko.observable("0");
    self.track_cycle = ko.observable("MONT");
    self.validity = ko.observable("365");


    //The Object which stored data entered in the observables

    var CompanyData = {

        id: self.id,
        name: self.name,
        displayname: self.displayname,
        contactname: self.contactname,
        phonenumber: self.phonenumber,
        address: self.address,
        emailid: self.emailid,
        logo: self.logo,
        points_ratio: self.points_ratio,
        point_range_max: self.point_range_max,
        point_range_min: self.point_range_min,
        root_url_1: self.root_url_1,
        root_url_2: self.root_url_2,
        shipping_charge: self.shipping_charge,
        min_order_for_free_shipping: self.min_order_for_free_shipping,

        sales_tracking: self.sales_tracking,
        track_cycle: self.track_cycle,
        validity: self.validity

    };
}

/*********************Populate Company Names*********************************/
function PopulateCompany(ddlID) {

    var EntryMode = "N";

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postPopulateCompany',
        type: "POST",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.name));
            });

            loadingcut();
        }
    });

}
/***************Function: Get Company Data******************/
function getCompDetails() {

    loadingadd();

    var Company_ID = $("#CompanyEntryType").val();

    if (Company_ID != 0) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/user/admin/postCompanyDetails/',
            type: 'POST',
            data: { id: Company_ID },
            dataType: 'json',
            success: function (result) {
                vm_CompanyViewModel = result[0];
                ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));

                var val = $('input:radio[name=SalesTracking]:checked').val();
                if (val == "1") {
                    $("#ddlTrackCycle").prop('disabled', false);
                }
                else {
                    $("#ddlTrackCycle").prop('disabled', true);
                }
                loadingcut();
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    }
    else {
        vm_CompanyViewModel = new CompanyViewModel();
        ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));

    }


}


/***************Function: Saving Company Data******************/

function SaveCompany() {

    var CMCompanyName = $.trim($("#CMCompanyName").val());
    if (CMCompanyName == null || CMCompanyName == "") {
        alert('Please Enter Company Name');
        return false;
    }

    var i = 0;

    $('select#CompanyEntryType').find('option').each(function () {
        if (CMCompanyName == $.trim($(this).text())) {
            i++;
        }
    });

    var CompanyEntryType = $('#CompanyEntryType').val();

    if (CompanyEntryType == 0) {

        if (i != 0) {
            alert('You Have Already Entered This Company Name');
            return false;
        }
    }

    //var CMContactName = $.trim($("#CMContactName").val());
    //if (CMContactName == null || CMContactName == "") {
    //    alert('Please Enter Company Contact Name');
    //    return false;
    //}
    //var CMContactPhoneNo = $.trim($("#CMContactPhoneNo").val());
    //if (CMContactPhoneNo == null || CMContactPhoneNo == "") {
    //    alert('Please Enter Contact Phone No.');
    //    return false;
    //}
    //var CMContactAddress = $.trim($("#CMContactAddress").val());
    //if (CMContactAddress == null || CMContactAddress == "") {
    //    alert('Please Enter Contact Address');
    //    return false;
    //}
    var CMEmailAddress = $.trim($("#emailid").val());
    if (CMContactAddress == null || CMContactAddress == "") {
        alert('Please Enter Email Address');
        return false;
    }


    var InrRatio = $.trim($("#txtRatio").val());
    if (InrRatio == null || InrRatio == "") {
        alert('Please Enter INR Points Ratio');
        return false;
    }


    var txtMaxPoint = $.trim($("#txtMaxPoint").val());
    if (txtMaxPoint == null || txtMaxPoint == "") {
        alert('Please Enter Max. Point Range');
        return false;
    }

    var txtMinPoint = $.trim($("#txtMinPoint").val());
    if (txtMinPoint == null || txtMinPoint == "") {
        alert('Please Enter Min. Point Range');
        return false;
    }

    var txtrooturl1 = $.trim($("#txtrooturl1").val());
    if (txtrooturl1 == null || txtrooturl1 == "") {
        alert('Please Enter Root URL 1');
        return false;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postCompanyEntry/',
        type: 'POST',
        data: vm_CompanyViewModel,
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            var ddlCompMode = $("#CompanyEntryType");
            PopulateCompany(ddlCompMode);
            vm_CompanyViewModel = new CompanyViewModel();
            ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}


/*********************Store / Category Maintenance start***********************/

/******************************Category ViewModel*******************************/
var vm_CategoryViewModel;


function CategoryViewModel() {

    //Make the self as ҴhisҠreference

    var self = this;




    //Declare observable which will be bind with UI  
    self.id = ko.observable(0);
    self.Name = ko.observable("");
    self.displayname = ko.observable("");
    self.description = ko.observable("");

    self.ParentId = ko.observable(0);
    self.special = ko.observable("");

    self.active = ko.observable(1);
    self.filters = ko.observable("");
    self.image_urls = ko.observable("");
    self.video_urls = ko.observable("");
    self.ad_urls = ko.observable("");
    self.number_items = ko.observable("");
    //self.store_defn = ko.observableArray(st_dt);
    //self.store_defn = ko.observableArray([]);


    //The Object which stored data entered in the observables

    var CategoryData = {
        id: self.id,
        Name: self.Name,
        displayname: self.displayname,
        description: self.description,

        ParentId: self.ParentId,
        special: self.special,
        active: self.active,
        filters: self.filters,
        image_urls: self.image_urls,
        video_urls: self.video_urls,
        ad_urls: self.ad_urls,
        number_items: self.number_items
        //store_defn: self.store_defn
    };
}

/************************************************************************************************************/

/********************************************Warehouse Viewmodel************************************************/

function WarehouseViewmodel() {
    var self = this;

    self.wirehouse_name = ko.observable("");
    self.owner_name = ko.observable("");
    self.owner_email = ko.observable("");
    self.wirehouse_address = ko.observable("");


    var WarehouseData = {

        wirehouse_name: self.wirehouse_name,
        owner_name: self.owner_name,
        owner_email: self.owner_email,
        wirehouse_address: self.wirehouse_address

    };

}


/***************************************************************************************************************/

/************************************************** Populate Warehouse List************************************/

function PopulateWarehouseList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/getwarehouse',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            ddlID.append($("<option></option>").val("0").text("New Wirehouse"));
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.warehouse_name).text(value.warehouse_name));
            });
        }
    });


}

/**************************************************End of Warehouse*****************************************************/

/********************************************************* Get Warehouse Data***************************************************************/

function getwarehouse_details()
{
    var warehouse_name = $('#warehouse_list').val();

    if (warehouse_name == 0) {

        clearwarehousefields();
        $("#txtware_name").prop('disabled', false);
    }
    else {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/get/warehouse/details/{warehouse_name}',
            type: 'GET',
            data: { warehouse_name: warehouse_name },
            dataType: 'json',
            success: function (result) {
                $("#txtware_name").prop('disabled', true);
                $("#txtware_name").val(warehouse_name);
                $("#txtware_address").val(result[0].warehouse_address);
                $("#txtowner_name").val(result[0].owner_name);
                $("#txtemail").val(result[0].owner_email);

                // ddlID = $("#warehouse_list");
                // PopulateWarehouseList(ddlID);

            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }

}



/************************************************************************************************************************/


/**************************************************Save Warehouse*********************************************************/

function SaveWarehouse() {

    var warehouse = $('#warehouse_list').val();        
    var warhouse_name = $("#txtware_name").val();
    var warhouse_address = $("#txtware_address").val();
    var owner_name = $("#txtowner_name").val();
    var owner_email = $("#txtemail").val();


    if (warehouse == 0) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/warehouse/insert',
            type: 'POST',
            //data: vm_ProductViewModel,
            data: { warehouse_name: warhouse_name, warehouse_address: warhouse_address, owner_name: owner_name, owner_email: owner_email },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Saved Successfully.');
                clearwarehousefields();
                ddlID = $("#warehouse_list");
                PopulateWarehouseList(ddlID);
           
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/warehouse/update',
            type: 'POST',
            data: { warehouse_name: warhouse_name, warehouse_address: warhouse_address, owner_name: owner_name, owner_email: owner_email },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Saved Successfully.');
                clearwarehousefields();
                var ddlID = $("#warehouse_list");
                PopulateWarehouseList(ddlID);
       
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    }
}


/************************************************************************************************************/


/***************Function: Saving Company Data******************/


/*********************Populate Company Names*********************************/


function PopulateCategoryList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_list',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });
            loadingcut();
        }
    });
}



/*********************Populate Brand List*********************************/


function PopulateBrandList(ddlID, flag) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/brand_list/{Flag}',
        type: "GET",
        data: { Flag: flag },
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.name).text(value.name));
            });
            //loadingcut();
        }
    });
}



function PopCatFilterList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_filter_list',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });

            loadingcut();
        }
    });
}

/*********Populate Added Category Filter In Grid*********/


function PopCategoryAddedFilter() {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/insert_catagory/',
        type: 'POST',
        data: {},
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            $("#CatalogGrid").GridUnload();
            SetupGrid(result);

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}

function SaveCategory() {

    if (getCategoryData() == false) {
        return false;
    }

    var CatName = $.trim($("#CategoryName").val());
    var ParentCatName = $.trim($('#ddlParentCat :selected').text());


    var i = 0;

    var cat_id = $('#ddlCategory').val();

    if (cat_id == 0) {

        $('select#ddlCategory').find('option').each(function () {
            var CategoryNames = $.trim($(this).text());
            var CategoryNamesPart = CategoryNames.toString().split('-');
            var LevelVal = $('input:radio[name=level]:checked').val();
            if (LevelVal == 1) {
                if (CatName.toString().toLowerCase() == $.trim(CategoryNamesPart[0].toString().toLowerCase())) {
                    i++;
                }

            } else if (LevelVal == 2) {

                if (CategoryNamesPart.length == 2) {
                    if (ParentCatName.toString().toLowerCase() == $.trim(CategoryNamesPart[0].toString().toLowerCase()) && CatName.toString().toLowerCase() == $.trim(CategoryNamesPart[1].toString().toLowerCase())) {
                        i++;
                    }
                }

            } else if (LevelVal == 3) {
                if (CategoryNamesPart.length == 3) {
                    if (ParentCatName.toString().toLowerCase() == $.trim(CategoryNamesPart[0].toString().toLowerCase()) + '-' + $.trim(CategoryNamesPart[1].toString().toLowerCase()) && CatName.toString().toLowerCase() == $.trim(CategoryNamesPart[2].toString().toLowerCase())) {
                        i++;
                    }
                }
            }

        });


        if (i != 0) {
            alert('You Have Already Entered This Category Name');
            return false;
        }
    }


    $("#btnCatSave").prop('disabled', true);
    $('#btnAddAnotherCat').prop('disabled', false);

    loadingadd();

    var category_date = getCategoryData();



    if (cat_id == 0) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/insert_catagory/',
            type: 'POST',
            //data: vm_CategoryViewModel,
            data: category_date,
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Saved Successfully.');

                var ddlCategory = $("#ddlCategory");
                PopulateCategoryList(ddlCategory);


            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/update_catagory/',
            type: 'POST',
            //data: vm_CategoryViewModel,
            data: category_date,
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Updated Successfully.');

            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    }
}


function getCategoryData() {

    var cat_id = $('#ddlCategory').val();

    var CategoryName = $.trim($("#CategoryName").val());
    if (CategoryName == null || CategoryName == "") {
        alert('Please Enter Category Name');
        return false;
    }
    var CatDispName = $.trim($("#CatDispName").val());
    if (CatDispName == null || CatDispName == "") {
        alert('Please Enter Category Display Name');
        return false;
    }
    var txtCatDesc = $.trim($("#txtCatDesc").val());
    if (txtCatDesc == null || txtCatDesc == "") {
        alert('Please Enter Category Description');
        return false;
    }
    var ddlParentCat = $("#ddlParentCat").val();
    if (!$("input[name='level']").is(':checked')) {
        alert('Please Select Category Level');
        return false;
    }
    else {
        var val = $('input:radio[name=level]:checked').val();
        if (val == "2" || val == "3") {
            if (ddlParentCat == "0") {
                alert('Please Select Parent Category');
                return false;
            }
        }
    }
    if ($('#chkCateStatus').is(':checked')) {
        var cat_active = 0;
    }
    else {
        var cat_active = 1;
    }
    var txtSpecialStore = "";
    var StoreStartDate = "";
    var StoreEndDate = "";
    if ($('#chkSplStore').is(':checked')) {
        txtSpecialStore = $.trim($("#txtSpecialStore").val());
        if (txtSpecialStore == null || txtSpecialStore == "") {
            alert('Please Enter Special Store Name');
            return false;
        }
        StoreStartDate = $.trim($("#StoreStartDate").val());
        if (StoreStartDate == null || StoreStartDate == "") {
            alert('Please Select Start Date');
            return false;
        }
        StoreEndDate = $.trim($("#StoreEndDate").val());
        if (StoreEndDate == null || StoreEndDate == "") {
            alert('Please Select End Date');
            return false;
        }
        ddlParentCat = "0";
    }

    var val = $('input:radio[name=level]:checked').val();
    var txtShipping = "";
    var txtMinShipDays = 0;
    var txtMaxShipDays = 0;

    if (val == "3") {
        txtShipping = $.trim($("#txtShipping").val());
        if (txtShipping == null || txtShipping == "") {
            alert('Please Enter Shipping');
            return false;
        }

        txtMinShipDays = $.trim($("#txtMinShipDays").val());
        if (txtMinShipDays == null || txtMinShipDays == "") {
            alert('Please Enter Minimum Shipping Days');
            return false;
        }


        txtMaxShipDays = $.trim($("#txtMaxShipDays").val());
        if (txtMaxShipDays == null || txtMaxShipDays == "") {
            alert('Please Enter Maximum Shipping Days');
            return false;
        }

        if ((txtMinShipDays * 1) > (txtMaxShipDays * 1)) {
            alert('Minimum Shipping Days Should Be Less Than Maximum Shipping Days');
            return false;
        }



    }

    var number_items = "";
    var store_date = new Array();
    var storedata = {
        end_date: StoreStartDate,
        start_date: StoreEndDate
    }
    store_date[0] = storedata;

    return {
        id: cat_id,
        Name: CategoryName,
        displayname: CatDispName,
        description: txtCatDesc,
        ParentId: ddlParentCat,
        special: self.special,
        active: cat_active,
        number_items: number_items,
        special: txtSpecialStore,
        store_defn: store_date,
        Shipping: txtShipping,
        min_ship: txtMinShipDays,
        max_ship: txtMaxShipDays
    };
}

function ClearCateFields() {

    $('#ddlCategory').val(0);
    $("#CategoryName").val('');
    $("#CatDispName").val('');
    $("#txtCatDesc").val('');
    $("#ddlParentCat").find('option').remove().end();
    jQuery("#tdLevel input:radio").removeAttr('disabled');
    $('#ddlParentCat').attr('disabled', false);
    $('[name=level]').prop('checked', false);
    $('#chkCateStatus').prop('checked', false);
    $('#chkSplStore').prop('checked', false);
    $("#txtSpecialStore").val('');
    $("#StoreStartDate").val('');
    $("#StoreEndDate").val('');
    $('#chkSplStore').attr('disabled', false);
    $("#txtShipping").val('');
    $(".CateFields").val('');

}

function PopCatData(data) {

    $("#CategoryName").val(data.Name);
    $("#CatDispName").val(data.displayname);
    $("#txtCatDesc").val(data.description);

    if (data.Shipping != "BsonNull") {
        $("#txtShipping").val(data.Shipping);
    }
    else {
        $("#txtShipping").val('');
    }

    if (data.min_ship != "0") {
        $("#txtMinShipDays").val(data.min_ship);
    }
    else {
        $("#txtMinShipDays").val('');
    }

    if (data.max_ship != "0") {
        $("#txtMaxShipDays").val(data.max_ship);
    }
    else {
        $("#txtMaxShipDays").val('');
    }



    if (data.active == 0) {
        $('#chkCateStatus').prop('checked', true);
    } else {
        $('#chkCateStatus').prop('checked', false);
    }
    if (data.special != "BsonNull") {
        $('#chkSplStore').prop('checked', true);
        $("#txtSpecialStore").val(data.special);
        $("#StoreStartDate").val(data.store_defn[0].start_date);
        $("#StoreEndDate").val(data.store_defn[0].end_date);
    } else {
        $('#chkSplStore').prop('checked', false);
        $("#txtSpecialStore").val('');
        $("#StoreStartDate").val('');
        $("#StoreEndDate").val('');
    }
    $("#ddlParentCat").val(data.ParentId);
    $('#chkSplStore').attr('disabled', true);

    loadingcut();

}




/***************************************************************/


function getCategoryDetails(CallFrom) {

    if (CallFrom == "1") {

        $("#btnCatSave").prop('disabled', false);
        $('#btnAddAnotherCat').prop('disabled', true);

        var Category_ID = $('#ddlCategory').val();
        var CatDiv = $("#CatLink");
        if (Category_ID == 0 || Category_ID == null) {
            CatDiv.html("");
        }
        else {
            'javascript:window.open(' + url + ',"_self")'
            var url = rootCaturl + Category_ID;
            var row = $('<a></a>').html('<a href=javascript:window.open("' + url + '","_self")>' + url + '</a>');
            CatDiv.html("");
            CatDiv.append(row);
        }
    }
    else if (CallFrom == "2") {

        var Category_ID = $('#CFCategoryList').val();
        var CatFilterLink = $("#CatFilterLink");
        if (Category_ID == 0 || Category_ID == null) {
            CatFilterLink.html("");
            $('#CFCategoryName').val('');
            $('#CFCatDisplayName').val('');
            $("#FilterList").GridUnload();
        }
        else {
            var url = rootCaturl + Category_ID;
            //var row = $('<a></a>').html('<a href="' + url + '">' + url + '</a>');
            var row = $('<a></a>').html('<a href=javascript:window.open("' + url + '","_self")>' + url + '</a>');

            CatFilterLink.html("");
            CatFilterLink.append(row);
        }
    }


    if (Category_ID != 0 && Category_ID != null) {

        loadingadd();
        $("#btnUpldImgVid").prop('disabled', false);

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/cat_details/{cat_id}',
            type: 'GET',
            data: { cat_id: Category_ID },
            dataType: 'json',
            cache: false,
            success: function (result) {
                if (CallFrom == "1") {
                    var level_value = result[0].ParentId;

                    $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', true);
                    if (level_value == "0") {
                        $('[name=level][value="1"]').prop('checked', true);
                        PopParentCatList();
                    }
                    else {
                        var arr = result[0].ParentId.split('.');
                        if (arr[1] == "0") {
                            $('[name=level][value="2"]').prop('checked', true);
                            PopParentCatList();
                        }
                        else {
                            $('[name=level][value="3"]').prop('checked', true);

                            $("#txtShipping , #txtMinShipDays , #txtMaxShipDays").prop('disabled', false);
                            PopParentCatList();
                        }
                    }
                    //vm_CategoryViewModel = result[0];
                    //setTimeout(function () { ko.applyBindings(vm_CategoryViewModel, document.getElementById("Store-Category-Maintenance")); }, 600);
                    setTimeout(function () { PopCatData(result[0]); }, 2000);

                    $('#addressSection input[type=radio]').attr('disabled', false);
                    jQuery("#tdLevel input:radio").attr('disabled', 'disabled');
                    $('#ddlParentCat').attr('disabled', true);


                }
                else if (CallFrom == "2") {


                    $('#CFCategoryName').val(result[0].Name);
                    $('#CFCatDisplayName').val(result[0].displayname);
                    loadingcut();
                    PopulateFilterGrid(result[0].filters);
                }

            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {

        jQuery("#tdLevel input:radio").removeAttr('disabled');
        $('#ddlParentCat').attr('disabled', false);
        $('[name=level]').prop('checked', false);
        $("#ddlParentCat").find('option').remove().end();
        $("#btnUpldImgVid").prop("disabled", true);


        //vm_CategoryViewModel = new CategoryViewModel();
        //ko.applyBindings(vm_CategoryViewModel, document.getElementById("Store-Category-Maintenance"));
        ClearCateFields();
    }


}



/*****************Product / Catalog Maintenance start**********************/

/******************************Category ViewModel*******************************/

var vm_ProductViewModel;

function ProductViewModel() {

    //Make the self as ҴhisҠreference

    var self = this;

    //Declare observable which will be bind with UI

    self.id = ko.observable("");
    self.sku = ko.observable("");
    self.Name = ko.observable("");
    self.display_name = ko.observable("");
    self.description = ko.observable("");
    self.shortdesc = ko.observable("");
    self.mrp = ko.observable("");
    self.list = ko.observable("");
    self.min = ko.observable("");
    self.discount = ko.observable("");
    self.stock = ko.observable("");
    self.price = ko.observable("");
    self.IS_ACTIVE = ko.observable(false);
    //The Object which stored data entered in the observables

    var ProductData = {

        id: self.id,
        sku: self.sku,
        Name: self.Name,
        display_name: self.display_name,
        description: self.description,
        shortdesc: self.shortdesc,
        mrp: self.mrp,
        list: self.list,
        min: self.min,
        discount: self.discount,
        stock: self.stock,
        price: self.price,
        IS_ACTIVE: self.IS_ACTIVE
    };
}



/***************Function: Populate Product List ******************/

function PopulateProductList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/product_list',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });

            loadingcut();
        }
    });

}

/***************Function: Saving Company Data******************/

function SaveProduct() {

    var ID = $("#txtProdId").val();
    var SKU = $("#txtProdSKU").val();
    var PRODUCT_NAME = $("#txtProductName").val();

    var BRAND = $("#ddlBrnad").val();
    var PROD_DESC = $("#txtProdDesc").val();
    var PROD_SHRT_DESC = $("#txtProdShrtDesc").val();

    var MIN_ORD_QTY = $.trim($("#txtMinOrdQty").val());

    //var MRP = $("#txtMRP").val() * 1;
    //var LIST_PRICE = $("#txtListPrice").val() * 1;
    //var DISCOUNT = $("#txtDiscount").val() * 1;
    //var MIN_SALE = $("#txtMinSalePrice").val() * 1;

    //var STOCK = $("#txtStock").val();

    //var cal_price = {
    //    mrp: MRP,
    //    list: LIST_PRICE,
    //    discount: DISCOUNT,
    //    min: MIN_SALE,
    //    final_offer: LIST_PRICE,
    //    final_discount: DISCOUNT
    //}

    var txtReorderLevel = $("#txtReorderLevel").val();

    if (ID == '' || ID == '0') {
        alert('Please provide the Product ID');
        return false;
    }
    else if (PRODUCT_NAME == '') {
        alert('Please provide the Product Name');
        return false;
    }
    else if (BRAND == '' || BRAND == null) {
        alert('Please select a brand');
        return false;
    }
    else if (MIN_ORD_QTY == '' || MIN_ORD_QTY == null) {
        alert('Please enter minimum order quantity');
        return false;
    }
        //else if (MRP == '' || MRP == null) {
        //    alert('Please enter MRP');
        //    return false;
        //}
        //else if (LIST_PRICE == '' || LIST_PRICE == null) {
        //    alert('Please enter List Price');
        //    return false;
        //}

        //else if (DISCOUNT == '' || DISCOUNT == null) {
        //    alert('Please enter Discount');
        //    return false;
        //}
    else if (PROD_SHRT_DESC == '' || PROD_SHRT_DESC == null) {
        alert('Please enter Product Short Description');
        return false;
    }
    else if (PROD_DESC == '' || PROD_DESC == null) {
        alert('Please enter Product Description');
        return false;
    }

    else if (PROD_DESC.length < 100) {
        alert('Product Description Must Be Atleast 500 Characters');
        return false;

    }
    else if (PROD_DESC.length > 1500) {
        alert('Product Description Must Be Less Than 1500 Characters');
        return false;
    }

    var EXPRESS = "";
    var HAVE_CHILD = "";

    if ($("#chkHaveChild").is(':checked')) {
        var HAVE_CHILD = 1;
        EXPRESS = 1;
    }
    else {
        var HAVE_CHILD = 0;
        //if (STOCK == '' || STOCK == null) {
        //    alert('Please Enter Stock');
        //    return false;
        //}
        if ($('#chkExpress').is(':checked')) {
            EXPRESS = 1;
        }
        else {
            EXPRESS = 0;
        }
    }

    if (txtReorderLevel == '' || txtReorderLevel == null) {
        alert('Please Enter Re-Order Level');
        return false;
    }




    var prod_id = $('#ddlProduct').val();
    if (prod_id == 0) {

        var i = 0;
        var vProductName = $('#txtProductName').val();
        $('select#ddlProduct').find('option').each(function () {
            if (vProductName.toString().toLowerCase() == $.trim($(this).text().toString().toLowerCase())) {
                i++;
            }
        });


        if (i != 0) {
            alert('You Have Already Entered This Product Name');
            return false;
        }


    }



    var Prod_Status = "";

    if ($('#chkProdStatus').is(':checked')) {
        Prod_Status = 0;
    }
    else {
        Prod_Status = 2;
    }


    loadingadd();

    if (prod_id == 0) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/insert_product/',
            type: 'POST',
            //data: vm_ProductViewModel,
            data: { id: ID, sku: SKU, Name: PRODUCT_NAME, active: Prod_Status, brand: BRAND, min_order_qty: MIN_ORD_QTY, description: PROD_DESC, shortdesc: PROD_SHRT_DESC, have_child: HAVE_CHILD, Express: EXPRESS, Reorder_Level: txtReorderLevel, parent_prod: "0" },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Saved Successfully.');
                ClearProdFields();
                ddlProduct = $("#ddlProduct");
                PopulateProductList(ddlProduct);
                $("#txtProdId").prop('disabled', false);
                $("#txtProdSKU").prop('disabled', false);
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/update_product/',
            type: 'POST',
            data: { id: ID, sku: SKU, Name: PRODUCT_NAME, active: Prod_Status, brand: BRAND, min_order_qty: MIN_ORD_QTY, description: PROD_DESC, shortdesc: PROD_SHRT_DESC, have_child: HAVE_CHILD, Express: EXPRESS, Reorder_Level: txtReorderLevel, parent_prod: "0" },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Saved Successfully.');
                ClearProdFields();
                var ddlProduct = $("#ddlProduct");
                PopulateProductList(ddlProduct);
                $("#txtProdId").prop('disabled', false);
                $("#txtProdSKU").prop('disabled', false);
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    }



}

function CalDiscount() {

    var MRP = $("#txtMRP").val() * 1;
    var LIST_PRICE = $("#txtListPrice").val() * 1;
    var CAL = LIST_PRICE / MRP;
    var DISCOUNT = 100 * (1 - CAL);
    $("#txtDiscount").val(Math.round(DISCOUNT * 100) / 100);


}
function CalListPrice() {

    var MRP = $("#txtMRP").val() * 1;
    var DISCOUNT = $("#txtDiscount").val() * 1;
    var CAL_AMT = (MRP * DISCOUNT) / 100;
    var LIST_PRICE = MRP - CAL_AMT;
    $("#txtListPrice").val(Math.round(LIST_PRICE * 100) / 100);


}


function getProductDetails(CallFrom) {

    loadingadd();

    if (CallFrom == "1") {
        var Product_ID = $('#ddlProduct').val();

        if (Product_ID != "0") {

            chkChildProdExist(Product_ID);

            var ProductLink = $("#ProductLink");
            var url = rootProdurl + Product_ID;
            var row = $('<a></a>').html('<a href=javascript:window.open("' + url + '","_self")>' + url + '</a>');
            ProductLink.html("");
            ProductLink.append(row);

            $("#btnUpldPrdImg").prop("disabled", false);
            $("#btnBulkProdPrice").prop("disabled", false);
            $("#btnBulkProdStock").prop("disabled", false);

            $.support.cors = true;
            $.ajax({
                url: rooturl + '/category/product_details/{prod_id}',
                type: 'GET',
                data: { prod_id: Product_ID },
                dataType: 'json',
                success: function (result) {

                    PopBulkPriceGrid(result[0].price);
                    PopBulkStockGrid(result[0].stock);

                    $("#txtProdId").val(result[0].id);
                    $("#txtProdSKU").val(result[0].sku);
                    $("#txtProductName").val(result[0].Name);
                    $("#ddlBrnad").val(result[0].brand);
                    $("#txtProdDesc").val(result[0].description);
                    $("#txtProdShrtDesc").val(result[0].shortdesc);
                    $("#txtMinOrdQty").val(result[0].min_order_qty);
                    //$("#txtMRP").val(result[0].price.mrp);
                    //$("#txtListPrice").val(result[0].price.list);
                    //$("#txtDiscount").val(result[0].price.discount);
                    //$("#txtMinSalePrice").val(result[0].price.min);

                    $("#txtReorderLevel").val(result[0].Reorder_Level);

                    $("#txtProdId").prop('disabled', true);
                    $("#txtProdSKU").prop('disabled', true);

                    if (result[0].active == 0) {
                        $('#chkProdStatus').prop('checked', true);
                    } else {
                        $('#chkProdStatus').prop('checked', false);
                    }

                    if (result[0].have_child == "0") {
                        $('#chkHaveChild').prop('checked', false);
                        //$("#txtStock").prop('disabled', false);
                        $("#btnBulkProdStock").prop("disabled", false);
                        $("#chkExpress").prop('disabled', false);
                    } else {
                        $('#chkHaveChild').prop('checked', true);
                        //$("#txtStock").prop('disabled', true);
                        $("#btnBulkProdStock").prop("disabled", true);
                        $("#chkExpress").prop('disabled', true);
                    }

                    //if (result[0].stock == 0) {
                    //    $("#txtStock").val('');
                    //} else {
                    //$("#txtStock").val(result[0].stock);
                    //}

                    if (result[0].Express == "0" || result[0].Express == "BsonNull" || result[0].Express == null) {
                        $('#chkExpress').prop('checked', false);
                    } else {
                        $('#chkExpress').prop('checked', true);
                    }
                    //$('#chkExpress').prop('disabled', true);
                    loadingcut();
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }
        else {


            ClearProdFields();
            $("#txtProdId").prop('disabled', false);
            $("#txtProdSKU").prop('disabled', false);
            $("#chkHaveChild").prop('disabled', false);
            loadingcut();
        }
    }
    else if (CallFrom == "2") {
        var Product_ID = $('#PCAProdList').val();

        $('#DynamicChkBox').find('input[type=checkbox]:checked').removeAttr('checked');

        if (Product_ID != "0") {

            $.support.cors = true;
            $.ajax({
                url: rooturl + '/category/product_details/{prod_id}',
                type: 'GET',
                data: { prod_id: Product_ID },
                dataType: 'json',
                success: function (result) {
                    $("#PCAProdName").val(result[0].Name);
                    $("#PCAProdDesc").val(result[0].description);

                    //var selectedValues = new Array();
                    //for (var i = 0; i < result[0].cat_id.length; i++) {
                    //    alert(result[0].cat_id[i].cat_id);
                    //    selectedValuesSavePCA[i] = result[0].cat_id[i].cat_id;
                    //}
                    //$(".ClsSelect").val(selectedValues);


                    $('#DynamicChkBox input[type=checkbox]').each(function () {
                        var attrib = $(this).attr("value");
                        for (var x = 0; x < result[0].cat_id.length; x++) {
                            if (result[0].cat_id[x].cat_id == attrib) {
                                //$(this).attr("checked", "checked");
                                $(this).prop('checked', true);
                            }
                        }
                    });



                    $("#PCAParentCategory").val(result[0].parent_cat_id);
                    loadingcut();
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }
        else {

            $("#PCAProdName").val('');
            $("#PCAProdDispName").val('');
            $("#PCAProdDesc").val('');
            loadingcut();
        }

    }

}





function ClearProdFields() {

    $("#ddlProduct").val(0);
    $("#CountCharacter").text('');
    $("#txtProdId").val('');
    $("#txtReorderLevel").val('');

    $("#txtProdSKU").val('');
    $("#txtProductName").val('');
    $("#ddlBrnad").val('');
    $("#txtProdDesc").val('');
    $("#txtProdShrtDesc").val('');
    $("#txtMinOrdQty").val(10);

    //$("#txtMRP").val('');
    //$("#txtListPrice").val('');
    //$("#txtDiscount").val('');
    //$("#txtMinSalePrice").val('');
    $('#chkProdStatus').prop('checked', false);
    $('#chkHaveChild').prop('checked', false);
    $('#chkExpress').prop('checked', false);
    //$("#txtStock").prop('disabled', false);
    $("#btnBulkProdStock").prop("disabled", false);
    $("#chkExpress").prop('disabled', false);
    //$("#txtStock").val('');
    var ProductLink = $("#ProductLink");
    ProductLink.html("");
    $("#btnUpldPrdImg").prop("disabled", true);
    $("#btnBulkProdPrice").prop("disabled", true);
    $("#btnBulkProdStock").prop("disabled", true);


}

function clearwarehousefields()
{
    $("#txtware_name").val('');
    $("#txtware_address").val('');
    $("#txtowner_name").val('');
    $("#txtemail").val('');

}



/****************** Populate Category List To copy Filter ************************/
function PopCatListToCopy(ddlID) {

    var OrgCat_ID = $("#CFCategoryList").val();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_list_to_copy/{cat_id}',
        type: "GET",
        data: { cat_id: OrgCat_ID },
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });
        }
    });
}


function PopFilters() {

    var Category_ID = $('#CFCatList').val();
    var ddlID = $('#ddlCatFilters');

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_filters_details/{cat_id}',
        type: 'GET',
        data: { cat_id: Category_ID },
        dataType: 'json',
        success: function (result) {
            var tableSrc = result[0].filters;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.val).text(value.name));
            });
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}




/***********Populate All Category List For Selection For A Product**********/

function PopAllCategoryList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_list_for_product',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });
        }
    });

}

/***********Populate All Category List For Selection For A Product**********/

function PopAllCategoryTree(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_list_tree',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            //ddlID.find('option').remove().end();
            ddlID.html("");
            $.each(tableSrc, function (key, value) {
                //ddlID.append($("<option></option>").val(value.id).text(value.Name));
                //$('#DynamicChkBox').append('<input type="checkbox" value="' + value.id + '" /> ' + value.Name + '<br />');
                ddlID.append('<input type="checkbox" value="' + value.id + '" /> ' + value.Name + '<br />');
            });
            //AddSpace();
            //setTimeout(function () { AddSpace(); }, 100);
        }
    });
}

function AddSpace() {
    var dropDownLists = $("#PCACategoryList");
    for (var i = 0; i < dropDownLists[0].length; i++) {
        var oDDL = dropDownLists[0];
        for (var j = 0; j < oDDL.options.length; j++) {
            oDDL.options[j].innerHTML = oDDL.options[j].innerHTML.replace("|", "</br>");
        }
    }

}

/************Save Product Category Association ***************************/

function SavePCA() {


    if (getPCAData() == false)
        return false;

    loadingadd();

    var PCA_data = getPCAData();

    $.support.cors = true;
    $.ajax({

        url: rooturl + '/category/admin/update_prod_cat/',
        type: 'POST',
        data: PCA_data,
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}

function getPCAData() {

    var Product_ID = $("#PCAProdList").val();
    //var Cat_ID_List = $("#PCACategoryList").val();
    //if (Cat_ID_List == "" || Cat_ID_List == null) {
    //    alert('Please Select category List');
    //    return false;
    //}
    var arrCat_ID = new Array();
    var i = 0;
    $('#DynamicChkBox input:checked').each(function () {
        arrCat_ID.push({ cat_id: $(this).attr('value') });
        i++;
    });
    if (i == 0) {
        alert('Please Select category List');
        return false;
    }

    var Parent_Cat_ID = $("#PCAParentCategory").val();
    if (Parent_Cat_ID == "0" || Parent_Cat_ID == null) {
        alert('Please Select Primary Category');
        return false;
    }



    return {
        id: Product_ID,
        cat_id: arrCat_ID,
        parent_cat_id: Parent_Cat_ID
    };

}

/*************Populate Category List For Multiple Selection For Product **************/




function PopFilterGrid() {

    var grid = jQuery('#FilterList');
    var GridData = grid.getRowData();
    var txtFilterName = $.trim($("#txtFilterName").val());
    if (txtFilterName == '' || txtFilterName == null) {
        alert('Please Enter Filter Name ');
        return false;
    }

    for (var j = 0; j < GridData.length; j++) {
        if (GridData[j].name == txtFilterName) {
            alert(' Filter Name Is Already Added ');
            return false;
        }
    }

    var tableSrc;
    if (GridData.length == 0) {
        var FirstGridData = new Array();
        if (FilterGridData() == false)
            return false;

        FirstGridData[0] = FilterGridData();
        tableSrc = FirstGridData;
    }
    else {
        var AllGridData = new Array();
        for (var i = 0; i < GridData.length; i++) {
            AllGridData[i] = GridData[i];
        }
        if (FilterGridData() == false)
            return false;
        AllGridData[AllGridData.length] = FilterGridData();
        tableSrc = AllGridData;
    }

    $("#FilterList").GridUnload();
    PopulateFilterGrid(tableSrc);
    $("#txtFilterName").val('');
}

function FilterGridData() {
    var txtFilterName = $("#txtFilterName").val();
    return {
        name: txtFilterName
    };
}

function DeleteFilter(FilterName) {


    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {

        var FilterVal = new Array();
        FilterVal[0] = null;
        var Data = {
            name: FilterName,
            values: FilterVal
        }
        var FilterData = new Array();
        FilterData[0] = Data;

        var Category_ID = $("#CFCategoryList").val();

        $.support.cors = true;
        $.ajax({

            url: rooturl + '/category/admin/del_cat_filter/',
            type: 'POST',
            data: { id: Category_ID, filters: FilterData },
            dataType: 'json',
            success: function (result) {
                getCategoryDetails(2);

            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });


    }


}

function AddFilterValue(FilterName) {
    getCatFiltersDetails(FilterName);
    $('#FilterName').text(FilterName);
    $('#AddFilterValue').removeClass('hide');
}
function AddFilterValues() {

    var FilterValues = $("#FilterValues");
    var txtValue = $('#txtValue').val();
    if (txtValue == "" || txtValue == null) {
        alert('Please Enter Filter Value');
        return false;
    }
    var PrevFilterValues = $('#FilterValues').val();
    var DataSrc = {
        id: txtValue,
        name: txtValue
    }
    var tableSrc = new Array();
    tableSrc[0] = DataSrc;

    $.each(tableSrc, function (key, value) {
        $("#FilterValues").append($("<option></option>").val(value.id).text(value.name));
    });

    $('#txtValue').val('');
}

function CopyFilterValue() {

    loadingadd();

    var FilterName = $("#FilterName").text();
    var FilterVal = new Array();
    var i = 0;
    $('#FilterValues').find('option').each(function () {
        FilterVal[i] = $(this).val();
        i++;
    });

    var FilterData = new Array();

    var Data = {
        name: FilterName,
        values: FilterVal
    }

    FilterData[0] = Data;

    var Category_ID = $("#CFCategoryList").val();

    $.support.cors = true;
    $.ajax({

        url: rooturl + '/category/admin/update_cat_filter/',
        type: 'POST',
        data: { id: Category_ID, filters: FilterData },
        dataType: 'json',
        success: function (result) {

            $('#AddFilterValue').addClass('hide');
            loadingcut();
            alert('Value Added Successfully');

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function PopulateFilterGrid(tableSrc) {

    $("#FilterList").GridUnload();

    $("#FilterList").jqGrid({

        datastr: tableSrc,
        datatype: "local",
        height: 230,

        colNames: ['Filter Name', 'Action', 'Values', ''],
        colModel: [{ name: 'name', index: 'name', width: 100, align: "center" },
                   { name: 'name', index: 'FilterName', width: 100, align: "center", edittype: 'image', formatter: DelImageFormatter },
                   { name: 'name', index: 'FilterName', width: 100, align: "center", edittype: 'image', formatter: AddImageFormatter },
                   { name: 'name', index: 'FilterName', width: 100, align: "center", hidden: true }
        ],
        //multiselect: true,
        sortname: 'FilterName',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i>Added Filter List</div>"

    });

    function DelImageFormatter(cellvalue, options, rowObject) {

        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteFilter('" + rowObject.name + "');\" />";
    };
    function AddImageFormatter(cellvalue, options, rowObject) {

        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Add Values' src='images/AddItem.png' name='image'  " +
        "onclick=\"AddFilterValue('" + rowObject.name + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++)
        $("#FilterList").jqGrid('addRowData', i + 1, tableSrc[i]);

}

function getCatFiltersDetails(FilterName) {

    var Category_ID = $('#CFCategoryList').val();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/cat_details/{cat_id}',
        type: 'GET',
        data: { cat_id: Category_ID },
        dataType: 'json',
        success: function (result) {
            PopulateFilterValue(result[0].filters, FilterName);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function PopulateFilterValue(ddldata, FilterNAme) {

    for (var i = 0; i < ddldata.length; i++) {
        if (ddldata[i].name == FilterNAme) {
            var tblsrc = new Array();
            for (var j = 0; j < ddldata[i].values.length; j++) {
                var dataSrc = {
                    id: ddldata[i].values[j],
                    name: ddldata[i].values[j]
                }
                tblsrc[j] = dataSrc;
            }
        }
    }

    $("#FilterValues").find('option').remove().end();

    if (typeof tblsrc === "undefined") {

    }
    else {
        $.each(tblsrc, function (key, value) {
            $("#FilterValues").append($("<option></option>").val(value.id).text(value.name));
        });

    }

}

function OpenUploadImagePage(CallFrom) {

    UploadFor.value = CallFrom;

    if (CallFrom == 1) {
        $("#lblUpdFor").text('Category Name');
        $("#lblUpdForID").text('Category ID');

        $("#txtUpdForDesc").val($('#ddlCategory :selected').text());
        $("#txtUpdForID").val($('#ddlCategory :selected').val());

        PopImgVidAdUrls(1, $('#ddlCategory :selected').val());

    } else if (CallFrom == 2) {

        $("#lblUpdFor").text('Product Name');
        $("#lblUpdForID").text('Product ID');

        $("#txtUpdForDesc").val($('#ddlProduct :selected').text());
        $("#txtUpdForID").val($('#ddlProduct :selected').val());

        PopImgVidAdUrls(2, $('#ddlProduct :selected').val());
    }


    ClearUpldPage();

    $('.popup-main-container').addClass('hide');
    $('#UploadImageVideoUrl').removeClass('hide');
}





function PopImgVidAdUrls(From, ID) {

    if (From == 1) {
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/get_cat_imgvidurls/{cat_id}',
            type: 'GET',
            data: { cat_id: ID },
            dataType: 'json',
            success: function (result) {
                PopulateImgVidAd(result);
                Store_result = result;
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    } else if (From == 2) {
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/get_prod_imgvidurls/{prod_id}',
            type: 'GET',
            data: { prod_id: ID },
            dataType: 'json',
            success: function (result) {
                PopulateImgVidAd(result);
                Store_result = result;
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }

}


function PopulateImgVidAd(result) {

    if (result[0].image_urls.length > 0) {
        var ddlImageUrls = $("#ddlImageUrls");
        PopImgList(ddlImageUrls, result[0].image_urls);
    }
    if (result[0].video_urls.length > 0) {
        var ddlVideoUrls = $("#ddlVideoUrls");
        PopVidList(ddlVideoUrls, result[0].video_urls);
    }
    if (result[0].ad_urls.length > 0) {
        var ddlAdUrls = $("#ddlAdUrls");
        PopAdList(ddlAdUrls, result[0].ad_urls);
    }
}

function PopImgList(ddlID, result) {
    var tableSrc = result;
    ddlID.find('option').remove().end();
    ddlID.append($("<option></option>").val(0).text(''));
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.display_name).text(value.display_name));
    });
}
function PopVidList(ddlID, result) {
    var tableSrc = result;
    ddlID.find('option').remove().end();
    ddlID.append($("<option></option>").val(0).text(''));
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.display_name).text(value.display_name));
    });
}
function PopAdList(ddlID, result) {
    var tableSrc = result;
    ddlID.find('option').remove().end();
    ddlID.append($("<option></option>").val(0).text(''));
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.href).text(value.href));
    });
}

var Store_result;

function PopImgDtls() {

    UploadType.value = 1;

    var ddlImageUrls = $("#ddlImageUrls").val();

    $("#txtImageDisplayName").prop('disabled', true);

    for (var i = 0; i < Store_result[0].image_urls.length; i++) {
        if (ddlImageUrls == Store_result[0].image_urls[i].display_name) {
            $("#txtImageDisplayName").val(Store_result[0].image_urls[i].display_name);
            $("#txtImageLink").val(Store_result[0].image_urls[i].link);
            $("#txtImageThumbLink").val(Store_result[0].image_urls[i].thumb_link);
            $("#txtImageZoomLink").val(Store_result[0].image_urls[i].zoom_link);
        }
    }
}
function PopVidDtls() {

    UploadType.value = 1;
    var ddlVideoUrls = $("#ddlVideoUrls").val();
    $("#txtImageDisplayName").prop('disabled', true);

    for (var i = 0; i < Store_result[0].video_urls.length; i++) {

        if (ddlVideoUrls == Store_result[0].video_urls[i].display_name) {
            $("#txtVideoDisplayName").val(Store_result[0].video_urls[i].display_name);
            $("#txtVideoLink").val(Store_result[0].video_urls[i].link);
        }

    }

}
function PopAdDtls() {

    UploadType.value = 1;
    var ddlAdUrls = $("#ddlAdUrls").val();

    $("#txtImageDisplayName").prop('disabled', true);

    for (var i = 0; i < Store_result[0].ad_urls.length; i++) {

        if (ddlAdUrls == Store_result[0].ad_urls[i].href) {
            $("#txtAdhref").val(Store_result[0].ad_urls[i].href);
            $("#txtAdLink").val(Store_result[0].ad_urls[i].link);

        }

    }

}


function UploadImgVidLink() {

    var txtImageDisplayName = $("#txtImageDisplayName").val();

    var txtImageLink = $("#txtImageLink").val();
    var txtImageThumbLink = $("#txtImageThumbLink").val();
    var txtImageZoomLink = $("#txtImageZoomLink").val();


    var txtVideoDisplayName = $("#txtVideoDisplayName").val();


    var txtVideoLink = $("#txtVideoLink").val();
    var txtAdhref = $("#txtAdhref").val();

    var txtAdLink = $("#txtAdLink").val();

    if (UploadType.value == 0) {

        var i = 0;
        $('#ddlImageUrls').find('option').each(function () {
            if (txtImageDisplayName == $(this).val()) {
                i++;
            }
        });

        if (i != 0) {
            alert('You Have Already Entered This Image Display Name');
            return false;
        }

        var j = 0;
        $('#ddlVideoUrls').find('option').each(function () {
            if (txtVideoDisplayName == $(this).val()) {
                j++;
            }
        });

        if (j != 0) {
            alert('You Have Already Entered This Video Display Name');
            return false;
        }
        var k = 0;
        $('#ddlAdUrls').find('option').each(function () {
            if (txtAdhref == $(this).val()) {
                k++;
            }
        });

        if (k != 0) {
            alert('You Have Already Entered This Ad href');
            return false;
        }
    }


    var image_urls = {
        display_name: txtImageDisplayName,
        link: txtImageLink,
        thumb_link: txtImageThumbLink,
        zoom_link: txtImageZoomLink
    }
    var video_urls = {
        display_name: txtVideoDisplayName,
        link: txtVideoLink
    }
    var ad_urls = {
        href: txtAdhref,
        link: txtAdLink
    }
    loadingadd();

    var ImageArray = new Array();
    var VideoArray = new Array();
    var AdArray = new Array();
    ImageArray[0] = image_urls;
    VideoArray[0] = video_urls;
    AdArray[0] = ad_urls;


    if (UploadFor.value == "1") {

        var Category_Id = $("#ddlCategory").val();
        if (UploadType.value == 0) {

            $.support.cors = true;
            $.ajax({

                url: rooturl + '/category/admin/upload_cat_imgvid/',
                type: 'POST',
                data: { id: Category_Id, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
                dataType: 'json',
                success: function (result) {
                    loadingcut();
                    alert('Uploaded Successfully!');
                    PopImgVidAdUrls(1, $('#ddlCategory :selected').val());
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }
        else {

            $.support.cors = true;
            $.ajax({

                url: rooturl + '/category/admin/update_cat_imgvid/',
                type: 'POST',
                data: { id: Category_Id, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
                dataType: 'json',
                success: function (result) {
                    loadingcut();
                    alert('Updated Successfully!');
                    PopImgVidAdUrls(1, $('#ddlCategory :selected').val());
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });


        }
    }

    else if (UploadFor.value == "2") {

        var Product_ID = $("#ddlProduct").val();

        if (UploadType.value == 0) {

            $.support.cors = true;
            $.ajax({
                url: rooturl + '/category/admin/upload_prod_imgvid/',
                type: 'POST',
                data: { id: Product_ID, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
                dataType: 'json',
                success: function (result) {
                    loadingcut();
                    alert('Uploaded Successfully!');
                    PopImgVidAdUrls(2, $('#ddlProduct :selected').val());
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }

        else {

            $.support.cors = true;
            $.ajax({

                url: rooturl + '/category/admin/update_prod_imgvid/',
                type: 'POST',
                data: { id: Product_ID, image_urls: ImageArray, video_urls: VideoArray, ad_urls: AdArray },
                dataType: 'json',
                success: function (result) {
                    loadingcut();
                    alert('Updated Successfully!');
                    PopImgVidAdUrls(2, $('#ddlProduct :selected').val());
                },
                error: function (req, status, errorObj) {
                    alert('Error');
                }
            });
        }
    }

}

function ClearUpldPage() {

    UploadType.value = 0;
    $("#txtImageDisplayName").val('');
    $("#txtImageLink").val('');
    $("#txtImageThumbLink").val('');
    $("#txtImageZoomLink").val('');
    $("#txtVideoDisplayName").val('');
    $("#txtVideoLink").val('');
    $("#txtAdhref").val('');
    $("#txtAdLink").val('');

    $("#ddlImageUrls").val(0);
    $("#ddlVideoUrls").val(0);
    $("#ddlAdUrls").val(0);

    $("#txtImageDisplayName").prop('disabled', false);
    $("#txtVideoDisplayName").prop('disabled', false);
    $("#txtAdhref").prop('disabled', false);

}

function AddChildProduct() {

    var ddlParentProduct = $("#ddlParentProduct").val();
    ddlParentProduct = $.trim(ddlParentProduct);
    if (ddlParentProduct == 0 || ddlParentProduct == null) {
        alert('Please Select Parent Product');
        return false;
    }
    var txtChildProdSize = $("#txtChildProdSize").val();
    txtChildProdSize = $.trim(txtChildProdSize);
    if (txtChildProdSize == null || txtChildProdSize == "") {
        alert('Please Enter Size');
        return false;
    }
    var txtChildProdSKU = $("#txtChildProdSKU").val();
    txtChildProdSKU = $.trim(txtChildProdSKU);
    if (txtChildProdSKU == null || txtChildProdSKU == "") {
        alert('Please Enter SKU');
        return false;
    }

    //var txtChildProdStock = $("#txtChildProdStock").val();
    //txtChildProdStock = $.trim(txtChildProdStock);
    //if (txtChildProdStock == null || txtChildProdStock == "") {
    //    alert('Please Enter Stock');
    //    return false;
    //}

    var EXP = "";

    if ($("#ChildProdExpress").is(':checked')) {
        EXP = 1;
    }
    else {
        EXP = 0;
    }


    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/insert_child_product/',
        type: 'POST',
        data: { parent_prod: ddlParentProduct, size: txtChildProdSize, sku: txtChildProdSKU, Express: EXP },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            $("#txtChildProdSize").val('');
            $("#txtChildProdSKU").val('');
            //$("#txtChildProdStock").val('');
            $("#ChildProdExpress").prop('checked', false);
            getChildProduct();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}

function getChildProduct() {

    loadingadd();

    var ddlParentProduct = $("#ddlParentProduct").val();

    if (ddlParentProduct != "0") {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/get_child_product/{parent_prod}',
            type: 'GET',
            data: { parent_prod: ddlParentProduct },
            dataType: 'json',
            success: function (result) {
                PopChildProdGrid(result);
                loadingcut();
            },
            error: function (req, status, errorObj) {
                alert('Error');
                loadingcut();
            }
        });
    }
    else {
        $("#ChildProduct").GridUnload();
        loadingcut();
        $("#txtChildProdSize").val('');
        $("#txtChildProdSKU").val('');
    }
}

function PopChildProdGrid(tableSrc) {


    for (j = 0; j < tableSrc.length; j++) {

        var ChildProdStock = "Please Add Stock";

        if (tableSrc[j].stock.length > 0) {
            ChildProdStock = "Stock :";
            for (i = 0; i < tableSrc[j].stock.length; i++) {
                ChildProdStock = ChildProdStock + " " + tableSrc[j].stock[i].warehouse + " : " + tableSrc[j].stock[i].stock;
            }
        }

        tableSrc[j].stock = ChildProdStock
    }


    $("#ChildProduct").GridUnload();

    $("#ChildProduct").jqGrid({

        datastr: tableSrc,
        datatype: "local",
        height: 290,
        colNames: ['Parent Product', 'Product ID', 'Size', 'SKU', 'Stock', 'Express', 'Edit', 'Delete', 'Add Stock'],
        colModel: [{ name: 'Name', index: 'Name', width: 250, align: "left" },
                   { name: 'id', index: 'id', width: 100, align: "left" },
                   { name: 'size', index: 'size', width: 80, align: "left" },
                   { name: 'sku', index: 'sku', width: 100, align: "left" },
                   { name: 'stock', index: 'stock', width: 400, align: "left" },
                   { name: 'Express', index: 'Express', width: 80, align: "left" },
                   { name: 'id', index: 'id', width: 100, align: "center", edittype: 'image', formatter: EditChildProdFormatter },
                   { name: 'id', index: 'id', width: 100, align: "center", edittype: 'image', formatter: DelImageFormatter },
                   { name: 'id', index: 'id', width: 100, align: "center", edittype: 'image', formatter: AddStockImageFormatter }

        ],
        sortname: 'Name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });

    function EditChildProdFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Edit' src='images/Edit.png' name='image'  " +
        "onclick=\"EditChildProd('" + rowObject.id + "','" + rowObject.stock + "','" + rowObject.Express + "');\" />";
    };

    function DelImageFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteChildProd('" + rowObject.id + "');\" />";
    };

    function AddStockImageFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        var prod_name = rowObject.Name;
        prod_name = prod_name.replace("'", "\\'");		
        return "<input style='width:25px;height: 25px;' type='image' title='Add Stock' src='images/AddItem.png' name='image'  " +
        "onclick=\"BulkProdStock(2,'" + prod_name + "','" + rowObject.id + "');\" />";
    };



    for (var i = 0; i <= tableSrc.length; i++)
        $("#ChildProduct").jqGrid('addRowData', i + 1, tableSrc[i]);

}

var ChildProdId = "";

function EditChildProd(ChildProductId, ChildProductStock, ChildProductExpress) {

    $('#EditChildProd, .alert-overlay2').removeClass('hide');
    ChildProdId = ChildProductId;
    $("#txtEditChildProdStock").val(ChildProductStock);
    if (ChildProductExpress == 0) {
        $("#chkEditProdExpress").prop('checked', false);
    } else {
        $("#chkEditProdExpress").prop('checked', true);
    }

}

function SaveEditChildProd() {


    if (ChildProdId == "") {
        alert('Please Select A Child Product');
        return false;
    }

    var txtEditChildProdStock = $("#txtEditChildProdStock").val();
    txtEditChildProdStock = $.trim(txtEditChildProdStock);
    if (txtEditChildProdStock == null || txtEditChildProdStock == "") {
        alert('Please Enter Stock');
        return false;
    }

    var EXP = "";

    if ($("#chkEditProdExpress").is(':checked')) {
        EXP = 1;
    }
    else {
        EXP = 0;
    }


    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/upd_child_product/',
        type: 'POST',
        data: { id: ChildProdId, stock: txtEditChildProdStock, Express: EXP },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            $('#EditChildProd, .alert-overlay2').addClass('hide');
            getChildProduct();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });




}


function DeleteChildProd(ChildProductId) {

    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/del_child_product/',
            type: 'POST',
            data: { id: ChildProductId },
            dataType: 'json',
            success: function (result) {
                alert('Delete Successfully!');
                getChildProduct();
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
}


/***************Function: Populate Parent Product List ******************/

function PopParentProductList(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/parent_product_list',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });

            loadingcut();
        }
    });

}

function GetProdFeatures() {

    var Product_ID = $('#PFMProductList').val();
    var ProdFeatureID = $("#ProdFeatureID");

    if (Product_ID != "0") {

        $("#ProductFeatures").GridUnload();
        $("#AdditionalFeature").GridUnload();
        $("#FeaturesGrid").GridUnload();
        var url = rootProdurl + Product_ID;
        //var row = $('<a></a>').html('<a href="' + url + '">' + url + '</a>');
        var row = $('<a></a>').html('<a href=javascript:window.open("' + url + '","_self")>' + url + '</a>');

        ProdFeatureID.html("");
        ProdFeatureID.append(row);

        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/product_features/{prod_id}',
            type: 'GET',
            data: { prod_id: Product_ID },
            dataType: 'json',
            success: function (result) {
                $("#txtPFMProdName").val(result[0].Name);

                if (result[0].parent_cat_id != "BsonNull") {
                    getFeaturesGrid(result[0].parent_cat_id);
                }
                if (result[0].feature.length == 0) {

                    var Features = new Array();
                    PopFeaturesGrid(Features);

                }
                else {
                    var AddedFeatureGrid = new Array();
                    for (var j = 0; j < result[0].feature.length; j++) {

                        var AddedFeatureData = {
                            name: result[0].feature[j].name,
                            Value: result[0].feature[j].values
                        }
                        AddedFeatureGrid[j] = AddedFeatureData;
                    }
                    //AddedFeaturesGrid(AddedFeatureGrid);
                    PopFeaturesGrid(AddedFeatureGrid);
                }
                loadingcut();

            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {
        ProdFeatureID.html("");
        ClearProdFeature();



    }


}

function PopParentCatList() {

    loadingadd();

    var val = $('input:radio[name=level]:checked').val();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/parent_cat_list/{level}',
        type: "GET",
        data: { level: val },
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            $("#ddlParentCat").find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                $("#ddlParentCat").append($("<option></option>").val(value.id).text(value.Name));
            });
            loadingcut();
        }
    });


}

function getFeaturesGrid(Parent_Cat_ID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/get_cat_features/{cat_id}',
        type: 'GET',
        data: { cat_id: Parent_Cat_ID },
        dataType: 'json',
        success: function (result) {
            CreateFeaturesGrid(result[0].filters);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}


//function CreateFeaturesGrid(tableSrc) {

//    $("#ProductFeatures").GridUnload(); 

//    $("#ProductFeatures").jqGrid({
//        datastr: tableSrc,
//        datatype: "local",
//        width: 400,
//        height: 150,
//        async: false,
//        colNames: ['Features', 'Value'],
//        colModel: [{ name: 'name', index: 'name', width: 200, align: "left" },                

//                   { name: 'name', index: 'name', width: 80, formatter: DelImageFormatter },
//        ],
//        sortname: 'name',
//        sortorder: "desc",
//        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

//    });
//    function DelImageFormatter(cellvalue, options, rowObject) {
//        var UserId = cellvalue;
//        return "<select style='width:100px;height: 20px;' id='ddlValList'></select>"                                                   
//    };

//    for (var i = 0; i <= tableSrc.length; i++) {
//        $("#ProductFeatures").jqGrid('addRowData', i + 1, tableSrc[i]);
//        if (i < tableSrc.length) {
//            BindValue(tableSrc[i].values);
//        }
//    }

//    function BindValue(tableSrc) {
//        $.each(tableSrc, function (key, value) {
//            $("#ddlValList").append($("<option></option>").val(value).text(value));
//        });
//    }

//}

function CreateFeaturesGrid(tableSrc) {

    $("#ProductFeatures").GridUnload();

    $("#ProductFeatures").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        width: 500,
        height: 110,
        async: false,
        colNames: ['Features', 'Value', '', 'Add Value', ''],
        colModel: [{ name: 'name', index: 'name', width: 200, align: "left" },
                   { name: 'Value', index: 'Value', width: 200, align: "left" },
                   { name: 'values', index: 'values', width: 200, align: "left", hidden: true },
                   { name: 'name', index: 'name', width: 200, formatter: AddFeaturesValue },
                   { name: 'name', index: 'name', width: 200, align: "left", hidden: true }
        ],
        sortname: 'name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function AddFeaturesValue(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Add' src='images/Edit.png' name='image'  " +
        "onclick=\"AddValue('" + rowObject.name + "','" + rowObject.values + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#ProductFeatures").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

}

function PopFeaturesGrid(tableSrc) {

    $("#FeaturesGrid").GridUnload();

    $("#FeaturesGrid").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        width: 500,
        height: 110,
        async: false,
        colNames: ['Features', 'Value', 'Action', ''],
        colModel: [{ name: 'name', index: 'name', width: 200, align: "left" },
                   { name: 'Value', index: 'Value', width: 200, align: "left" },
                   { name: 'name', index: 'name', width: 55, formatter: DeleteFeaturesValue },
                   { name: 'name', index: 'name', width: 55, hidden: true }

        ],
        sortname: 'name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DeleteFeaturesValue(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:20px;height: 20px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteFeature('" + rowObject.name + "','" + rowObject.Value + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#FeaturesGrid").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

}

function AddedFeaturesGrid(tableSrc) {

    $("#ProductFeatures").GridUnload();

    $("#ProductFeatures").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        width: 500,
        height: 65,
        async: false,
        colNames: ['Features', 'Value', 'Action', ''],
        colModel: [{ name: 'name', index: 'name', width: 200, align: "left" },
                   { name: 'Value', index: 'Value', width: 200, align: "left" },
                   { name: 'name', index: 'name', width: 55, formatter: DeleteFeaturesValue },
                   { name: 'name', index: 'name', width: 55, hidden: true }

        ],
        sortname: 'name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DeleteFeaturesValue(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:20px;height: 20px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteFeature('" + rowObject.name + "','" + rowObject.Value + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#ProductFeatures").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

}

function AddValue(FeatureName, FeatureValues) {
    $('#FeatureName').text(FeatureName);
    $('#SelFeaturesValue').removeClass('hide');
    var arrFeatureValues = FeatureValues.split(',');
    $("#FeatureValues").find('option').remove().end()
    $.each(arrFeatureValues, function (key, value) {
        $("#FeatureValues").append($("<option></option>").val(value).text(value));
    });
}

function AddFeatureVal() {
    var FeatureName = $('#FeatureName').text();
    var FeatureVal = $("#FeatureValues").val();

    if (FeatureVal == null) {
        alert('Please Select A Value!');
        return false;
    }

    //var grid = jQuery('#ProductFeatures');
    //var GridData = grid.getRowData();

    //for (var i = 0; i < GridData.length; i++) {

    //    if (GridData[i].name == FeatureName) {

    //        GridData[i].Value = FeatureVal;
    //    }

    //}


    var grid = jQuery('#FeaturesGrid');
    var GridData = grid.getRowData();

    var match = 0;

    for (var i = 0; i < GridData.length; i++) {
        if (GridData[i].name == FeatureName) {
            GridData[i].Value = FeatureVal;
            match++;
        }
    }
    var NewGridData = GridData;
    if (match == 0) {
        if (FeatureName != null && FeatureName != "") {

            var NewFeatureData = {
                name: FeatureName,
                Value: FeatureVal
            }
            NewGridData[GridData.length] = NewFeatureData;
        }
    }

    $('#SelFeaturesValue').addClass('hide');

    PopFeaturesGrid(NewGridData);
}

function CreateAddFeatures() {

    var txtAddFeature = $("#txtAddFeature").val();
    txtAddFeature = $.trim(txtAddFeature);
    if (txtAddFeature == "" || txtAddFeature == null) {
        alert('Please Enter Additional Feature Name!');
        return false;
    }

    var txtAddFeatureValue = $("#txtAddFeatureValue").val();
    txtAddFeatureValue = $.trim(txtAddFeatureValue);
    if (txtAddFeatureValue == "" || txtAddFeatureValue == null) {
        alert('Please Enter Additional Feature Value!');
        return false;
    }
    var NewAddFeatureData = {
        name: txtAddFeature,
        Value: txtAddFeatureValue
    }

    var grid = jQuery('#FeaturesGrid');
    var GridData = grid.getRowData();


    if (GridData.length == 0) {
        var AdFeatureArray = new Array();
        AdFeatureArray[0] = NewAddFeatureData;
        PopFeaturesGrid(AdFeatureArray);
    }
    else {
        for (i = 0; i < GridData.length; i++) {
            if (GridData[i].name == txtAddFeature) {
                alert('Feature Name Already Added.');
                return false
            }
        }
        var NewGridData = GridData;
        NewGridData[GridData.length] = NewAddFeatureData;
        PopFeaturesGrid(NewGridData);
    }

    $("#txtAddFeature").val('');
    $("#txtAddFeatureValue").val('');
}
function CreateAddFeaturesGrid(tableSrc) {

    $("#AdditionalFeature").GridUnload();

    $("#AdditionalFeature").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        width: 500,
        height: 65,
        colNames: ['Features', 'Value'],
        colModel: [{ name: 'name', index: 'name', width: 200, align: "left" },
                   { name: 'Value', index: 'Value', width: 200, align: "left" }
        ],
        sortname: 'name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#AdditionalFeature").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

}

function SaveFeature() {

    var Product_ID = $("#PFMProductList").val();
    //var grid = jQuery('#ProductFeatures');
    var grid = jQuery('#FeaturesGrid');
    var GridData = grid.getRowData();
    var FeatureArray = new Array();
    if (GridData.length == 0) {
        alert('No Features Available!');
        return false;
    }
    else {
        for (var i = 0; i < GridData.length; i++) {
            var Features_Data = {
                name: GridData[i].name,
                values: GridData[i].Value
            }
            FeatureArray[i] = Features_Data;
        }
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({

        url: rooturl + '/category/admin/update_prod_features/',
        type: 'POST',
        data: { id: Product_ID, feature: FeatureArray },
        dataType: 'json',
        success: function (result) {

            loadingcut();
            alert('Record Saved Successfull!');
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function SaveAddFeature() {

    var Product_ID = $("#PFMProductList").val();


    var grid = jQuery('#AdditionalFeature');
    var GridData = grid.getRowData();
    var FeatureArray = new Array();
    if (GridData.length == 0) {

        alert('No Features Available!');
        return fals;

    }
    else {
        for (var i = 0; i < GridData.length; i++) {
            var Features_Data = {
                name: GridData[i].name,
                values: GridData[i].Value
            }
            FeatureArray[i] = Features_Data;
        }
    }

    $.support.cors = true;
    $.ajax({

        url: rooturl + '/category/admin/update_prod_features/',
        type: 'POST',
        data: { id: Product_ID, feature: FeatureArray },
        dataType: 'json',
        success: function (result) {

            alert('Update Successfull!');
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}


function DeleteFeature(FilterName, FilterValue) {

    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {
        var Product_ID = $("#PFMProductList").val();
        var FeatureArr = new Array();
        var Features_Data = {
            name: FilterName,
            values: FilterValue
        }
        FeatureArr[0] = Features_Data;

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/product/del_product_feature/',
            type: 'POST',
            data: { id: Product_ID, feature: FeatureArr },
            dataType: 'json',
            success: function (result) {
                alert('Delete Successfully!');
                GetProdFeatures();
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
}

/*********Validate User Authentication*********/

function TrapKeyEvent(e) {
    e.preventDefault();
    var key = window.event ? e.keyCode : e.which;
    if (key == '13') {
        UserValidate();;
    }
}
function UserValidate() {


    if (getUser() == false) {
        return false;
    }
    var User = getUser();

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/admin_login/',
        type: 'POST',
        data: User,
        dataType: 'json',
        success: function (result) {

            if (result.value == "Log In Successfully") {

                var username = $.trim($("#Username").val());
                setTimeout(function () { getUserRights(username); }, 2000);

                $("#ddlTrackCycle").val('MONT');
                $("#ddlTrackCycle").prop('disabled', true);

                ddlCompMode = $("#CompanyEntryType");
                PopulateCompany(ddlCompMode);
                vm_CompanyViewModel = new CompanyViewModel();
                ko.applyBindings(vm_CompanyViewModel, document.getElementById("Customer-Maintenance"));


            }
            else {
                alert('Invalid Username Or Password');
                $("#Password").val('');
                $("#Password").focus();
                loadingcut();
            }


        },
        error: function (req, status, errorObj) {
            //console.log(req);
            alert('Error in Login');
            loadingcut();
        }
    });
}


function getUserRights(user_id) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/getuser_assign_rights?USER_NAME=' + user_id + '&Comp_ID=1&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            $(".main-nav li ").hide();
            $(".set-up li").hide();

            USER_RIGHTS = result;

            if (USER_RIGHTS.length > 0) {
                for (var i = 0; i < USER_RIGHTS.length; i++) {
                    var Rights_Val = USER_RIGHTS[i].user_right_id;

                    if (Rights_Val == 1) { $("#CustMaint").show(); }
                    else if (Rights_Val == 2) { $("#CatMgmt").show(); }
                    else if (Rights_Val == 3) { $("#ProdMgmt").show(); }
                    else if (Rights_Val == 4) { $("#ChildProdMaint").show(); }
                    else if (Rights_Val == 5) { $("#ProdCatAssoc").show(); }
                    else if (Rights_Val == 6) { $("#StoreCatalog").show(); }
                    else if (Rights_Val == 7) { $("#ProdFeatureMgmt").show(); }
                    else if (Rights_Val == 8) { $("#CatFilter").show(); }
                    else if (Rights_Val == 9) { $("#DealSetup").show(); }
                    else if (Rights_Val == 10) { $("#Brand").show(); }
                    else if (Rights_Val == 11) { $("#ProdAppr").show(); }
                    else if (Rights_Val == 12) { $("#StoreConfig").show(); }
                    else if (Rights_Val == 13) { $("#EGiftVoucher").show(); }
                    else if (Rights_Val == 14) { $("#MangEGiftVoucher").show(); }
                        //else if (Rights_Val == 15) { $("#EGiftVoucher").show(); }
                    else if (Rights_Val == 16) { $("#OrdView").show(); }
                    else if (Rights_Val == 17) { $("#UpldCustPoint").show(); }
                    else if (Rights_Val == 18) { $("#AssignUserRights").show(); }
                    else if (Rights_Val == 19) { $("#IntimateUsers").show(); }

                    else if (Rights_Val == 20) { $("#MigrateStore").show(); }
                    else if (Rights_Val == 21) { $("#EGiftVouReport").show(); }
                    else if (Rights_Val == 22) { $("#PromoMgmt").show(); }
                    else if (Rights_Val == 23) { $("#ChngPin").show(); }

                    else if (Rights_Val == 24) { $("#OrdTrack").show(); }
                    else if (Rights_Val == 25) { $("#Report").show(); $("#liDailySalesWiseReport").show(); }
                    else if (Rights_Val == 26) { $("#liUserPointsDetail").show(); }
                    else if (Rights_Val == 27) { $("#wirehouse").show(); }
                    else if (Rights_Val == 29) { $("#ProdEnquiry").show(); }
                }

            }

            loadingcut();


            $('.main-app').removeClass('hide');
            $('.inv-login').addClass('hide');

        }
    });


}

/*********Fetch the User Information given by User*********/
function getUser() {
    var username = $("#Username").val();
    username = $.trim(username);

    if (username == "" || username == null) {
        alert('Please enter your username');
        return false;
    }

    var password = $("#Password").val();
    password = $.trim(password);

    if (password == "" || password == null) {
        alert('Please enter your password');
        return false;
    }

    var usertype = "ADM";

    return {
        user_name: username,
        password: password,
        user_type: usertype
    };
}

function SaveBrand() {

    var txtBrandName = $.trim($("#txtBrandName").val());
    if (txtBrandName == '' || txtBrandName == null) {
        alert('Please Enter Brand Name');
        return false;
    }

    var ddlBrand = $("#ddlBrand").val();

    if (ddlBrand == "New Brand") {
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/insert_brand/',
            type: 'POST',
            data: { name: txtBrandName },
            dataType: 'json',
            success: function (result) {

                if (result == "Brand Name Already Exists") {
                    alert(result);
                }
                else {
                    alert('Record Saved Successfully.');
                    $("#txtBrandName").val('');
                    var BrandList = $("#ddlBrand");
                    PopulateBrandList(BrandList, "N");
                }
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/update_brand/',
            type: 'POST',
            data: { _id: ddlBrand, name: txtBrandName },
            dataType: 'json',
            success: function (result) {
                if (result == "Brand Name Already Exists") {
                    alert(result);
                }
                else {
                    alert('Record Updated Successfully.');
                    $("#txtBrandName").val('');
                    var BrandList = $("#ddlBrand");
                    PopulateBrandList(BrandList, "N");
                }
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });

    }
}

function GetBrandDetails() {

    var ddlBrand = $("#ddlBrand").val();

    if (ddlBrand != "New Brand" && ddlBrand != null) {

        loadingadd();
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/brand_details/{brand_name}',
            type: 'GET',
            data: { brand_name: ddlBrand },
            dataType: 'json',
            success: function (result) {
                $("#txtBrandName").val(result[0].name);
                loadingcut();
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }
    else {
        $("#txtBrandName").val('');

    }


}


function SaveDeal() {

    var ddlDealBrand = $("#ddlDealBrand").val();
    var txtDealDisc = $.trim($("#txtDealDisc").val());
    if (ddlDealBrand == 0 || ddlDealBrand == null) {
        alert('Please Select A Brand ');
        return false;
    }


    else if (txtDealDisc == '' || txtDealDisc == null) {
        alert('Please Enter Discount');
        return false;
    }

    else if (txtDealDisc.length > 2) {
        alert('Discount Should Be 2 Digits');
        return false;
    }

    var cal_price = {
        mrp: "",
        list: "",
        discount: txtDealDisc,
        min: "",
        final_offer: "",
        final_discount: ""
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/deal_setup/',
        type: 'POST',
        data: { brand: ddlDealBrand, price: cal_price },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            PopDealHistory();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function ClearProdFeature() {
    $("#txtPFMProdName").val('');
    $("#txtAddFeature").val('');
    $("#txtAddFeatureValue").val('');
    $("#FeaturesGrid").GridUnload();
    $("#ProductFeatures").GridUnload();
    $("#AdditionalFeature").GridUnload();
    var ProdFeatureID = $("#ProdFeatureID");
    ProdFeatureID.html("");
}
function PopDealHistory() {

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/Deal_History/',
        type: "GET",
        data: { deal_details: null },
        dataType: "json",
        cache: false,
        success: function (result) {
            CreateDealsGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}

function CreateDealsGrid(tableSrc) {

    $("#tblExistingDeals").GridUnload();

    $("#tblExistingDeals").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        width: 500,
        height: 200,
        cache: false,
        colNames: ['Deal Description', 'Action', ''],
        colModel: [{ name: 'deal_details', index: 'deal_details', width: 300, align: "left" },
                   { name: 'deal_details', index: 'deal_details', width: 50, align: "center", edittype: 'image', formatter: DelDeal },
                   { name: 'deal_details', index: 'deal_details', hidden: true }
        ],
        sortname: 'name',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DelDeal(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteDeal('" + rowObject.deal_details + "');\" />";
    };


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#tblExistingDeals").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    loadingcut();
}

function DeleteDeal(Deal_Name) {


    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/del_deal/',
            type: 'POST',
            data: { deal_details: Deal_Name },
            dataType: 'json',
            success: function (result) {
                alert('Delete Successfully!');
                PopDealHistory();
            },
            error: function (req, status, errorObj) {
                alert('Error');
            }
        });
    }

}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode <= 45 || charCode == 47 || charCode > 57)) {
        return false;
    }
    return true;
}


function PopOrderDetails() {




    var OrdStore = $("#ddlOrderStore").val();
    if (OrdStore == 0) {
        OrdStore = null;
    }
    else {

        var OrdStore = $.trim($("#ddlOrderStore option:selected").text());
    }

    var OrdTyp = $("#ddlOrderType").val();
    var txtOrdFrom = $.trim($("#txtOrdFrom").val());
    if (txtOrdFrom == '' || txtOrdFrom == null) {
        txtOrdFrom = null;
    }
    var txtOrdTo = $.trim($("#txtOrdTo").val());
    if (txtOrdTo == '' || txtOrdTo == null) {
        txtOrdTo = null;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/Order_History?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&OrderFrmDt=' + txtOrdFrom + '&OrderToDt=' + txtOrdTo + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            CreateOrderGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}
function CreateOrderGrid(tableSrc) {

    $("#OrderList").GridUnload();

    $("#OrderList").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 300,
        cache: false,
        colNames: ['Order #', 'Store Company', 'Date', 'Customer Name', 'Ship To Name', 'Status', 'Total Purchase Amount', 'G.T. (Purchased)', 'Action'],
        colModel: [{ name: 'order_id', index: 'order_id', width: 120 },
                   { name: 'store', index: 'store', width: 250 },
                   { name: 'order_date', index: 'order_date', width: 120 },
                   { name: 'BillToName', index: 'BillToName', width: 250 },
                   { name: 'ShipToName', index: 'ShipToName', width: 200, hidden: true },
                   { name: 'Order_Status', index: 'Order_Status', width: 200 },
                   { name: 'total_amount', index: 'total_amount', width: 200 },
                   { name: 'total_amount', index: 'total_amount', width: 150, hidden: true },
                   { name: 'order_id', index: 'order_id', width: 100, align: "center", edittype: 'image', formatter: DelDeal }],
        multiselect: true,
        sortname: 'order_id',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DelDeal(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 22px;' type='image' title='View' src='images/Dj_View.png' name='image'  " +
        "onclick=\"ViewOrder('" + rowObject.order_id + "');\" />";
    };


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#OrderList").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    //$("#OrderList").fluidGrid({ example: "#catalog-search .catalog-searchResult-main", offset: 0 });


    loadingcut();
}

function ViewOrder(OrderId) {

    GlobalOrderId = OrderId;


    $('.OrderView').val('');
    $('#lblOrder').text("Order # " + OrderId);

    $('#txtDisputeOrderNo, #txtPayInfoOrdNo').val('');
    $('#txtDisputeOrderNo, #txtPayInfoOrdNo').val(OrderId);
    $("#txtDisputeOrderNo, #txtDisputeNotesInAct, #txtPayInfoOrdNo").prop('disabled', true);


    $('#Order-Processing').addClass('hide');
    $('#OrderView').removeClass('hide');

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/get_order_data/{order_id}',
        type: "GET",
        data: { Order_Id: OrderId },
        dataType: "json",
        cache: false,
        success: function (result) {

          
   
            PopPayStatus(result.status);

            $('#txtOrderDate').val(result.order_date);
            $('#ddlOrdStatus').val(result.status);
            $('#txtPurchasedFrom').val(result.store);
            $('#txtCustomerName').val(result.Cust_Name);
            $('#txtBillingAddress').val(result.billing_address);
            $('#txtShippingAddress').val(result.shipping_address);
            // $('.address').val(result.billing_address);

            $('#txtCustEmail').val(result.email_id);
            $('#txtContactNumber').val(result.contact_number);
            $('#txtPointsBalance').val(result.bal_points);
            $('#txtPointsRedeemed').val(result.points);
            $('#txtGiftVoucher').val(result.egift_vou_amt);

            if (result.egift_vou_amt > 0)
                $('#txtGiftVoucherNo').val(result.egift_vou_no);
            else
                $('#txtGiftVoucherNo').val('');

            $('#txtCashCardPaid').val(result.paid_amount);

            if (result.paid_amount >= 500 || result.paid_amount == 0) {
                $('#txtFreeShipping').val("Free Rs 0.00");
            } else {
                $('#txtFreeShipping').val(" Rs 99.00");
            }


            $('#txtCourServProvider').val(result.shipping_info);
            $('#txtCourierTrackNo').val(result.courier_track_no);
            $('#txtCourierTrackLink').val(result.courier_track_link);
            $('#txtShippingDate').val(result.shipping_date);
            $('#txtCourierCost').val(result.courier_cost);
            //        $('#txtspecial_discount').val(result.special_discount);  //Atul 17/09/2014
            //        $('#txtfinal_amt_paid').val(result.final_amt_paid);    //Atul 17/09/2014
            $('#txtDisputeNotesInAct').val(result.dispute_notes);
            if (result.status != 5) {
                $("#chkSendEmail").prop('checked', true);
            }
            else {
                $("#chkSendEmail").prop('checked', false);
            }

            PartialOrderProducts = result.cart_data;
                  
             
            PopOrderItemGrid(result.cart_data);
            PopSplDiscountGrid(result);
            


        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });



}



function SearchProduct() {

    var PRODUCT_NAME = $("#txtSchProdName").val();
    var BRAND = $("#ddlSchBrand").val();

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/Prod_List_App/',
        type: 'POST',
        data: { Name: PRODUCT_NAME, brand: BRAND },
        dataType: 'json',
        success: function (result) {
            CreateProdAppGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}

function CreateProdAppGrid(tableSrc) {

    $("#ProductGrid").GridUnload();

    $("#ProductGrid").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        cache: false,
        height: 300,
        colNames: ['Product ID', 'Product SKU', 'Product Name', 'Brand', 'Action'],
        colModel: [{ name: 'id', index: 'id', width: 150, align: "left" },
                   { name: 'sku', index: 'sku', width: 150, align: "left" },
                   { name: 'Name', index: 'Name', width: 350, align: "left" },
                   { name: 'brand', index: 'brand', width: 150, align: "left" },
                   { name: 'id', index: 'id', width: 300, align: "left", edittype: 'image', formatter: DelDeal },
        ],
        sortname: 'order_id',
        multiselect: true,
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DelDeal(cellvalue, options, rowObject) {
        var Product_ID = cellvalue;
        var url = rootProdurl + Product_ID;
        return "<a href=\"" + url + "\" target=\"blank\">" + url + "</a>";

    };


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#ProductGrid").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    loadingcut();
}

function ProductApproval() {

    if (GetProductId() == false) {

        return false;
    }


    var PRODUCT_IDs = GetProductId();
    var Products = new Array();
    for (var i = 0; i < PRODUCT_IDs.length; i++) {
        var prod_ids = {
            id: PRODUCT_IDs[i]
        }
        Products[i] = prod_ids;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/Prod_For_Approval/',
        type: 'POST',
        data: { prod_ids: Products },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Product Approval Successfully');
            SearchProduct();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function GetProductId() {


    try {
        var ids = jQuery("#ProductGrid").getGridParam('selarrrow');

        if (typeof ids === "undefined") {
            alert('Please Select Product');
            return false;
        }
        else {
            var count = ids.length;
            if (count == 0) {
                alert('Please Select Product');
                return false;
            }
            else {
                var PRODUCT_ID = new Array();
                for (var i = 0; i < count; i++) {
                    PRODUCT_ID[i] = $('#ProductGrid').jqGrid('getCell', ids[i], 'id');
                }
                return PRODUCT_ID;
            }
        }
    }
    catch (e) {
        return 0;
    }


}

/*********************Populate Store/Company Names*********************************/
function PopStoreCompany(ddlID) {

    var EntryMode = "N";

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postPopulateCompany',
        type: "POST",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;

            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                if (value.id == 0) {
                    value.name = "";
                }
                ddlID.append($("<option></option>").val(value.id).text(value.name));
            });

            loadingcut();
        }
    });

}

function PopCatBrndSku() {



    var ddlStoreComp = $("#ddlStoreComp").val();
    var ddlStoreCompText = $("#ddlStoreComp option:selected").text();
    if (ddlStoreComp != 0) {
        loadingadd();
        getStoreCatList(ddlStoreComp);
        getStoreBrandList(ddlStoreComp);
        getStoreSKUList(ddlStoreComp);
        setTimeout(function () { getStoreConfigData(ddlStoreCompText); }, 6000);


    } else {
        $('#txtAddDiscount').val('');
        $("#StoreConfigGrid").GridUnload();
        $('#ddlStoreCat option').attr('selected', false);
        $('#ddlStoreBrand option').attr('selected', false);
        $('#ddlStoreSku option').attr('selected', false);
    }





}

function getStoreConfigData(StoreComp) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/get_store_config/{StoreCompId}',
        type: "GET",
        data: { StoreCompId: StoreComp },
        dataType: "json",
        cache: false,
        success: function (result) {

            if (result.length != 0) {

                var Store_Config_Arr = new Array();
                for (var i = 0; i < result[0].discount.length; i++) {
                    if (result[0].discount[i].type == "store") {
                        $('#txtAddDiscount').val(result[0].discount[i].val);
                    }
                    else {
                        var Store_Data = {
                            ID: result[0].discount[i].key,
                            Type: result[0].discount[i].type,
                            Value: result[0].discount[i].key_text,
                            Off: result[0].discount[i].val
                        }
                        Store_Config_Arr[i] = Store_Data;
                    }

                }
                CreateDiscGrid(Store_Config_Arr);


                var CatSelValues = new Array();
                var BrandSelValues = new Array();
                var SkuSelValues = new Array();

                for (var j = 0; j < result[0].exclusion.length; j++) {

                    if (result[0].exclusion[j].type == "cat") {

                        CatSelValues[CatSelValues.length] = result[0].exclusion[j].value;

                    }
                    else if (result[0].exclusion[j].type == "brand") {

                        BrandSelValues[BrandSelValues.length] = result[0].exclusion[j].value;
                    }
                    else if (result[0].exclusion[j].type == "sku") {

                        SkuSelValues[SkuSelValues.length] = result[0].exclusion[j].value;
                    }
                }
                $(".ClsStoreCat").val(CatSelValues);
                $(".ClsStoreBrand").val(BrandSelValues);
                $(".ClsStoreSku").val(SkuSelValues);


            }
            loadingcut();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}

function getStoreCatList(StoreCompID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/store_cat_list/{StoreCompId}',
        type: "GET",
        data: { StoreCompId: StoreCompID },
        dataType: "json",
        cache: false,
        success: function (result) {
            var ddlStoreCat = $('#ddlStoreCat')
            PopStoreCatList(result, ddlStoreCat, 0);
            var ddlStoreDiscCat = $('#ddlStoreDiscCat')
            PopStoreCatList(result, ddlStoreDiscCat, 1);
            var ddlStoreDiscCatList = $('#ddlStoreDiscCatList')
            PopStoreCatList(result, ddlStoreDiscCatList, 1);


        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}
function PopStoreCatList(result, ddlID, Flag) {

    ddlID.find('option').remove().end();
    if (Flag == 1) {
        ddlID.append($("<option></option>").val(0).text(""));
    }
    var tableSrc = result;
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.id).text(value.Name));
    });

}


function getStoreBrandList(StoreCompID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/store_Brand_list/{StoreCompId}',
        type: "GET",
        data: { StoreCompId: StoreCompID },
        dataType: "json",
        cache: false,
        success: function (result) {

            var ddlStoreBrand = $('#ddlStoreBrand')
            PopStoreBrandList(result, ddlStoreBrand, 0);
            var ddlStoreDiscBrnd = $('#ddlStoreDiscBrnd')
            PopStoreBrandList(result, ddlStoreDiscBrnd, 1);

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function PopStoreBrandList(result, ddlID, flag) {
    var tableSrc = result;
    ddlID.find('option').remove().end();
    if (flag == 1) {
        ddlID.append($("<option></option>").val(0).text(""));
    }
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.name).text(value.name));
    });
}


function getStoreSKUList(StoreCompID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/store_Sku_list/{StoreCompId}',
        type: "GET",
        data: { StoreCompId: StoreCompID },
        dataType: "json",
        cache: false,
        success: function (result) {

            var ddlStoreSku = $('#ddlStoreSku')
            PopStoreSKUList(result, ddlStoreSku, 0);
            var ddlStoreDiscSKUList = $('#ddlStoreDiscSKUList')
            PopStoreSKUList(result, ddlStoreDiscSKUList, 1);

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function PopStoreSKUList(result, ddlID, flag) {

    var tableSrc = result;
    ddlID.find('option').remove().end();
    if (flag == 1) {
        ddlID.append($("<option></option>").val(0).text(""));
    }
    $.each(tableSrc, function (key, value) {
        ddlID.append($("<option></option>").val(value.id).text(value.Name));
    });
}

function DiscOnCat() {

    var ddlStoreDiscCat = $('#ddlStoreDiscCat').val();
    var ddlStoreDiscCatText = $("#ddlStoreDiscCat option:selected").text();
    if (ddlStoreDiscCat == 0 || ddlStoreDiscCat == null) {
        alert('Please Select A Category');
        return false;

    }
    var txtCatDisc = $.trim($("#txtCatDisc").val());
    if (txtCatDisc == '' || txtCatDisc == null) {
        alert('Please Enter Discount Value');
        return false;

    }

    var grid = jQuery('#StoreConfigGrid');
    var GridData = grid.getRowData();
    var DiscArray = new Array();

    DiscArray = GridData;
    CheckDiscFld(GridData, ddlStoreDiscCat);
    var Disc_Data = {
        ID: ddlStoreDiscCat,
        Type: "Cat",
        Value: ddlStoreDiscCatText,
        Off: txtCatDisc
    }
    DiscArray[GridData.length] = Disc_Data;

    CreateDiscGrid(DiscArray);
}



function DiscOnBrandCat() {

    var ddlStoreDiscBrnd = $('#ddlStoreDiscBrnd').val();
    var ddlStoreDiscBrndText = $("#ddlStoreDiscBrnd option:selected").text();
    if (ddlStoreDiscBrnd == 0 || ddlStoreDiscBrnd == null) {
        alert('Please Select A Brand');
        return false;

    }
    var ddlStoreDiscCatList = $('#ddlStoreDiscCatList').val();
    var ddlStoreDiscCatListText = $("#ddlStoreDiscCatList option:selected").text();
    if (ddlStoreDiscCatList == 0 || ddlStoreDiscCatList == null) {
        alert('Please Select A Category');
        return false;

    }

    var txtBrndCatDisc = $.trim($("#txtBrndCatDisc").val());
    if (txtBrndCatDisc == '' || txtBrndCatDisc == null) {
        alert('Please Enter Discount Value');
        return false;

    }

    var grid = jQuery('#StoreConfigGrid');
    var GridData = grid.getRowData();
    var DiscArray = new Array();

    DiscArray = GridData;

    var Disc_Brnd_Data = {
        ID: ddlStoreDiscBrnd,
        Type: "Brand",
        Value: ddlStoreDiscBrndText,
        Off: txtBrndCatDisc
    }
    DiscArray[GridData.length] = Disc_Brnd_Data;

    var Disc_Cat_Data = {
        ID: ddlStoreDiscCatList,
        Type: "Cat",
        Value: ddlStoreDiscCatListText,
        Off: txtBrndCatDisc
    }

    DiscArray[GridData.length + 1] = Disc_Cat_Data;
    CreateDiscGrid(DiscArray);
}



function DiscOnSKU() {

    var ddlStoreDiscSKUList = $('#ddlStoreDiscSKUList').val();
    var ddlStoreDiscSKUListText = $("#ddlStoreDiscSKUList option:selected").text();
    if (ddlStoreDiscSKUList == 0 || ddlStoreDiscSKUList == null) {
        alert('Please Select A SKU');
        return false;

    }
    var txtDiscOnSKU = $.trim($("#txtDiscOnSKU").val());
    if (txtDiscOnSKU == '' || txtDiscOnSKU == null) {
        alert('Please Enter Discount Value');
        return false;

    }

    var grid = jQuery('#StoreConfigGrid');
    var GridData = grid.getRowData();
    var DiscArray = new Array();

    DiscArray = GridData;


    CheckDiscFld(GridData, ddlStoreDiscSKUList);

    var Disc_Data = {
        ID: ddlStoreDiscSKUList,
        Type: "sku",
        Value: ddlStoreDiscSKUListText,
        Off: txtDiscOnSKU
    }
    DiscArray[GridData.length] = Disc_Data;

    CreateDiscGrid(DiscArray);
}

function CheckDiscFld(GridData, FldID) {


    for (var j = 0; j < GridData.length; j++) {
        if (GridData[j].ID == FldID) {
            alert('Already Applied Discount On Seleced Item ');
            return false;
        }
    }

}
function CreateDiscGrid(tableSrc) {

    $("#StoreConfigGrid").GridUnload();

    $("#StoreConfigGrid").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        cache: false,
        height: 100,
        colNames: ['ID', 'Type', 'Value', '% Off', 'Action', ''],
        colModel: [
                   { name: 'ID', index: 'ID', width: 150, align: "left" },
                   { name: 'Type', index: 'Type', width: 150, align: "left" },
                   { name: 'Value', index: 'Value', width: 350, align: "left" },
                   { name: 'Off', index: 'Off', width: 150, align: "left" },
                   { name: 'ID', index: 'ID', width: 300, align: "left", edittype: 'image', formatter: DelDiscount },
                   { name: 'ID', index: 'ID', width: 150, align: "left", hidden: true }
        ],
        sortname: 'ID',

        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });
    function DelDiscount(cellvalue, options, rowObject) {

        var UserId = cellvalue;
        return "<input style='width:20px;height: 20px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DeleteFilter('" + rowObject.ID + "');\" />";
    };


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#StoreConfigGrid").jqGrid('addRowData', i + 1, tableSrc[i]);
    }


}

function SaveExclusionDisc() {


    var ddlStoreComp = $('#ddlStoreComp').val();
    var ddlStoreCompText = $("#ddlStoreComp option:selected").text();
    if (ddlStoreComp == 0 || ddlStoreComp == null) {
        alert('Please Select Store Or Company');
        return false;
    }

    var Exclu_Data = new Array();

    var ddlStoreCat = $("#ddlStoreCat").val();
    if (ddlStoreCat == "" || ddlStoreCat == null) {
        alert('Please Select Category');
        return false;
    }
    for (var i = 0; i < ddlStoreCat.length; i++) {
        var Exclu = {
            type: "cat",
            value: ddlStoreCat[i]
        }
        Exclu_Data[i] = Exclu;
    }

    var ddlStoreBrand = $("#ddlStoreBrand").val();
    if (ddlStoreBrand == "" || ddlStoreBrand == null) {
        alert('Please Select Brand');
        return false;
    }
    for (var j = 0; j < ddlStoreBrand.length; j++) {
        var Exclu = {
            type: "brand",
            value: ddlStoreBrand[j]
        }

        Exclu_Data[Exclu_Data.length] = Exclu;

    }

    var ddlStoreSku = $("#ddlStoreSku").val();
    if (ddlStoreSku == "" || ddlStoreSku == null) {
        alert('Please Select SKU');
        return false;
    }
    for (var k = 0; k < ddlStoreSku.length; k++) {

        var Exclu = {
            type: "sku",
            value: ddlStoreSku[k]
        }

        Exclu_Data[Exclu_Data.length] = Exclu;
    }


    var txtAddDiscount = $.trim($("#txtAddDiscount").val());
    if (txtAddDiscount == '' || txtAddDiscount == null) {
        alert('Please Enter Special Discount');
        return false;
    }


    var grid = jQuery('#StoreConfigGrid');
    var GridData = grid.getRowData();
    var DiscArray = new Array();
    var Store_Disc = {
        type: "store",
        key: "",
        val: txtAddDiscount,
    }
    DiscArray[0] = Store_Disc;


    for (var j = 0; j < GridData.length; j++) {
        var Discount_Data = {
            type: GridData[j].Type,
            key: GridData[j].ID,
            key_text: GridData[j].Value,
            val: GridData[j].Off,
        }

        DiscArray[j + 1] = Discount_Data;

    }

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/insert_store_config/',
        type: 'POST',
        data: { store: ddlStoreCompText, exclusion: Exclu_Data, discount: DiscArray },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });


}

function SaveeGift() {

    var ddlVouComp = $('#ddlVouComp').val();
    var ddlVouCompText = $("#ddlVouComp option:selected").text();
    if (ddlVouComp == 0 || ddlVouComp == null) {
        alert('Please Select Store Or Company');
        return false;
    }

    var txtAmount = $.trim($("#txtAmount").val());
    if (txtAmount == '' || txtAmount == null) {
        alert('Please Enter Amount');
        return false;
    }

    var txtNoOfVou = $.trim($("#txtNoOfVou").val());
    if (txtNoOfVou == '' || txtNoOfVou == null) {
        alert('Please Enter No Of Voucher');
        return false;
    }

    var txtEvent = $.trim($("#txtEvent").val());
    if (txtEvent == '' || txtEvent == null) {
        alert('Please Enter Event/Label');
        return false;
    }

    var txtValidity = $.trim($("#txtValidity").val());
    if (txtValidity == '' || txtValidity == null) {
        alert('Please Enter Validity');
        return false;
    }

    var txtFromEmail = $.trim($("#txtFromEmail").val());
    if (txtFromEmail == '' || txtFromEmail == null) {
        alert('Please Enter From Email');
        return false;
    }

    var txtToEmail = $.trim($("#txtToEmail").val());
    if (txtToEmail == '' || txtToEmail == null) {
        alert('Please Enter To Email');
        return false;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/insert/',
        type: 'POST',
        data: { NoOfVou: txtNoOfVou, amount: txtAmount, validity: txtValidity, giver_email: txtFromEmail, rcv_email: txtToEmail, company: ddlVouCompText, event_desc: txtEvent },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}

function getAllVoucherCode() {

    $('.VouClass').addClass('FieldGreen');


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/get_all_voucher_code/{gv_status}',
        type: "GET",
        data: { gv_status: 1 },
        dataType: "json",
        cache: false,
        success: function (result) {

            if (result.length > 0) {
                var VoucherCodeData = new Array();
                for (var i = 0; i < result.length; i++) {
                    var date = new Date(result[i].create_date);
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var VoucherValidity = "Upto" + ' ' + date.getDate() + ' ' + monthNames[date.getMonth()] + "," + ' ' + date.getFullYear();
                    var VouData = {
                        gift_voucher_code: result[i].gift_voucher_code,
                        amount: result[i].amount,
                        company: result[i].company,
                        create_date: VoucherValidity
                    }

                    VoucherCodeData[i] = VouData;
                }
                $("#txtEVouCode").autocomplete({
                    source: VoucherCodeData,
                    focus: function (event, ui) {
                        $("#txtEVouCode").val(ui.item.gift_voucher_code);
                        return false;
                    },
                    select: function (event, ui) {

                        $("#txtEVouCode").val(ui.item.gift_voucher_code);
                        $("#lblVoucherAmount").text(ui.item.amount);
                        $("#lblStoreName").text(ui.item.company);
                        $("#lblVouValidity").text(ui.item.create_date);
                        return false;
                    }
                })
                .data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                        .append("<a>" + item.gift_voucher_code + "</a>")
                        .appendTo(ul);
                };
            }

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}
function VoucherRedeem(CallFrom) {

    var MoreValidity = 0;
    var txtEVouCode = $.trim($("#txtEVouCode").val());
    if (txtEVouCode == '' || txtEVouCode == null) {
        alert('Please Enter An E-Voucher Code');
        return false;
    }

    if (CallFrom == 2) {
        var ddlMorevalidity = $("#ddlMorevalidity").val();
        if (ddlMorevalidity == 0 || ddlMorevalidity == null) {
            alert('Please Select Add More Validity');
            return false;
        }
        MoreValidity = 30 * ddlMorevalidity;
    }


    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/update/post_redeem_voucher_code/',
        type: 'POST',
        data: { gift_voucher_code: txtEVouCode, active: 2, status: "redeemed", validity: MoreValidity },
        dataType: 'json',
        success: function (result) {
            //getAllVoucherCode();
            loadingcut();
            if (CallFrom == 1) {
                alert('Voucher Redeemed Successfully.');
            } else {
                alert('Changed Validity Successfully.');
            }
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}


function FindVoucherDetail() {

    var ddlVouSchComp = $('#ddlVouSchComp').val();
    var StoreName = $.trim($("#ddlVouSchComp option:selected").text());
    if (ddlVouSchComp == 0 || ddlVouSchComp == null) {
        alert('Please Select Store Or Company');
        return false;
    }

    var txtEVouCode = $.trim($("#txtEVouCode").val());
    if (txtEVouCode == '' || txtEVouCode == null) {
        alert('Please Enter An E-Voucher Code');
        return false;
    }



    loadingadd();


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/get_find_voucher_code?gv_code=' + txtEVouCode + '&store=' + StoreName + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            if (result.gift_voucher_code == null) {

                $('.VouClass').removeClass('FieldGreen');
                $('.VouClass').addClass('FieldRed');
                $("#lblVoucherAmount").text('');
                $("#lblStoreName").text('');
                $("#lblVouValidity").text('');
                $("#lblCreatedDate").text('');
                $("#lblVouStatus").text('');

            }
            else {

                $('.VouClass').removeClass('FieldRed');
                $('.VouClass').addClass('FieldGreen');

                var date = new Date(result.create_date);

                var Valid_Date = new Date(result.valid_date);

                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var VoucherValidity = "Upto" + ' ' + Valid_Date.getDate() + ' ' + monthNames[Valid_Date.getMonth()] + "," + ' ' + Valid_Date.getFullYear();
                var VoucherCreateDate = date.getDate() + ' ' + monthNames[date.getMonth()] + "," + ' ' + date.getFullYear();

                $("#lblVoucherAmount").text(result.amount);
                $("#lblStoreName").text(result.company);
                $("#lblVouValidity").text(VoucherValidity);
                $("#lblCreatedDate").text(VoucherCreateDate);
                $("#lblVouStatus").text(result.status);
                $("#btnVouRedeem").prop("disabled", false);
                if (result.status == "redeemed") {
                    $("#btnVouRedeem").prop("disabled", true);
                }

            }

            loadingcut();

        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}


function SearchOrder() {


    PopOrderDetails();

}

function PopSplDiscountGrid(tableSrc) {
    
    var data = [];
    data.push(tableSrc);
    $("#SplDiscount").GridUnload();

    $("#SplDiscount").jqGrid({
        datastr: data,
        datatype: "local",
        cache: false,
        height: 100,


        colNames: ['Special Discount','Logo Charges','Final Amount Charged'],
        colModel: [
                    
               { name: 'special_discount', index: 'data[i].special_discount', width: 150, align: "right" },
               { name: 'logo_charges', index: 'data[i].logo_charges', width: 220, align: "right" },
               { name: 'final_amt_paid', index: 'data[i].final_amt_paid', width: 220, align: "right" },
                   

        ],


        sortname: 'id',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });


    
    for (var i = 0; i <= data.length; i++) {
        $("#SplDiscount").jqGrid('addRowData', i + 1, data[i]);
    }

    loadingcut();
}

function PopOrderItemGrid(tableSrc) {

    
    //var data = [];
    //data.push(tableSrc);
    $("#ItemsOrdered").GridUnload();

    $("#ItemsOrdered").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        cache: false,
        height: 100,
        colNames: ['Product ID', 'Product SKU', 'Product Name', 'Brand', 'Quantity', 'Unit Price', 'Discount', 'Sub Total' ],
        colModel: [{ name: 'id', index: 'cart_data[i].id', width: 120, align: "left" },
                   { name: 'sku', index: 'cart_data[i].sku', width: 120, align: "left" },
                   { name: 'name', index: 'cart_data[i].name', width: 380, align: "left" },
                   { name: 'brand', index: 'cart_data[i].brand', width: 160, align: "left" },
                   { name: 'quantity', index: 'cart_data[i].quantity', width: 150, align: "right" },
                   { name: 'final_offer', index: 'cart_data[i].final_offer', width: 120, align: "right" },
                   { name: 'discount', index: 'cart_data[i].discount', width: 120, align: "right" },
                   { name: 'final_offer', index: 'final_offer', width: 120, align: "right", formatter: CalSubTotal },
                //   { name: 'special_discount', index: 'data[i].special_discount', width: 120, align: "right" },
                //   { name: 'final_amt_paid', index: 'data[i].final_amt_paid', width: 120, align: "right" },

        ],

        //colNames: ['Total Amount', 'Special Discount', 'Final AmtPaid'],
        //colModel: [{ name: 'total_amount', index: 'total_amount', width: 120, align: "left" },
        //           { name: 'special_discount', index: 'special_discount', width: 120, align: "left" },
        //           { name: 'final_amt_paid', index: 'final_amt_paid', width: 380, align: "left" },

        //           ],
        sortname: 'id',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });


    function CalSubTotal(cellvalue, options, rowObject) {

        var Qty = rowObject.quantity;
        var ProdFinalOffer = rowObject.final_offer;
        var SubTotal = 0;
        SubTotal = Qty * ProdFinalOffer * 1;

        return Math.round(SubTotal * 100) / 100;

    };


    for (var i = 0; i <= tableSrc.length; i++) {
        $("#ItemsOrdered").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    loadingcut();
}
function getUserData() {

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/getUser_For_Rights/{company}',
        type: 'GET',
        data: { company: "Annectos" },
        dataType: 'json',
        cache: false,
        success: function (result) {

            SetupUserGrid(result);

        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });
}

function SetupUserGrid(data) {

    var tableSrc = data;
    $("#UserGrid").GridUnload();
    $("#UserGrid").jqGrid({

        datastr: tableSrc,
        datatype: "local",
        height: 200,
        colNames: ['', 'User ID', 'User Name', 'Password', 'Company ID', 'User Type', 'Action', ''],
        colModel: [{ name: 'Id', index: 'Id', width: 40, align: 'center', formatter: RadioButtonFormat },
                   { name: 'Id', index: 'Id', width: 300, sorttype: "int" },
                   { name: 'user_name', index: 'user_name', width: 800, align: "left" },
                   { name: 'password', index: 'password', width: 150, align: "left", hidden: true },
                   { name: 'company_id', index: 'company_id', width: 150, align: "left", hidden: true },
                   { name: 'user_type', index: 'user_type', width: 150, align: "left", hidden: true },
                   { name: 'Id', index: 'Id', width: 160, formatter: EditUserValue },
                   { name: 'Id', index: 'Id', width: 150, align: "left", hidden: true }],

        sortname: 'Id',
        viewrecords: true,
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i>Search Result</div>"

    });

    function RadioButtonFormat(cellvalue, options, rowObject) {
        return "<input id='rbRole' type='radio' name='Role'" + "onclick=\"PopulateRights('" + rowObject.Id + "','" + options.rowId + "');\" />";
    };
    function EditUserValue(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Edit' src='images/Edit.png' name='image'  " +
        "onclick=\"EditUserRecord('" + options.rowId + "');\" />";
    };
    for (var i = 0; i <= tableSrc.length; i++)
        $("#UserGrid").jqGrid('addRowData', i + 1, tableSrc[i]);

    loadingcut();
}

function PopulateRights(USER_ID, RowID) {
    jQuery('#UserGrid').jqGrid('setSelection', RowID);

    GlobalUserId = USER_ID;
    var ddlAvailableRights = $('#ddlAvailableRights');
    BindRights(ddlAvailableRights, USER_ID, 1);

    var ddlAssignedRights = $('#ddlAssignedRights');
    BindRights(ddlAssignedRights, USER_ID, 2);

}

/************Bind Rights Against Role ID*******************/

function BindRights(ddlID, USER_ID, Flag) {

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/getUserRights?USER_ID=' + USER_ID + '&FLAG=' + Flag + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end()
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.user_right_id).text(value.user_right_desc));
            });

            loadingcut();
        }
    });
}

/************Assign Rights Against Role ID*******************/
function AssignRights() {

    var Rights_ID = $("#ddlAvailableRights").val();
    if (Rights_ID == null) {
        alertBox('Please Select Available Rights');
        return false;
    }

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/postAssignRights/',
        type: 'POST',
        data: { RIGHTS_IDS: Rights_ID, USER_ID: GlobalUserId },
        dataType: 'json',
        traditional: true,
        cache: false,
        success: function (result) {
            alert('Rights Assigned Successfully.');
            PopulateRights(GlobalUserId);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });
}


/************Delete Assigned Rights Against Role ID*******************/

function DeleteAssignRights() {

    var Rights_ID = $("#ddlAssignedRights").val();
    if (Rights_ID == null) {
        alertBox('Please Select Assigned Rights');
        return false;

    }

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/postDeleteAssignRights/',
        type: 'POST',
        data: { RIGHTS_IDS: Rights_ID, USER_ID: GlobalUserId },
        dataType: 'json',
        traditional: true,
        cache: false,
        success: function (result) {
            alert('Rights Deleted Successfully.');
            PopulateRights(GlobalUserId);
        },
        error: function (req, status, errorObj) {
            alertBox('Error');
        }
    });
}


function GenRootUrl() {
    var CMCompanyName = $('#CMCompanyName').val();
    CMCompanyName = RootStart + CMCompanyName + RootEnd;
    $('#txtrooturl1').val(CMCompanyName);
}


function SaveUser() {

    if (UsertValidation() == false) {
        return false;
    };

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postUserEntry/',
        type: 'POST',
        data: vm_UserViewModel,
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            getUserData();
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}

function UsertValidation() {

    var txtUserName = $.trim($("#txtUserName").val());
    if (txtUserName == null || txtUserName == "") {
        alert('Please Enter User Name');
        return false;
    }
    var txtUserPwd = $.trim($("#txtUserPwd").val());
    if (txtUserPwd == null || txtUserPwd == " ") {
        alert('Please Enter Password');
        return false;
    }
    var txtRePassword = $.trim($("#txtRePassword").val());
    if (txtRePassword == null || txtRePassword == " ") {
        alert('Please Enter Re Password');
        return false;
    }

    if (txtUserPwd != txtRePassword) {
        alert('Both Password Should Be Same');
        return false;
    }

    var txtUserType = $.trim($("#txtUserType").val());
    if (txtUserType == null || txtUserType == " ") {
        alert('Please Enter User Type');
        return false;
    }


}

function EditUserRecord(RowID) {

    var grid = jQuery('#UserGrid');
    grid.jqGrid('setSelection', RowID);
    var selr = grid.jqGrid('getGridParam', 'selrow');
    var GridData = grid.getRowData(selr);
    vm_UserViewModel = GridData;
    ko.applyBindings(vm_UserViewModel, document.getElementById("adduserdiv"));

    $('#adduserdiv, .alert-overlay2').removeClass('hide');
}


function PopCompanyUI(ddlID) {

    var EntryMode = "N";

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postPopulateCompany',
        type: "POST",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                if (value.id == 0) {
                    ddlID.append($("<option></option>").val(value.id).text("Select Company"));
                } else {
                    ddlID.append($("<option></option>").val(value.id).text(value.name));
                }
            });

            loadingcut();
        }
    });

}

function getUserIntimate() {

    getUserIntimateDtls(1);

}

function getUserIntimateDtls(PgNo) {

    var ddlUICompVal = $('#ddlUIComp').val();


    if (ddlUICompVal == "0") {
        alert('Please Select A Company');
        return false;
    }

    var ddlUIComp = $("#ddlUIComp option:selected").text();

    var SearchEmail = null;

    var txtSearchEmail = $.trim($("#txtSearchEmail").val());
    if (txtSearchEmail == null || txtSearchEmail == " ") {
        SearchEmail = null;
    }
    else {
        SearchEmail = txtSearchEmail;
    }


    $("#PageNo").val(PgNo);

    loadingadd();


    $.support.cors = true;
    $.ajax({

        url: rooturl + '/category/getUserDatePageWise?company=' + ddlUIComp + '&pagenum=' + PgNo + '&useremail=' + SearchEmail + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            SetupUserIntimateGrid(result);

        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}

function NextCall() {
    var pageNo = $("#PageNo").val();
    var NextPage = pageNo * 1 + 1;
    var TotalPage = $("#TotalPage").text();
    if (NextPage > TotalPage) {
        alert('No Next Page Available');
    }
    else {

        $("#PageNo").val(NextPage);
        getUserIntimateDtls(NextPage);
    }
}

/************** Search Previous Page option************/
function PrevCall() {
    var PageNo = $("#PageNo").val();
    var PrevPage = PageNo * 1 - 1;
    if (PrevPage == 0) {
        alert('No Previous Page Available');
    }
    else {
        $("#PageNo").val(PrevPage);
        getUserIntimateDtls(PrevPage);
    }
}
/************** Search First Page************/
function FirstCall() {
    getUserIntimateDtls(1);
}

/************** Search Last Page************/
function LastCall() {
    var TotalPage = $("#TotalPage").text();
    getUserIntimateDtls(TotalPage);
}



function SetupUserIntimateGrid(data) {

    var tableSrc = data;
    $("#tblUserIntimate").GridUnload();
    $("#tblUserIntimate").jqGrid({

        datastr: tableSrc,
        datatype: "local",
        height: 300, //Diwakar 7/2/2014

        colNames: ['First Name', 'Last Name', 'Email ID', 'Points', 'Mobile No', 'Status', 'Password', 'Location'],
        colModel: [
                   { name: 'first_name', index: 'first_name', width: 160 },
                   { name: 'last_name', index: 'last_name', width: 150 },
                   { name: 'email_id', index: 'email_id', width: 290 },
                   { name: 'points', index: 'points', width: 120, },
                   { name: 'mobile_no', index: ' mobile_no', width: 140, },
                   { name: 'user_status', index: 'user_status', width: 140, },
                   { name: 'password', index: 'password', width: 150, },
                   { name: 'location', index: 'location', width: 150, }

        ],

        sortname: 'first_name',
        multiselect: true,
        viewrecords: true,
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i>Search Result</div>"

    });

    for (var i = 0; i <= tableSrc.length; i++)
        $("#tblUserIntimate").jqGrid('addRowData', i + 1, tableSrc[i]);

    if (tableSrc.length > 0) {
        $("#TotalPage").text(tableSrc[0].Total_Page);
        $("#gvFooter1").show();
        $('#Gridtable').removeClass('hide');
    }
    else {
        $("#PageNo").val('');
        $("#TotalPage").text('');
        $("#gvFooter1").hide();
        $('#Gridtable').addClass('hide');
        alert('No Records Found');
    }


    loadingcut();
}


function SendUserCredential() {

    var Email_IDs = GetSelectedEmailID();
    loadingadd();
    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/postSendUserCredential/',
        type: 'POST',
        data: { emailids: Email_IDs },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert('Email Has Been Send');
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}

function RegPassword() {

    var Email_IDs = GetSelectedEmailID();
    loadingadd();
    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/postRegPassword/',
        type: 'POST',
        data: { emailids: Email_IDs },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert('Regenerate Password Successfully');
            getUserIntimateDtls(1);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}

function GetSelectedEmailID() {

    try {
        var ids = jQuery("#tblUserIntimate").getGridParam('selarrrow');

        if (typeof ids === "undefined") {
            alert('Please Populate The Grid First');
            return false;
        }
        if (ids.length == 0) {
            alert('Please Select Atleast One Email ID');
            return false;

        }
        var count = ids.length;
        var EMAIL_ID = new Array();
        for (var i = 0; i < count; i++) {
            EMAIL_ID[i] = $('#tblUserIntimate').jqGrid('getCell', ids[i], 'email_id');
        }
        return EMAIL_ID;
    }
    catch (e) {
        return 0;
    }

}

function ExportCustList() {


    var ids = jQuery("#tblUserIntimate").getGridParam('selarrrow');

    if (typeof ids === "undefined") {
        alert('Please Populate The Grid First');
        return false;
    }
    if (ids.length == 0) {
        alert('Please Select Atleast One Customer');
        return false;

    }
    var Cust_Data = new Array();
    var selRow = jQuery("#tblUserIntimate").jqGrid('getGridParam', 'selarrrow');  //get selected rows
    for (var i = 0; i < selRow.length; i++)  //iterate through array of selected rows
    {
        var ret = jQuery("#tblUserIntimate").jqGrid('getRowData', selRow[i]);   //get the selected row
        Cust_Data[i] = ret;
    }

    loadingadd();

    //var grid = jQuery('#tblUserIntimate');
    //var CustGridData = grid.getRowData();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/postExportCustList/',
        type: 'POST',
        dataType: 'json',
        data: { GridData: Cust_Data },
        success: function (result) {
            loadingcut();
            window.open(siteurl + result + ".csv", "_blank");

        },
        error: function (req, status, errorObj) {
            alert('Error');
            return false;
        }
    });

}


function SearchEGiftVoucher() {

    var ddlEGiftCompany = $('#ddlEGiftCompany').val();
    var StoreName = $.trim($("#ddlEGiftCompany option:selected").text());
    if (ddlEGiftCompany == 0 || ddlEGiftCompany == null) {
        alert('Please Select Store Or Company');
        return false;
    }

    var fromdt = "";
    var todt = "";


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/get_GV_Voucher_Details?store=' + StoreName + '&fromdt=' + fromdt + '&todt=' + todt + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            CreateVouGrid(result);
            loadingcut();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}

function CreateVouGrid(tableSrc) {

    $("#tblVouDtls").GridUnload();

    $("#tblVouDtls").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 300,
        cache: false,
        colNames: ['GV Code', 'Created Date', 'From Email', 'To Email', 'Amount', 'Validity'],
        colModel: [{ name: 'gift_voucher_code', index: 'gift_voucher_code', width: 200 },
                   { name: 'create_date', index: 'create_date', width: 200, sorttype: 'date', formatter: DateFormatter },
                   { name: 'giver_email', index: 'giver_email', width: 150 },
                   { name: 'rcv_email', index: 'rcv_email', width: 150 },
                   { name: 'amount', index: 'amount', width: 120 },
                   { name: 'validity', index: 'validity', width: 120 }],
        multiselect: true,
        sortname: 'gift_voucher_code',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });

    function DateFormatter(cellvalue, options, rowObject) {

        if (cellvalue != null && cellvalue != "") {
            var month, day;

            var date = new Date(cellvalue);

            if (date.getMonth() < 9)
                month = '0' + (date.getMonth() * 1 + 1);
            else
                month = (date.getMonth() * 1 + 1);

            if (date.getDate() < 10)
                day = '0' + date.getDate();
            else
                day = date.getDate();

            return (month + '/' + day + '/' + date.getFullYear())
        }
        else {

            return "";
        }
        //dateFormat(date, "dddd, mmmm dS, yyyy, h:MM:ss TT");
        //$.formatDateTime('mm/dd/y g:ii a', date); // 07/05/12 9:55 AM
    }

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#tblVouDtls").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    //$("#tblVouDtls").fluidGrid({ example: "#catalog-search .catalog-searchResult-main", offset: 0 });


    loadingcut();
}



function ExportGVVoucher() {

    var ids = jQuery("#tblVouDtls").getGridParam('selarrrow');

    if (typeof ids === "undefined") {
        alert('Please Populate The Grid First');
        return false;
    }
    if (ids.length == 0) {
        alert('Please Select Atleast One Voucher');
        return false;

    }
    var Voucher_Data = new Array();
    var selRow = jQuery("#tblVouDtls").jqGrid('getGridParam', 'selarrrow');  //get selected rows
    for (var i = 0; i < selRow.length; i++)  //iterate through array of selected rows
    {
        var ret = jQuery("#tblVouDtls").jqGrid('getRowData', selRow[i]);   //get the selected row
        Voucher_Data[i] = ret;
    }

    loadingadd();


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/gv/postExportVouList/',
        type: 'POST',
        dataType: 'json',
        data: { GridData: Voucher_Data },
        success: function (result) {
            loadingcut();
            window.open(siteurl + result + ".csv", "_blank");

        },
        error: function (req, status, errorObj) {
            alert('Error');
            return false;
        }
    });


}



function getPromoDetails(ddlID) {

    var Promo_Id = $('#ddlPromotion').val();

    if (Promo_Id == null) {
        Promo_Id = "0";
    }

    loadingadd();


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/promo_list/',
        type: "GET",
        data: { promo_id: Promo_Id },
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            ddlID.append($("<option></option>").val(0).text("New Promotion"));
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.promo_id).text(value.promo_name));
            });
            loadingcut();
        }
    });




}

function SavePromotion() {

    var Promotion_ID = $.trim($("#ddlPromotion").val());

    var txtPromoName = $.trim($("#txtPromoName").val());
    if (txtPromoName == null || txtPromoName == "") {
        alert('Please Enter Promotion Name');
        return false;
    }

    var ddlCustomerStore = $.trim($("#ddlCustomerStore").val());
    if (ddlCustomerStore == null || ddlCustomerStore == "0") {
        alert('Please Select Customer/Store');
        return false;
    }
    var Comp = $.trim($('#ddlCustomerStore :selected').text());



    var txtMinPurchase = $.trim($("#txtMinPurchase").val());
    if (txtMinPurchase == null || txtMinPurchase == "") {
        alert('Please Enter Minimum Purchase');
        return false;
    }

    var ddlDiscType = $.trim($("#ddlDiscType").val());
    if (ddlDiscType == null || ddlDiscType == "0") {
        alert('Please Select Discount Type');
        return false;
    }

    var txtMaxDiscount = $.trim($("#txtMaxDiscount").val());
    if (txtMaxDiscount == null || txtMaxDiscount == "") {
        alert('Please Enter Max Discount');
        return false;
    }

    var txtPromoStartDate = $.trim($("#txtPromoStartDate").val());
    if (txtPromoStartDate == null || txtPromoStartDate == "") {
        alert('Please Choose Start Date');
        return false;
    }

    var txtPromoStartDate = $.trim($("#txtPromoStartDate").val());
    if (txtPromoStartDate == null || txtPromoStartDate == "") {
        alert('Please Choose Start Date');
        return false;
    }

    var txtPromoEndDate = $.trim($("#txtPromoEndDate").val());
    if (txtPromoEndDate == null || txtPromoEndDate == "") {
        alert('Please Choose End Date');
        return false;
    }

    if (DateComparison(txtPromoStartDate, txtPromoEndDate) == false) {
        alert('Start Date Should Be Less Than End Date.');
        return false;
    }

    loadingadd();
    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/save_promo/',
        type: 'POST',
        data: { promo_id: Promotion_ID, promo_name: txtPromoName, company: Comp, min_purchase: txtMinPurchase, disc_type: ddlDiscType, max_discount: txtMaxDiscount, start_date: txtPromoStartDate, end_date: txtPromoEndDate },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();

            if (Promotion_ID == "0") {
                alert('Record Inserted Successfully');
            }
            else {
                alert('Record Updated Successfully');
            }

            $('#btnTransferPromo').removeAttr('disabled');
            $("#ddlPromotion").val(0);
            getPromoDetails($("#ddlPromotion"));

        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}

function DateComparison(Date1, Date2) {

    var FistDate = new Date(Date1);
    var SecondDate = new Date(Date2);

    if (FistDate > SecondDate) {

        return false;

    }
    else {

        return true;

    }

}

function TransferPromotion() {


    var ddlPromotion = $.trim($("#ddlPromotion").val());
    if (ddlPromotion == null || ddlPromotion == "0") {
        
        alert('Please Select Promotion');
        return false;
    }


    var ddlCustomerStore = $.trim($("#ddlCustomerStore").val());
    if (ddlCustomerStore == null || ddlCustomerStore == "0") {
        alert('Please Select Customer/Store');
        return false;
    }

    var Comp = $.trim($('#ddlCustomerStore :selected').text());

    var Curren_Date = new Date();
    var End_Date = $.trim($("#txtPromoEndDate").val());
    End_Date = new Date(End_Date);
    if (Curren_Date > End_Date) {
        alert("End Date Cannot Be Less Than Today's Date.");
        return false;

    }


    loadingadd();
    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/transfer_promo/',
        type: 'POST',
        data: { promo_id: ddlPromotion, company: Comp },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert("Transfer Points To Store Successfully.");
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}


function ShowPin() {

    var txtChngPinUserID = $.trim($("#txtChngPinUserID").val());
    if (txtChngPinUserID == null || txtChngPinUserID == "") {
        alert('Please Enter User ID');
        return false;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/getUserPin/{strUserId}',
        type: "GET",
        data: { strUserId: txtChngPinUserID },
        dataType: "json",
        cache: false,
        success: function (result) {
            loadingcut();
            $('#PinDiv').addClass('PinFieldRed');
            $("#lblPin").text(result);

        },
        error: function (req, status, errorObj) {
            loadingcut();
            alert('Error');

        }
    });


}

function ChangePin() {

    var txtChngPinUserID = $.trim($("#txtChngPinUserID").val());
    if (txtChngPinUserID == null || txtChngPinUserID == "") {
        alert('Please Enter User ID');
        return false;
    }
    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/admin/getChangeUserPin/{strUserId}',
        type: "GET",
        data: { strUserId: txtChngPinUserID },
        dataType: "json",
        cache: false,
        success: function (result) {
            loadingcut();
            $('#PinDiv').addClass('PinFieldRed');
            $("#lblPin").text(result);
        },
        error: function (req, status, errorObj) {
            loadingcut();
            alert('Error');
        }
    });

}

function ChangeSendPin() {

    alert('In Progress');

}



function SendPromoWithCredentials() {

    var Email_IDs = GetSelectedEmailID();

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/postSendUserPromoCredential/',
        type: 'POST',
        data: { emailids: Email_IDs },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}


function SendPromo() {

    var Email_IDs = GetSelectedEmailID();

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/postSendUserPromo/',
        type: 'POST',
        data: { emailids: Email_IDs },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}
function SendSMSTransactional() {

    var Email_IDs = GetSelectedEmailID();

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/postSendSmsTransactional/',
        type: 'POST',
        data: { emailids: Email_IDs },
        dataType: 'json',
        cache: false,
        success: function (result) {
            loadingcut();
            alert(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}
function getOrderTrackRecord() {

    loadingadd();

    var OrdStore = $("#ddlStoreForTrack").val();
    if (OrdStore == 0) {

        OrdStore = null;
    }
    else {

        var OrdStore = $.trim($("#ddlStoreForTrack option:selected").text());
    }

    var OrdTyp = $("#ddlOrderTrackStatus").val();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/getOrderTrackRecord?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            CreateOrderTrackGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });

}

function CreateOrderTrackGrid(tableSrc) {

    $("#OrderTrackRecord").GridUnload();

    $("#OrderTrackRecord").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 350,
        cache: false,
        colNames: ['Order #', 'Store Company', 'Date', 'Customer Name', 'Ship To Name', 'Status', 'Total Purchase Amount', 'G.T. (Purchased)'],
        colModel: [{ name: 'order_id', index: 'order_id', width: 120 },
                   { name: 'store', index: 'store', width: 250 },
                   { name: 'order_date', index: 'order_date', width: 180 },
                   { name: 'BillToName', index: 'BillToName', width: 250 },
                   { name: 'ShipToName', index: 'ShipToName', width: 200, hidden: true },
                   { name: 'Order_Status', index: 'Order_Status', width: 200 },
                   { name: 'total_amount', index: 'total_amount', width: 200 },
                   { name: 'total_amount', index: 'total_amount', width: 150, hidden: true }],
        //multiselect: true,
        sortname: 'order_id',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });



    for (var i = 0; i <= tableSrc.length; i++) {
        $("#OrderTrackRecord").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    //$("#OrderTrackRecord").fluidGrid({ example: "#catalog-search .catalog-searchResult-main", offset: 0 });


    loadingcut();

}

function SendTrackReport() {


    loadingadd();

    var OrdStore = $("#ddlStoreForTrack").val();
    if (OrdStore == 0) {
        OrdStore = null;
    }
    else {

        var OrdStore = $.trim($("#ddlStoreForTrack option:selected").text());
    }

    var OrdTyp = 1;

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/getSendTrackReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            loadingcut();
            alert('Report Send Successfully');
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}

function getWillShipTrackRecord() {

    loadingadd();

    var OrdStoreVal = $("#ddlStoreForTrack").val();
    var OrdStore = null;

    if (OrdStoreVal == 0) {

        OrdStore = null;
    }
    else {

        var OrdStore = $.trim($("#ddlStoreForTrack option:selected").text());
    }

    var OrdTyp = $("#ddlOrderTrackStatus").val();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/getWillShipTrackReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            CreateOrderTrackGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });



}


function WillShipTrackReport() {

    loadingadd();

    var OrdStoreVal = $("#ddlStoreForTrack").val();
    var OrdStore = null;

    if (OrdStoreVal == 0) {
        OrdStore = null;
    }
    else {
        var OrdStore = $.trim($("#ddlStoreForTrack option:selected").text());
    }

    var OrdTyp = 4;

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/getWillShipReport?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            loadingcut();
            alert('Report Send Successfully');
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });



}

function WillShipReportAftMaxDays() {

    loadingadd();

    var OrdStoreVal = $("#ddlStoreForTrack").val();
    var OrdStore = null;

    if (OrdStoreVal == 0) {
        OrdStore = null;
    }
    else {
        var OrdStore = $.trim($("#ddlStoreForTrack option:selected").text());
    }

    var OrdTyp = 4;

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/getWillShipReportAfterMaxShip?OrderStore=' + OrdStore + '&OrderType=' + OrdTyp + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {

            loadingcut();
            alert('Report Send Successfully');
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });



}


function ExportDailyReport() {

    var ddlReportStoreVal = $('#ddlReportStore').val();

    if (ddlReportStoreVal == "0") {
        alert('Please Select A Company');
        return false;
    }

    var ddlReportStore = $("#ddlReportStore option:selected").text();

    var txtReportFrmDt = $.trim($("#txtReportFrmDt").val());
    if (txtReportFrmDt == null || txtReportFrmDt == "") {
        alert('Please Choose From Date');
        return false;
    }

    var txtReportToDt = $.trim($("#txtReportToDt").val());
    if (txtReportToDt == null || txtReportToDt == "") {
        alert('Please Choose To Date');
        return false;
    }


    loadingadd();

    $.support.cors = true;

    $.ajax({
        url: rooturl + '/category/getExportDailySalesReport?company=' + ddlReportStore + '&ReportFromDate=' + txtReportFrmDt + '&ReportToDate=' + txtReportToDt + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            loadingcut();
            window.open(siteurl + result + ".csv", "_blank");
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });
}


function SearchProductList() {

    var txtSrchProdSKU = $.trim($("#txtSrchProdSKU").val());
    var txtSrchProdId = $.trim($("#txtSrchProdId").val());
    var txtSrchProdName = $.trim($("#txtSrchProdName").val());
    var ddlSrchBrand = $.trim($("#ddlSrchBrand").val());

    if (txtSrchProdSKU == '' && txtSrchProdId == '' && txtSrchProdName == '' && ddlSrchBrand == '') {
        alert('Please Enter Atleast One Criteria');
        return false;
    }

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/getProdSearchList?prodsku=' + txtSrchProdSKU + '&prodid=' + txtSrchProdId + '&prodName=' + txtSrchProdName + '&prodbrand=' + ddlSrchBrand + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            CreateProdGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}

function CreateProdGrid(tableSrc) {

    $("#tblSrchProdGrid").GridUnload();

    $("#tblSrchProdGrid").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 280,
        cache: false,
        colNames: ['', 'Product SKU', 'Product Id', 'Product Name', 'Brand'],
        colModel: [{ name: 'id', index: 'id', width: 30, align: "center", edittype: 'image', formatter: RadioButtonFormatter },
                   { name: 'sku', index: 'sku', width: 120 },
                   { name: 'id', index: 'id', width: 120 },
                   { name: 'Name', index: 'Name', width: 600 },
                   { name: 'brand', index: 'brand', width: 200 }],
        //multiselect: true,
        sortname: 'id',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });

    function RadioButtonFormatter(cellvalue, options, rowObject) {
        return "<input id='rbSelLoc' type='radio' name='rbSelLoc'/>";
    };

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#tblSrchProdGrid").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    //$("#OrderTrackRecord").fluidGrid({ example: "#catalog-search .catalog-searchResult-main", offset: 0 });


    loadingcut();

}



function ExportCustPointBalReport() {

    var ddlCustPointStoreReportVal = $('#ddlCustPointStoreReport').val();

    if (ddlCustPointStoreReportVal == "0") {
        alert('Please Select A Company');
        return false;
    }

    var ddlCustPointStoreReport = $("#ddlCustPointStoreReport option:selected").text();

    var txtEmailCustPointReport = $.trim($("#txtEmailCustPointReport").val());


    loadingadd();

    $.support.cors = true;

    $.ajax({
        url: rooturl + '/category/getExportCustPointBalReport?company=' + ddlCustPointStoreReport + '&email_id=' + txtEmailCustPointReport + '&json=true',
        type: "GET",
        dataType: "json",
        cache: false,
        success: function (result) {
            loadingcut();
            window.open(siteurl + result + ".csv", "_blank");
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });
}


function BulkProdPrice() {

    PopClose = 1;

    $("#txtBulkProdName").val($('#ddlProduct :selected').text());
    $("#txtBulkProdId").val($('#ddlProduct :selected').val());

    $(".bulkprice").val('');

    $('.popup-main-container').addClass('hide');
    $('#BulkProdPriceDiv').removeClass('hide');
}

function BulkProdStock(CallFrom, ProdName, ProdId) {

    var txtBulkwarehouse = $("#txtBulkwarehouse");
    popwarehouse(txtBulkwarehouse);

    if (CallFrom == 1) {

        PopClose = 2;

        $("#txtBulkStockProdName").val($('#ddlProduct :selected').text());
        $("#txtBulkStockProdId").val($('#ddlProduct :selected').val());
    }
    else {

        PopClose = 3;
        $("#BulkProdStockTable").GridUnload();
        $("#txtBulkStockProdName").val(ProdName);
        $("#txtBulkStockProdId").val(ProdId);
        getchildprodstock(ProdId);
    }

    $(".bulkstock").val('');

    $('.popup-main-container').addClass('hide');
    $('#BulkProdStockDiv').removeClass('hide');
}


function getchildprodstock(prodid) {


    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/child_product_details/{prod_id}',
        type: 'GET',
        data: { prod_id: prodid },
        dataType: 'json',
        success: function (result) {

            PopBulkStockGrid(result[0].stock);

            loadingcut();
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });



}

function AddBulkPrice() {

   
    var txtProdId = $.trim($("#txtBulkProdId").val());

    var txtBulkMinQty = $.trim($("#txtBulkMinQty").val());
    if (txtBulkMinQty == 0 || txtBulkMinQty == null) {
        alert('Please Enter Minimum Quantity');
        return false;
    }

    var txtBulkMaxQty = $.trim($("#txtBulkMaxQty").val());
    if (txtBulkMaxQty == 0 || txtBulkMaxQty == null) {
        alert('Please Enter Maximum Quantity');
        return false;
    }

    var txtBulkMRP = $.trim($("#txtBulkMRP").val());
    if (txtBulkMRP == null || txtBulkMRP == "") {
        alert('Please Enter MRP');
        return false;
    }


    var txtBulkListPrice = $.trim($("#txtBulkListPrice").val());
    if (txtBulkListPrice == null || txtBulkListPrice == "") {
        alert('Please Enter List Price');
        return false;
    }

    var txtBulkDiscount = $.trim($("#txtBulkDiscount").val());
    if (txtBulkDiscount == null || txtBulkDiscount == "") {
        alert('Please Enter Discount');
        return false;
    }

    var txtBulkMinSalePrice = $.trim($("#txtBulkMinSalePrice").val());
    if (txtBulkMinSalePrice == null || txtBulkMinSalePrice == "") {
        alert('Please Enter Minimum Sale Price');
        return false;
    }


    var txtBulkKAShipping = $.trim($("#txtBulkKAShipping").val());
    if (txtBulkKAShipping == null || txtBulkKAShipping == "") {
        alert('Please Enter KA Shipping');
        return false;
    }

    var txtBulkOtherShipping = $.trim($("#txtBulkOtherShipping").val());
    if (txtBulkOtherShipping == null || txtBulkOtherShipping == "") {
        alert('Please Enter Other Shipping');
        return false;
    }


    var grid = jQuery('#BulkProdPriceTable');
    var GridData = grid.getRowData();
    var NewPriceGridData = new Array();


    if (EditProdPriceCond == 1) {

        for (var i = 0; i < GridData.length; i++) {

            

            if (GridData[i].min_qty == min_quantity && GridData[i].max_qty == max_quantity) {
                var NewPriceData = {
                    "min_qty": txtBulkMinQty,
                    "max_qty": txtBulkMaxQty,
                    "mrp": txtBulkMRP,
                    "list": txtBulkListPrice,
                    "discount": txtBulkDiscount,
                    "min": txtBulkMinSalePrice,
                    "final_offer": txtBulkListPrice,
                    "final_discount": txtBulkDiscount,
                    "ka_shipping": txtBulkKAShipping,
                    "other_shipping": txtBulkOtherShipping

                }

            } else {

                var NewPriceData = {
                    "min_qty": GridData[i].min_qty,
                    "max_qty": GridData[i].max_qty,
                    "mrp": GridData[i].mrp,
                    "list": GridData[i].list,
                    "discount": GridData[i].discount,
                    "min": GridData[i].min,
                    "final_offer": GridData[i].final_offer,
                    "final_discount": GridData[i].final_discount,
                    "ka_shipping": GridData[i].ka_shipping,
                    "other_shipping": GridData[i].other_shipping

                }
            }
            NewPriceGridData[i] = NewPriceData;

        }
    } else {

        NewPriceGridData = GridData;
        var NewPriceData = {
            "min_qty": txtBulkMinQty,
            "max_qty": txtBulkMaxQty,
            "mrp": txtBulkMRP,
            "list": txtBulkListPrice,
            "discount": txtBulkDiscount,
            "min": txtBulkMinSalePrice,
            "final_offer": txtBulkListPrice,
            "final_discount": txtBulkDiscount,
            "ka_shipping": txtBulkKAShipping,
            "other_shipping": txtBulkOtherShipping

        }
        NewPriceGridData[GridData.length] = NewPriceData;

    }

    PopBulkPriceGrid(NewPriceGridData);


    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/update_prod_price_list/',
        type: 'POST',
        data: { id: txtProdId, price: NewPriceGridData },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            EditProdPriceCond = 0;
            $("#txtBulkMinQty , #txtBulkMaxQty").prop('disabled', false);
            $(".bulkprice").val('');
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });
}

function ClearFields() {

    $("#txtBulkMinQty").val('');
    $("#txtBulkMaxQty").val('');
    $("#txtBulkMRP").val('');
    $("#txtBulkListPrice").val('');
    $("#txtBulkDiscount").val('');
    $("#txtBulkMinSalePrice").val('');
    $("#txtBulkKAShipping").val('');
    $("#txtBulkOtherShipping").val('');
    
    $("#txtBulkMinQty").prop("disabled", false);
    $("#txtBulkMaxQty").prop("disabled", false);
   
}

function PopBulkPriceGrid(tableSrc) {


    $("#BulkProdPriceTable").GridUnload();

    $("#BulkProdPriceTable").jqGrid({

        datastr: tableSrc,
        datatype: "local",
        height: 230,

        colNames: ['Minimum Quantity', 'Maximum Quantity', 'MRP', 'List Price', 'Discount', 'Minimum Sale Price', 'KA Shipping', 'Other Shipping', 'Final Offer', 'Final Discount', 'Edit', 'Delete', ''],
        colModel: [{ name: 'min_qty', index: 'min_qty', width: 150, align: "right" },
                   { name: 'max_qty', index: 'max_qty', width: 150, align: "right" },
                   { name: 'mrp', index: 'mrp', width: 130, align: "right" },
                   { name: 'list', index: 'list', width: 130, align: "right" },
                   { name: 'discount', index: 'discount', width: 135, align: "right" },
                   { name: 'min', index: 'min', width: 160, align: "right" },
                   { name: 'ka_shipping', index: 'ka_shipping', width: 130, align: "right" },
                   { name: 'other_shipping', index: 'other_shipping', width: 140, align: "right" },

                   { name: 'final_offer', index: 'final_offer', width: 55, hidden: true },
                   { name: 'final_discount', index: 'final_discount', width: 55, hidden: true },
                   { name: 'mrp', index: 'mrp', width: 100, align: "center", edittype: 'image', formatter: EditProdFormatter },
                   { name: 'mrp', index: 'mrp', width: 100, align: "center", edittype: 'image', formatter: DelImageFormatter },
                   { name: 'mrp', index: 'mrp', width: 55, hidden: true }

        ],
        sortname: 'mrp',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i>Added Price List</div>"

    });

    

    function EditProdFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        
        return "<input style='width:25px;height: 25px;' type='image' title='Edit' src='images/Edit.png' name='image'  " +
        "onclick=\"EditProdPrice('" + rowObject.min_qty + "','" + rowObject.max_qty + "','" + rowObject.mrp + "','" + rowObject.list + "','" + rowObject.discount + "','" + rowObject.min + "','" + rowObject.ka_shipping + "','" + rowObject.other_shipping + "');\" />";
    };

    function DelImageFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DelProdPrice('" + options.rowId + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++)
        $("#BulkProdPriceTable").jqGrid('addRowData', i + 1, tableSrc[i]);

}


var EditProdPriceCond = 0;
var max_quantity = 0;
var min_quantity = 0;

function EditProdPrice(MinQty, MaxQty, MRP, ListPrice, Discount, MinSalePrice, KAShip, OthShip) {

    EditProdPriceCond = 1;

    max_quantity = MaxQty;
    min_quantity = MinQty;

    $("#txtBulkMinQty , #txtBulkMaxQty").prop('disabled', false);

    $("#txtBulkMinQty").val(MinQty);
    $("#txtBulkMaxQty").val(MaxQty);
    $("#txtBulkMRP").val(MRP);
    $("#txtBulkListPrice").val(ListPrice);
    $("#txtBulkDiscount").val(Discount);
    $("#txtBulkMinSalePrice").val(MinSalePrice);
    $("#txtBulkKAShipping").val(KAShip);
    $("#txtBulkOtherShipping").val(OthShip);


}

function DelProdPrice(rowid) {


    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {

        $('#BulkProdPriceTable').jqGrid('delRowData', rowid);

        var Product_ID = $.trim($("#txtBulkProdId").val());
        var grid = jQuery('#BulkProdPriceTable');
        var GridData = new Array();
        GridData = grid.getRowData();

        if (GridData.length == 0) {
            var NewPriceData = {
                "mrp": 0.0
            }
            GridData[0] = NewPriceData;
        }


        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/update_prod_price_list/',
            type: 'POST',
            data: { id: Product_ID, price: GridData },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Deleted Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
                loadingcut();
            }
        });

    }

}




function CalBulkDiscount() {

    var MRP = $("#txtBulkMRP").val() * 1;
    var LIST_PRICE = $("#txtBulkListPrice").val() * 1;
    var CAL = LIST_PRICE / MRP;
    var DISCOUNT = 100 * (1 - CAL);
    $("#txtBulkDiscount").val(Math.round(DISCOUNT * 100) / 100);


}
function CalBulkListPrice() {

    var MRP = $("#txtBulkMRP").val() * 1;
    var DISCOUNT = $("#txtBulkDiscount").val() * 1;
    var CAL_AMT = (MRP * DISCOUNT) / 100;
    var LIST_PRICE = MRP - CAL_AMT;
    $("#txtBulkListPrice").val(Math.round(LIST_PRICE * 100) / 100);


}

function CloseBulkPop() {

    if (PopClose == "1") {
        $('#BulkProdPriceDiv').addClass('hide');
        $('#Product-Catalog-Maintenance').removeClass('hide');
    }
    else if (PopClose == "2") {
        $('#BulkProdStockDiv').addClass('hide');
        $('#Product-Catalog-Maintenance').removeClass('hide');
    }
    else if (PopClose == "3") {
        getChildProduct();
        $('#BulkProdStockDiv').addClass('hide');
        $('#Child-Product-Maintenance').removeClass('hide');
    }

}


function AddBulkStock() {


    var txtProdId = $.trim($("#txtBulkStockProdId").val());

    var txtBulkwarehouse = $.trim($("#txtBulkwarehouse").val());
    if (txtBulkwarehouse == "0" || txtBulkwarehouse == null) {
        alert('Please Enter Warehouse');
        return false;
    }

    var txtBulkStock = $.trim($("#txtBulkStock").val());
    if (txtBulkStock == 0 || txtBulkStock == null) {
        alert('Please Enter Stock Quantity');
        return false;
    }

    var grid = jQuery('#BulkProdStockTable');
    var GridData = grid.getRowData();
    var NewStockGridData = new Array();

    if (EditProdStockCond != 1) {

        for (var j = 0; j < GridData.length; j++) {
            if (GridData[j].warehouse.toLowerCase() == txtBulkwarehouse.toLowerCase()) {
                alert('Warehouse Is Already Added ');
                return false;
            }
        }

        NewStockGridData = GridData;

        var NewStockData = {
            "warehouse": txtBulkwarehouse,
            "stock": txtBulkStock,
            "blocked_stock": 0
        }
        NewStockGridData[GridData.length] = NewStockData;

    } else {
        for (var i = 0; i < GridData.length; i++) {

            if (GridData[i].warehouse.toLowerCase() == txtBulkwarehouse.toLowerCase()) {
                var NewStockData = {
                    "warehouse": txtBulkwarehouse,
                    "stock": txtBulkStock,
                    "blocked_stock": 0
                }
            } else {

                var NewStockData = {
                    "warehouse": GridData[i].warehouse,
                    "stock": GridData[i].stock,
                    "blocked_stock": 0
                }
            }
            NewStockGridData[i] = NewStockData;
        }
    }

    PopBulkStockGrid(NewStockGridData);

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/admin/update_prod_stock_list/',
        type: 'POST',
        data: { id: txtProdId, stock: NewStockGridData },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            alert('Record Saved Successfully.');
            $("#txtBulkwarehouse").prop('disabled', false);
            EditProdStockCond = 0;
            $(".bulkstock").val('');

        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });
}

function PopBulkStockGrid(tableSrc) {


    $("#BulkProdStockTable").GridUnload();

    $("#BulkProdStockTable").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 230,
        colNames: ['Warehouse', 'Stock', 'Edit', 'Delete', '', ''],
        colModel: [{ name: 'warehouse', index: 'warehouse', width: 200, align: "right" },
                   { name: 'stock', index: 'stock', width: 200, align: "right" },
                   { name: 'warehouse', index: 'warehouse', width: 100, align: "center", edittype: 'image', formatter: EditProdFormatter },
                   { name: 'warehouse', index: 'warehouse', width: 100, align: "center", edittype: 'image', formatter: DelImageFormatter },
                   { name: 'blocked_stock', index: 'blocked_stock', width: 200, align: "right", hidden: true },
                   { name: 'warehouse', index: 'warehouse', width: 200, align: "right", hidden: true }
        ],
        sortname: 'warehouse',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i>Added Stock List</div>"

    });

    function EditProdFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Edit' src='images/Edit.png' name='image'  " +
        "onclick=\"EditProdStock('" + rowObject.warehouse + "','" + rowObject.stock + "');\" />";
    };

    function DelImageFormatter(cellvalue, options, rowObject) {
        var UserId = cellvalue;
        return "<input style='width:25px;height: 25px;' type='image' title='Delete' src='images/Del_1.png' name='image'  " +
        "onclick=\"DelProdStock('" + options.rowId + "');\" />";
    };

    for (var i = 0; i <= tableSrc.length; i++)
        $("#BulkProdStockTable").jqGrid('addRowData', i + 1, tableSrc[i]);

}

var EditProdStockCond = 0;

function EditProdStock(warehouse, stock) {

    EditProdStockCond = 1;

    $("#txtBulkwarehouse").prop('disabled', true);
    $("#txtBulkwarehouse").val(warehouse);
    $("#txtBulkStock").val(stock);

}

function DelProdStock(rowid) {


    var Del_Confrm_Val = confirm("Are You Sure You Want To Delete?");

    if (Del_Confrm_Val == true) {

        $('#BulkProdStockTable').jqGrid('delRowData', rowid);

        var Product_ID = $.trim($("#txtBulkStockProdId").val());
        var grid = jQuery('#BulkProdStockTable');
        var GridData = new Array();

        GridData = grid.getRowData();

        if (GridData.length == 0) {
            var NewStockData = {
                "warehouse": "",
                "stock": 0,
                "blocked_stock": 0
            }
            GridData[0] = NewStockData;
        }


        loadingadd();

        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/admin/update_prod_stock_list/',
            type: 'POST',
            data: { id: Product_ID, stock: GridData },
            dataType: 'json',
            success: function (result) {
                loadingcut();
                alert('Record Deleted Successfully.');
            },
            error: function (req, status, errorObj) {
                alert('Error');
                loadingcut();
            }
        });

    }

}



function popwarehouse(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/getwarehouse',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            ddlID.append($("<option></option>").val("0").text("Select Warehouse"));
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.warehouse_name).text(value.warehouse_name));
            });
        }
    });

}

function PopPayStatus(PumaPayStatus) {

    //PumaPayStatus = 90;
    var ddlPayStatus = $("#ddlOrdStatus");
    var PayStatusArray = [];

    if (PumaPayStatus > 100) {
        PayStatusArray = [{ "val": "101", "paystatus": "Offline-Pending" }, { "val": "102", "paystatus": "Offline-Clear-Will Ship" }, { "val": "103", "paystatus": "Offline-Reject" }];
    }
    else if (PumaPayStatus == 1) {
        PayStatusArray = [{ "val": 2, "paystatus": "Received Payment" }, { "val": 5, "paystatus": "Shipped" }, { "val": 17, "paystatus": "Partial Shipping" }, { "val": 7, "paystatus": "Order Cancelled" }, { "val": 14, "paystatus": "Extend Time" }];
    }
    else if (PumaPayStatus == 17) {
        PayStatusArray = [{ "val": 2, "paystatus": "Received Payment" }, { "val": 5, "paystatus": "Shipped" }, { "val": 17, "paystatus": "Partial Shipping" }, { "val": 7, "paystatus": "Order Cancelled" }, { "val": 14, "paystatus": "Extend Time" }];
    }
    else{
        PayStatusArray = [{ "val": 4, "paystatus": "Will Ship" }, { "val": 5, "paystatus": "Shipped" }, { "val": 6, "paystatus": "Order Delayed" }, { "val": 7, "paystatus": "Order Cancelled" }, { "val": 13, "paystatus": "Dispute" }];
    }

    if (PumaPayStatus == 2) {     
        $('#tblWarehouse').removeClass('hide');
    }
    else {

        $('#tblWarehouse').addClass('hide');
    }

    


    ddlPayStatus.find('option').remove().end();
    ddlPayStatus.append($("<option></option>").val("0").text(""));
    $.each(PayStatusArray, function (key, value) {
        ddlPayStatus.append($("<option></option>").val(value.val).text(value.paystatus));
    });


}


function PopParentProd(ddlID) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/get_parent_prod_list',
        type: "GET",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            ddlID.append($("<option></option>").val("0").text("Select Product"));
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.id).text(value.Name));
            });

            loadingcut();
        }
    });

}


function chkChildProdExist(Product_Id) {

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/category/chkChildProdExist/{prod_id}',
        type: 'GET',
        data: { prod_id: Product_Id },
        dataType: 'json',
        success: function (result) {
            if (result == true) { 
                $("#chkHaveChild").prop('disabled', true);
            }
            else {
                $("#chkHaveChild").prop('disabled', false);
            }
            loadingcut();
        },
        error: function (req, status, errorObj) {
            alert('Error');
            loadingcut();
        }
    });


}




/****************************** Product Enquiry **********************************/


/************************************Populate Company ********************************/
function Populate_Company(ddlID) {

    var EntryMode = "N";

    loadingadd();

    $.support.cors = true;
    $.ajax({
        url: rooturl + '/user/admin/postPopulateCompany',
        type: "POST",
        data: {},
        dataType: "json",
        cache: false,
        success: function (result) {
            var tableSrc = result;
            ddlID.find('option').remove().end();
            $.each(tableSrc, function (key, value) {
                ddlID.append($("<option></option>").val(value.name).text(value.name));
            });

            loadingcut();
        }
    });

}

/******************************************************************************/


function Search() {

    var ddlSrchprodenq = $.trim($("#ddlcompany").val());
    // if (ddlSrchprodenq == 'New Company') {
    //     alert("Please select one company");
    //	return false;
    // }

    var user = $.trim($("#usrname").val());
    var status = $("#ddlstatus").val();

    PopulateProdEnq(ddlSrchprodenq, user, status);
    
}



function updatestatus(enq_status) {

    var ID = get_id();
    if (ID != false) {

        loadingadd();
        $.support.cors = true;
        $.ajax({
            url: rooturl + '/category/updateenqstatus',
            type: 'POST',
            data: { enqids: ID, status: enq_status },
            dataType: 'json',
            cache: false,
            success: function (result) {
                loadingcut();
                alert('Status successfully updated');
                Search();
            },
            error: function (req, status, errorObj) {
                alert('Error');
                loadingcut();
            }

        });
    }
}


function get_id() {
    try {
        var ids = jQuery("#EnquiryList").getGridParam('selarrrow');

        if (typeof ids == "undefined") {
            alert('Please Populate The Grid First');
            return false;
        }
        if (ids.length == 0) {
            alert('Please Select Atleast One Enquire ID');
            return false;

        }
        var count = ids.length;
        var ENQ_ID = new Array();
        for (var i = 0; i < count; i++) {
            ENQ_ID[i] = $('#EnquiryList').jqGrid('getCell', ids[i], 'id');
        }
        return ENQ_ID;
    }
    catch (e) {
        return 0;
    }
}

function PopulateProdEnq(ddlID, user, status) {
    $.support.cors = true;
    $.ajax({      
        url: rooturl + '/category/populateprodenquiry',
        type: "POST",
        data: { company_name: ddlID, name: user, status: status },
        dataType: "json",
        cache: false,
        success: function (result) {
            CreateProdEnquiryGrid(result);
        },
        error: function (req, status, errorObj) {
            alert('Error');
        }
    });

}


function CreateProdEnquiryGrid(tableSrc) {

    $("#EnquiryList").GridUnload();

    $("#EnquiryList").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        height: 300,
        cache: false,
        colNames: ['EnquireID', 'Company', 'Name', 'Email', 'City', 'Mobile', 'Status'],
        colModel: [{ name: 'id', index: 'id', width: 120 },
                   { name: 'company_name', index: 'company_name', width: 120 },
                   { name: 'name', index: 'name', width: 250 },
                   { name: 'email_id', index: 'email_id', width: 120 },
                   { name: 'city', index: 'city', width: 260 },
                   { name: 'mobile_no', index: 'mobile', width: 280 },
                   { name: 'status', index: 'status', width: 150 }
        ],
        sortorder: "desc",
        multiselect: true,
        viewrecords: true,
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"
    });

    for (var i = 0; i <= tableSrc.length; i++) {
        $("#EnquiryList").jqGrid('addRowData', i + 1, tableSrc[i]);
    }
    loadingcut();
}


/***********************************************************************************/


/*Start***********************Serach Product In Different Page*************************************/

var ProdSrchFrom = 0;
function OpenSearchPopUp(CallFrom) {

    ProdSrchFrom = CallFrom;

    var ddlSrchBrand = $("#ddlSrchBrand");
    PopulateBrandList(ddlSrchBrand, "L");

    $('.ProdSrchFld').val('');
    $("#tblSrchProdGrid").GridUnload();

    $('#ProdSearchDiv h2').addClass('headcolor');
    $('#ProdSearchDiv, .alert-overlay2').removeClass('hide');

}

/*End***********************Serach Product In Different Page*************************************/


/***************Special Discount *******************************************/

function GiveSplDiscount() {

    var spldiscount = $("#TextSplDiscount").val();
    //   var orderid = $("#lblOrder").val();
    $.ajax({
        url: rooturl + '/category/update_order_for_spldisc/',
        type: 'POST',
        data: { order_id: GlobalOrderId, special_discount: spldiscount },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            ViewOrder(GlobalOrderId);
            alert('Record Saved Successfully. Special Discount Given');
           
        },
        error: function () {
            alert('Error');
            
        }
    });



}


/***********End****************************************************************/

/***************Logo Charges Ajax Call *******************************************/

function AddLogoCharges() {

    var logo_chrg = $("#txtLogoCharges").val();
    //   var orderid = $("#lblOrder").val();
    $.ajax({
        url: rooturl + '/category/update_order_for_logo_charges/',
        type: 'POST',
        data: { order_id: GlobalOrderId, logo_charge: logo_chrg },
        dataType: 'json',
        success: function (result) {
            loadingcut();
            ViewOrder(GlobalOrderId);
            alert('Record Saved Successfully. Logo Charges Added');

        },
        error: function () {
            alert('Error');

        }
    });



}


/***********End****************************************************************/

/***********************************Partial Order************************************************/
function ViewPartialProd(tableSrc) {


    $("#PartialOrderProdGrid").GridUnload();

    $("#PartialOrderProdGrid").jqGrid({
        datastr: tableSrc,
        datatype: "local",
        cache: false,
        height: 350,
        colNames: ['Product ID', 'Product SKU', 'Product Name', 'Brand', 'Size', 'Quantity', 'Del Qty', 'Ship Qty', 'Unit Price', 'Discount', 'Sub Total', 'MRP', 'Status', 'Add Ship Qty'],
        colModel: [{ name: 'id', index: 'id', width: 90, align: "left" },
                   { name: 'sku', index: 'sku', width: 105, align: "left" },
                   { name: 'name', index: 'name', width: 240, align: "left" },
                   { name: 'brand', index: 'brand', width: 80, align: "left" },
                   { name: 'selected_size', index: 'selected_size', width: 55, align: "center" },
                   { name: 'quantity', index: 'quantity', width: 75, align: "center" },
                   { name: 'delivered_qty', index: 'delivered_qty', width: 75, align: "center" },
                   { name: 'shipping_qty', index: 'shipping_qty', width: 75, align: "center" },
                   { name: 'final_offer', index: 'final_offer', width: 85, align: "right" },
                   { name: 'discount', index: 'discount', width: 80, align: "right" },                
                   { name: 'sub_total', index: 'sub_total', width: 90, align: "right", formatter: CalSubTotal },
                   { name: 'mrp', index: 'mrp', width: 90, align: "right",hidden:true },
                   { name: 'item_status', index: 'item_status', width: 90, align: "left" },
                   { name: 'id', index: 'id', width: 110, align: "center", edittype: 'image', formatter: AddShipImageFormatter }
        ],
        multiselect: true,
        sortname: 'Product ID',
        sortorder: "desc",
        caption: "<div class='search_result_heading green'><i class='icons'></i></div>"

    });


    function CalSubTotal(cellvalue, options, rowObject) {

        var Qty = rowObject.quantity;
        var ProdFinalOffer = rowObject.final_offer;
        var SubTotal = 0;
        SubTotal = Qty * ProdFinalOffer * 1;

        return Math.round(SubTotal * 100) / 100;

    };

    function AddShipImageFormatter(cellvalue, options, rowObject) {
        return "<input style='width:25px;height: 25px;' type='image' title='Add Stock' src='images/AddItem.png' name='image'  " +
        "onclick=\"AddShippingQty('" + rowObject.name + "','" + rowObject.sku + "','" + rowObject.id + "','" + rowObject.quantity + "','" + rowObject.delivered_qty + "','" + rowObject.shipping_qty + "');\" />";
    };
    for (var i = 0; i <= tableSrc.length; i++) {
        $("#PartialOrderProdGrid").jqGrid('addRowData', i + 1, tableSrc[i]);
    }

    loadingcut();
}

function AddShippingQty(ProdName, ProdSku,ProdId, OrdQty, DelQty, ShipQty) {

    $("#txtShipPartialOrd").val(GlobalOrderId);
    $("#txtShipPartialProdName").val(ProdName);
    $("#txtShipPartialProdSKU").val(ProdSku);
    $("#txtShipPartialProdID").val(ProdId);
    $("#txtShipPartialProdOrdQty").val(OrdQty);
    $("#txtShipPartialProdDelQty").val(DelQty);
    $("#txtShipPartialProdShipQty").val(ShipQty);

    $('#AddShippingQty, .alert-overlay3').removeClass('hide');
}

function get_Partial_Prod_ID() {
    try {
        var ids = jQuery("#PartialOrderProdGrid").getGridParam('selarrrow');

        if (typeof ids == "undefined") {
            alert('Please Populate The Grid First');
            return false;
        }
        if (ids.length == 0) {
            alert('Please Select Atleast One Product ID');
            return false;

        }
        var count = ids.length;
        var PROD_ID = new Array();
        for (var i = 0; i < count; i++) {

            var shipping_qty = $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'shipping_qty');
            var shipping_qty = shipping_qty * 1;


            if (item_status == "Shipped") {
                alert('You Have Selected Delivered Item');
                return false;
            }

            else if (shipping_qty <= 0 ) {
                alert('Please Enter Shipping Quantity');
                return false;
            }
           
            var prod_data = {
                id: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'id'),
                brand: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'brand'),
                final_offer: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'final_offer'),
                mrp: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'mrp'),
                name: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'name'),
                quantity: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'quantity'),
                selected_size: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'selected_size'),
                sku: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'sku'),
                shipping_qty: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'shipping_qty'),
                delivered_qty: $('#PartialOrderProdGrid').jqGrid('getCell', ids[i], 'delivered_qty')
            }

            PROD_ID[i] = prod_data;
        }
        return PROD_ID;
    }
    catch (e) {
        return false;
    }
}

