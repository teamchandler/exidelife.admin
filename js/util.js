﻿function ConvertDatetime(dateStr) {

    if (dateStr == "") {
        return null;
    }
    else {
        var newDateObj = new Date();
        var hr = (newDateObj.getHours() < 10) ? "0" + newDateObj.getHours() : newDateObj.getHours();
        var min = (newDateObj.getMinutes() < 10) ? "0" + newDateObj.getMinutes() : newDateObj.getMinutes();
        var sec = newDateObj.getSeconds();
        var formdatetime = hr + ":" + min + ":" + sec;
        return formDate = dateStr + " " + formdatetime;
    }
}

function GetChkBoxValue(bool) {
    if (bool)
        return 1;
    return 0;
}

function PopulateChkBox(chkId, value) {
    if (value == 1) {
        chkId.prop("checked", true);
    }
    else {
        chkId.prop("checked", false);
    }
}
